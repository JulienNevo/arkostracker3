#!/bin/bash

echo "Uploading..."

# Rename a simple generated file ("arkostracker.zip") into a more complex filename ("arkostracker-windows-3.0.0.zip").
mv "$1" "$2"

# Upload to BitBucket. See https://support.atlassian.com/bitbucket-cloud/docs/deploy-build-artifacts-to-bitbucket-downloads/
curl -v -f --retry 3 --max-time 900 --connect-timeout 360 -X POST "https://${BITBUCKET_USERNAME}:${BITBUCKET_APP_PASSWORD}@api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads" --form files=@"$2"

echo "Uploaded."