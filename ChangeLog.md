# 3.2.5 - 06/03/25

## Features
- Generate instruments from selected blocks!
- Instrument Editor: Added a small highlight of what bar is being played.
- Lists: Added a "duplicate" option.
- Linker: Editing multiple positions is now possible, to modify their height and related pattern colors.
- Pattern Viewer: added a "swap channels" tool in the toolbox.
- The Export to WAV dialog has now independent parameters (mix, frequency, stereo separation).

## Tweaking
- Effect Context: when playing the song (as opposed to line/pattern/block), the loop does not use effect context, which is what would happen in the players.
- Windows:
    - Installer has a better icons and installation folder.
- Stream Music Analyzer: the loop management has been refined, UI slightly reorganized.
- Loading a song via "Open recent Song" updates the last folder to open when loading a song.
- Mac:
    - Save shortcut now uses Command, not Ctrl.
- Added more bass drum/snare instruments from famous Atari ST composers to the package.

## Bugfixes
- Corrected a big bug in the audio engine if using multiple PSGs and fast replay frequency (the audio could stutter, also in the WAV export) (thanks Jonah (Tasteful Mr) Ship!).
- A change in the replay frequency wasn't taken in account directly (thanks Jonah (Tasteful Mr) Ship!).
- Stream Music Analyzer: Improved the instrument export:
    - Noise could be wrong if the YM had too many bits encoded.
    - Noise and software period of 0 are set to 1 if used, as AT considers 0 "empty" (for noise) and "auto" (for period). This takes care of several Big Alec sounds.
- PV toolbox: on rare occasions, the Undo/Redo could fail if using multiple subsongs.
- Various minor fixes...

# 3.2.4 - 11/02/25

## Features
- Effect context in the Pattern Viewer! Playing will recover previous effects, the cursor follows them too.
- One or several instruments can be drag'n'dropped to load them.
- BPM is displayed along the speed.

## Tweaking
- Added four songs composed by Doclands (thanks!) to the package.
- Sample editor:
    - Samples now display the original file name.
    - Display of the static data is moved below the sample wave. 
- Space now plays the pattern from the top/block, everywhere in the software. Previous actions mapped to space has been remapped with shift.
- Pattern Viewer: a glide after a reset effect is considered an error.
- Play icons in the top bar are highlighted according to the play mode. 
- Added the "Gray" theme.
- Focusing on lists shows the right color in the background.

## Bugfixes
- AT2 instruments can now be loaded (thanks Haddhar!).
- Since AT2 (!), a Reset effect didn't stop a volume in/out! This was a critical mistake! Present in the editor only, not in the players.
- Mac:
    - All cut/copy/paste were wrongly mapped to Ctrl instead of Command (thanks Doclands!).
- Windows:
    - The YM exports could wrongfully be zipped (thanks Doclands!).
    - Corrected a possible crash when changing the keyboard layout.
- Pattern Viewer: improved/corrected the positioning of the toolbox and its icon when resizing the window.
- YM export: corrected a possible crash yet again.
- Loaded samples were using a default sample frequency, instead of the real one (thanks Doclands!).

# 3.2.3 - 23/12/24

## Features
- Tools:
    - Added a "Rearrange patterns" to sort the patterns in ascending order. 
    - "Rearrange patterns" can also remove useless patterns.
    - Added a "Clear patterns" to remove all the patterns and tracks of this subsong.

## Tweaking
- IE: The position height is now in decimal if the lines are shown in decimal (thanks Reset!). Improved the Set Height dialog.
- Bar editors:
    - Space to duplicate now fills the other values better, when out of bounds.

## Bugfixes
- The shift parameter in Expressions wasn't saved!
- Arkos Tracker 1 song couldn't be loaded unless unzipped (thanks Reset!).
- Linker:
    - Cannot cut all the patterns anymore (thanks Reset!). Cut and Delete contextual options are greyed out if the whole song is selected.
- YM Export:
    - Non-used hardware envelopes/periods aren't encoded as 0 anymore.
    - Corrected how simple event could produce a corrupted file.
    - Corrected a possible crash on export.

# 3.2.2 - 01/12/24

## Features
- Linux: The app icon is well set, as app is well declared to the OS and can be found (thanks Shalafi!).
- Mac: Run now on both Silicon and older Intel processors.
- Instrument Editor:
    - Most rows can now be hidden. The arrow is shown only on hovering. Empty rows are automatically hidden.
    - Moving from an instrument to another keeps its hidden/shrunk features.
    - Rows can also be shrunk according to their maximum values.
    - Shift/ctrl+F to toggle the visibility of the row above/below the cursor.
    - Shift/ctrl+G to grow/shrink the row where the cursor is.
    - Shift+H to hide all the empty rows and shrink all rows. Ctrl+H to also shrink to a maximum.
- Bar editors:
    - Backspace to reset the value where the cursor is.
    - Space now only duplicates the value where cursor is (shift+space to duplicate the whole column).
- Test area: the currently used expression is written below each icon.
- Streamed music analyzer:
    - Last loaded song and playing position are kept in memory for reopening.
    - Added a "lock length" icon for the loop end to follow the loop start.
- Press Tab to go from a panel to another, Shift+Tab to go reverse.
- Command line tools (AKG, AKM): added a "s" parameter to export only specific subsongs. As a convenience, if not present, all the subsongs are exported (different behavior from AT2 where only the first one was!).
- Selecting the instrument/pitch/arpeggio editor panel automatically opens the related list.

## Bugfixes
- Creating a new song wouldn't clear the path of the previous song, so saving it would overwrite it!
- Corrected the width of the caption background in the bars when there are 3 digits.
- The default song doesn't have a second empty arpeggio/pitch anymore.

# 3.2.1 - 21/10/24

## Tweaking
- PV: Changed how clone pattern would treat linked-to tracks: cloned tracks are linked.
- Some wording in contextual menus.

## Bugfixes
- LK: Corrected a few bugs when moving patterns (thanks Zik!).
- PV: Special track names were not saved (thanks Zik again!).
- WAV export: Corrected a wrong looping behavior (thanks Zik again again!).

# 3.2.0 - 21/09/24

## Features
- An instrument optimizer tool window allows to see what instruments are unused, and delete them.
- PV: Toolbox to transpose, map/remap instrument.

## Tweaking
- PV: Crt+T to select the whole track can also select several tracks according the current selection.
- PV: Crt+A to toggle between selecting the whole music tracks/tracks.
- PV: Space behavior changed: if there is no block, plays the whole pattern instead of creating a block.
- TA: Muted channels are now skipped in both monophony and polyphony.
- LK: Cloning a position doesn't duplicate the tracks names (to make the Link feature work better) and marker.
- LK: New shortcut to increase/decrease the pattern index (ctrl+shift+up/down).
- IE: New shortcuts to set/toggle the loop/auto-spread (Ctrl+I/O/P, add shift for auto-spread).

## Bugfixes
- PV: Corrected the MIDI record which failed.
- IE: Corrected a crash when moving the cursor to the start.
- TA: Corrected a crash when changing a toggle (regression).

# 3.1.0 - 15/06/24

## Breaking changes
- Due to the new Link feature, **songs created with 3.0.0 will NOT be compatible!**
- AKY export has been fixed for hardware+noise. **All AKY must be exported again with AT3 to sound correctly with the new player**.

## Features
- PV: Tracks can be linked/unlinked, Special Tracks too. Context menu possible.
- PV: Effect dropdown overhaul.
- PV: Capture the instrument is extended to capture effect and arp/pitch.
- PV: Red border when recording.
- PV: Reordering/adding/removing instruments/expressions does not erase illegal items in the patterns anymore.
- LK: Added hovering icons to add new/repeat positions.
- LK: Enter and double-click on a position now opens the Pattern Viewer.
- LK: Click on the color swatch to edit the position.
- IE: A cursor is now present. Use cursor, space to duplicate, Enter to edit. Insert/delete/generate actions perform from here.
- IE/PV: Alt+mouse wheel to scroll horizontally.
- Ability to import/export Themes from/to XML.
- Added changelog to the build.

## Bugfixes
- AKY: Hardware sound with noise was broken on exporter and player config change interpreted in the player (thanks Zik).

# 3.0.0 - 16/03/24

First release.