#include "Properties.h"

namespace arkostracker
{
Properties::Properties(juce::String pName, const int pInitialSpeed, const float pReplayFrequencyHz, const int pDigiChannel,
    const int pHighlightSpacing, const int pSecondaryHighlight) noexcept :
            name(std::move(pName)),
            initialSpeed(pInitialSpeed),
            replayFrequencyHz(pReplayFrequencyHz),
            digiChannel(pDigiChannel),
            highlightSpacing(pHighlightSpacing),
            secondaryHighlight(pSecondaryHighlight)
{
}

Properties::Properties(const Subsong::Metadata& metadata) noexcept :
        name(metadata.getName()),
        initialSpeed(metadata.getInitialSpeed()),
        replayFrequencyHz(metadata.getReplayFrequencyHz()),
        digiChannel(metadata.getDigiChannel()),
        highlightSpacing(metadata.getHighlightSpacing()),
        secondaryHighlight(metadata.getSecondaryHighlight())
{
}

bool Properties::operator==(const Properties& rhs) const
{
    return name == rhs.name &&
           initialSpeed == rhs.initialSpeed &&
           juce::exactlyEqual(replayFrequencyHz, rhs.replayFrequencyHz) &&
           digiChannel == rhs.digiChannel &&
           highlightSpacing == rhs.highlightSpacing &&
           secondaryHighlight == rhs.secondaryHighlight;
}

bool Properties::operator!=(const Properties& rhs) const
{
    return !(rhs == *this);
}

const juce::String& Properties::getName() const
{
    return name;
}

int Properties::getInitialSpeed() const
{
    return initialSpeed;
}

float Properties::getReplayFrequencyHz() const
{
    return replayFrequencyHz;
}

int Properties::getDigiChannel() const
{
    return digiChannel;
}

int Properties::getHighlightSpacing() const
{
    return highlightSpacing;
}

int Properties::getSecondaryHighlight() const
{
    return secondaryHighlight;
}

void Properties::setMetadataToSubsong(Subsong& subsong) const noexcept
{
    subsong.setName(name);
    subsong.setInitialSpeed(initialSpeed);
    subsong.setReplayFrequency(replayFrequencyHz);
    subsong.setDigiChannel(digiChannel);
    subsong.setHighlightSpacings(highlightSpacing, secondaryHighlight);
}

}   // namespace arkostracker
