#pragma once

namespace arkostracker
{

/** A speed for value modification. */
enum class Speed : unsigned char
{
    normal,
    fast,
    fastest,
    limit,
};

}   // namespace arkostracker

