#include "SourceProfileXmlNodes.h"

namespace arkostracker 
{

const juce::String SourceProfileXmlNodes::sourceProfilesRoot = "sourceProfiles";                        // NOLINT(cert-err58-cpp, *-statically-constructed-objects)
const juce::String SourceProfileXmlNodes::sourceProfileRoot = "sourceProfile";                          // NOLINT(cert-err58-cpp, *-statically-constructed-objects)
const juce::String SourceProfileXmlNodes::name = "name";                                                // NOLINT(cert-err58-cpp, *-statically-constructed-objects)
const juce::String SourceProfileXmlNodes::readOnly = "readOnly";                                        // NOLINT(cert-err58-cpp, *-statically-constructed-objects)
const juce::String SourceProfileXmlNodes::littleEndian = "littleEndian";                                // NOLINT(cert-err58-cpp, *-statically-constructed-objects)
const juce::String SourceProfileXmlNodes::generateComments = "generateComments";                        // NOLINT(cert-err58-cpp, *-statically-constructed-objects)
const juce::String SourceProfileXmlNodes::commentDeclaration = "commentDeclaration";                    // NOLINT(cert-err58-cpp, *-statically-constructed-objects)
const juce::String SourceProfileXmlNodes::addressChangeDeclaration = "addressChangeDeclaration";        // NOLINT(cert-err58-cpp, *-statically-constructed-objects)
const juce::String SourceProfileXmlNodes::addressDeclaration = "addressDeclaration";                    // NOLINT(cert-err58-cpp, *-statically-constructed-objects)
const juce::String SourceProfileXmlNodes::byteDeclaration = "byteDeclaration";                          // NOLINT(cert-err58-cpp, *-statically-constructed-objects)
const juce::String SourceProfileXmlNodes::wordDeclaration = "wordDeclaration";                          // NOLINT(cert-err58-cpp, *-statically-constructed-objects)
const juce::String SourceProfileXmlNodes::labelDeclaration = "labelDeclaration";                        // NOLINT(cert-err58-cpp, *-statically-constructed-objects)
const juce::String SourceProfileXmlNodes::stringDeclaration = "stringDeclaration";                      // NOLINT(cert-err58-cpp, *-statically-constructed-objects)
const juce::String SourceProfileXmlNodes::binaryFileExtension = "binaryFileExtension";                  // NOLINT(cert-err58-cpp, *-statically-constructed-objects)
const juce::String SourceProfileXmlNodes::sourceFileExtension = "sourceFileExtension";                  // NOLINT(cert-err58-cpp, *-statically-constructed-objects)
const juce::String SourceProfileXmlNodes::tabulationLength = "tabulationLength";                        // NOLINT(cert-err58-cpp, *-statically-constructed-objects)

}   // namespace arkostracker
