#pragma once

#include <unordered_map>

#include "../../../song/cells/EffectError.h"

namespace arkostracker 
{

class CellEffects;

/** Checks, corrects, normalizes the effects in a Cell. */
class CellEffectsChecker
{
public:
    /**
     * Checks if the given effects are error free.
     * @param effectsToCheck the effects to check.
     * @return the error, for each index. Empty if no error is present.
     */
    static std::unordered_map<int, EffectError> checkEffects(const CellEffects& effectsToCheck) noexcept;

    /**
     * Corrects the given CellEffects.
     * @param effectsToCorrect the effects to correct.
     * @return the effects, corrected or similar.
     */
    static CellEffects correctEffects(const CellEffects& effectsToCorrect) noexcept;

    /**
     * Normalizes the given CellEffects. Removes the effects in error, and order them so that they are always encoded the same way.
     * @param cellEffects the CellEffects.
     * @return the Cell Effects, corrected, normalized.
     */
    static CellEffects normalize(const CellEffects& cellEffects) noexcept;
};

}   // namespace arkostracker
