#include "PatternOptimizer.h"

#include "../../../../song/Song.h"
#include "../../../../utils/TaggedObject.h"
#include "../CellOperations.h"

namespace arkostracker 
{

using patternAndIndex = TaggedObject<int, Pattern>;

PatternOptimizer::PatternOptimizer(const Song& pOriginalSong, std::vector<Id> pSubsongIds) noexcept :
        originalSong(pOriginalSong),
        originalSubsongIds(std::move(pSubsongIds)),
        originalPositions(),
        originalPatterns(),
        originalTracks(),
        originalSpeedTracks(),
        originalEventTracks(),
        sortedTracks(),
        sortedSpeedTracks(),
        sortedEventTracks()
{
}

std::unique_ptr<Song> PatternOptimizer::optimize(const bool keepOnlyTillSubsongLoopEnd, const bool normalizeCells) noexcept
{
    if (!originalPositions.empty()) {
        jassertfalse;           // This method must only be called once!
        return nullptr;
    }

    auto newSong = std::make_unique<Song>(originalSong.getName(), originalSong.getAuthor(), originalSong.getComposer(), originalSong.getComments());

    for (const auto& originalSubsongId : originalSubsongIds) {
        // First, gets the Tracks that are used, stores them from highest to smallest according to the pattern height, removing their content if needed.
        std::unordered_map<int, int> sortedTrackIndexToNew;     // Links the index of the original Track into the index of the new one.
        std::vector<Track> finalTracks;                         // The Tracks to use in the final Song, in the right order.
        std::vector<SpecialTrack> finalSpeedTracks;             // The SpeedTracks to use in the final Song, in the right order.
        std::unordered_map<int, int> sortedSpeedTrackIndexToNew;     // Links the index of the original SpeedTrack into the index of the new one.
        std::vector<SpecialTrack> finalEventTracks;             // The SpeedTracks to use in the final Song, in the right order.
        std::unordered_map<int, int> sortedEventTrackIndexToNew;     // Links the index of the original EventTrack into the index of the new one.
        std::vector<Pattern> finalPatterns;                     // The Patterns to use in the final Song, in the right order.

        originalSong.performOnConstSubsong(originalSubsongId, [&](const Subsong& subsong) noexcept {
            originalPositions = subsong.getPositions(keepOnlyTillSubsongLoopEnd);
            originalPatterns = subsong.getPatterns();
            originalTracks = subsong.getTracks();
            originalSpeedTracks = subsong.getSpecialTracks(true);
            originalEventTracks = subsong.getSpecialTracks(false);
        });

        createSortedAndCorrectedAndReducedTracks(normalizeCells);

        // Creates new Patterns using the new Tracks, if their original Track were included to any of them.
        std::vector<Position> finalPositions;
        finalPositions.reserve(originalPositions.size());

        std::vector<patternAndIndex> newPatternsAndIndexes;

        for (const auto& originalPosition : originalPositions) {
            std::vector<int> newPatternTrackIndexes;

            const auto positionHeight = originalPosition.getHeight();
            const auto& originalPattern = originalPatterns.at(static_cast<size_t>(originalPosition.getPatternIndex()));
            for (const auto originalTrackIndex : originalPattern.getCurrentTrackIndexes()) {
                const auto finalTrackIndex = findOptimizedTrack(sortedTracks, sortedTrackIndexToNew, originalTrackIndex, originalTracks,
                                                                normalizeCells, positionHeight, finalTracks);
                if (finalTrackIndex.isAbsent()) {
                    jassertfalse;       // Abnormal!! Should never happen!
                    return nullptr;
                }

                newPatternTrackIndexes.emplace_back(finalTrackIndex.getValue());
            }
            // The same for the Speed Track.
            const auto originalSpeedTrackIndex = originalPattern.getCurrentSpecialTrackIndex(true);
            const auto newSpeedTrackIndex = findOptimizedTrack(sortedSpeedTracks, sortedSpeedTrackIndexToNew, originalSpeedTrackIndex,
                                                               originalSpeedTracks, normalizeCells, positionHeight, finalSpeedTracks);
            // The same for the Event Track.
            const auto originalEventTrackIndex = originalPattern.getCurrentSpecialTrackIndex(false);
            const auto newEventTrackIndex = findOptimizedTrack(sortedEventTracks, sortedEventTrackIndexToNew, originalEventTrackIndex,
                                                               originalEventTracks, normalizeCells, positionHeight, finalEventTracks);
            if (newSpeedTrackIndex.isAbsent() || newEventTrackIndex.isAbsent()) {
                jassertfalse;       // Abnormal!! Should never happen!
                return nullptr;
            }

            Pattern newPattern(newPatternTrackIndexes, newSpeedTrackIndex.getValue(), newEventTrackIndex.getValue(), originalPattern.getArgbColor());

            // Is the Pattern new?
            int newPatternIndex;        // NOLINT(*-init-variables)
            if (auto it = std::find_if(newPatternsAndIndexes.begin(), newPatternsAndIndexes.end(), [&](const patternAndIndex& item) {
                return (item.getObjectRef().areEqualMusically(newPattern));     // Don't compare the color!
            }); it == newPatternsAndIndexes.end()) {
                // New pattern. Its "id" is its index, which is the size.
                newPatternIndex = static_cast<int>(newPatternsAndIndexes.size());
                newPatternsAndIndexes.emplace_back(newPattern, newPatternIndex);

                finalPatterns.push_back(newPattern);
            } else {
                // Already known Pattern. The tag is its index.
                newPatternIndex = it->getTag();
            }

            // Creates a new Position which references the Pattern, old or new.
            finalPositions.emplace_back(newPatternIndex, positionHeight, originalPosition.getMarkerName(), originalPosition.getMarkerColor(),
                                      originalPosition.getChannelToTransposition());
        }
        jassert(finalPositions.size() == originalPositions.size());

        // Creates the Subsong.
        const auto originalSubsongMetadata = originalSong.getSubsongMetadata(originalSubsongId);
        const auto originalSubsongPsgs = originalSong.getSubsongPsgs(originalSubsongId);
        auto newSubsong = std::make_unique<Subsong>(originalSubsongMetadata.getName(), originalSubsongMetadata.getInitialSpeed(),
                                                    originalSubsongMetadata.getReplayFrequencyHz(),
                                                    originalSubsongMetadata.getDigiChannel(), originalSubsongMetadata.getHighlightSpacing(),
                                                    originalSubsongMetadata.getSecondaryHighlight(), 0, 0, // See why below.
                                                    originalSubsongPsgs, false);
        // Encodes the Tracks/Patterns/Positions in the output Song.
        for (const auto& track : finalTracks) {
            newSubsong->addTrack(track);
        }
        for (const auto& specialTrack : finalSpeedTracks) {
            newSubsong->addSpecialTrack(true, specialTrack);
        }
        for (const auto& specialTrack : finalEventTracks) {
            newSubsong->addSpecialTrack(false, specialTrack);
        }

        for (const auto& pattern : finalPatterns) {
            newSubsong->addPattern(pattern);
        }
        // This DOES change the loop, so it is set after.
        for (const auto& position : finalPositions) {
            newSubsong->addPosition(position);
        }
        newSubsong->setLoopAndEndStartPosition(originalSubsongMetadata.getLoopStartPosition(), originalSubsongMetadata.getEndPosition());

        // Finally, duplicates all the Instruments/Expressions from the original song.
        originalSong.performOnConstInstruments([&](const std::vector<std::unique_ptr<Instrument>>& instruments) {
            for (const auto& instrument : instruments) {
                auto newInstrument = std::make_unique<Instrument>(*instrument);
                newSong->addInstrument(std::move(newInstrument));
            }
        });
        originalSong.getConstExpressionHandler(true).performOnConstExpressions([&](const std::vector<std::unique_ptr<Expression>>& expressions) {
            for (const auto& expression : expressions) {
                newSong->getExpressionHandler(true).addExpression(*expression);
            }
        });
        originalSong.getConstExpressionHandler(false).performOnConstExpressions([&](const std::vector<std::unique_ptr<Expression>>& expressions) {
            for (const auto& expression : expressions) {
                newSong->getExpressionHandler(false).addExpression(*expression);
            }
        });

        newSong->addSubsong(std::move(newSubsong));
    }

    return newSong;
}

void PatternOptimizer::createSortedAndCorrectedAndReducedTracks(const bool normalizeCells) noexcept
{
    for (const auto& position : originalPositions) {
        const auto positionHeight = position.getHeight();
        const auto patternIndex = static_cast<size_t>(position.getPatternIndex());
        const auto& pattern = originalPatterns.at(patternIndex);
        for (const auto trackIndex : pattern.getCurrentTrackIndexes()) {
            // Duplicates the Track, resizing it to what the Position requires.
            const auto& originalTrack = originalTracks.at(static_cast<size_t>(trackIndex));
            auto normalizedTrack = normalizeTrack(originalTrack, normalizeCells, positionHeight);

            sortedTracks.push_back(normalizedTrack);
        }
        // The same for the Special Tracks.
        const auto speedTrackIndex = pattern.getCurrentSpecialTrackIndex(true);
        const auto reducedSpeedTrack = originalSpeedTracks.at(static_cast<size_t>(speedTrackIndex)).reducedTo(positionHeight);
        sortedSpeedTracks.push_back(reducedSpeedTrack);

        const auto eventTrackIndex = pattern.getCurrentSpecialTrackIndex(false);
        const auto reducedEventTrack = originalEventTracks.at(static_cast<size_t>(eventTrackIndex)).reducedTo(positionHeight);
        sortedEventTracks.push_back(reducedEventTrack);
    }

    sortTracks(sortedTracks);
    sortTracks(sortedSpeedTracks);
    sortTracks(sortedEventTracks);
}

Track PatternOptimizer::normalizeTrack(const Track& originalTrack, const bool normalizeCells, const int positionHeight) noexcept
{
    auto reducedTrack = originalTrack.reducedTo(positionHeight);

    // Corrects the track by normalizing each Cell, if wanted.
    if (normalizeCells) {
        for (auto cellIndex = 0; cellIndex < positionHeight; ++cellIndex) {
            constexpr NormalizeCell normalizeCell;
            const auto& cell = reducedTrack.getCellRefConst(cellIndex);
            const auto normalizedCell = normalizeCell(cell);
            if (normalizedCell != nullptr) {        // Any change?
                reducedTrack.setCell(cellIndex, *normalizedCell);
            }
        }
    }

    return reducedTrack;
}

SpecialTrack PatternOptimizer::normalizeTrack(const SpecialTrack& originalTrack, bool /*normalizeCells*/, const int positionHeight) noexcept
{
    return originalTrack.reducedTo(positionHeight);
}

}   // namespace arkostracker
