#include "ChannelPlayerResults.h"

namespace arkostracker 
{

ChannelPlayerResults::ChannelPlayerResults(const OptionalInt pNewPlayedNote, const bool pEffectDeclared, OptionalId pInstrumentId,
                                           std::unique_ptr<ChannelOutputRegisters> pChannelOutputRegisters, SamplePlayInfo pSamplePlayInfo,
                                           const int pPlayedIndexInInstrument) noexcept :
        newPlayedNote(pNewPlayedNote),
        effectDeclared(pEffectDeclared),
        instrumentId(std::move(pInstrumentId)),
        channelOutputRegisters(std::move(pChannelOutputRegisters)),
        samplePlayInfo(std::move(pSamplePlayInfo)),
        playedIndexInInstrument(pPlayedIndexInInstrument)
{
}

ChannelPlayerResults::ChannelPlayerResults() noexcept :
        newPlayedNote(),
        effectDeclared(false),
        instrumentId(),
        channelOutputRegisters(std::make_unique<ChannelOutputRegisters>()),
        samplePlayInfo(SamplePlayInfo::buildNoSample()),
        playedIndexInInstrument(0)
{
}

bool ChannelPlayerResults::isNewNotePlayed() const noexcept
{
    return newPlayedNote.isPresent();
}

OptionalInt ChannelPlayerResults::getNewPlayedNote() const noexcept
{
    return newPlayedNote;
}

bool ChannelPlayerResults::isEffectDeclared() const noexcept
{
    return effectDeclared;
}

OptionalId ChannelPlayerResults::getInstrumentId() const noexcept
{
    return instrumentId;
}

const ChannelOutputRegisters& ChannelPlayerResults::getChannelOutputRegisters() const noexcept
{
    return *channelOutputRegisters;
}

const SamplePlayInfo& ChannelPlayerResults::getSamplePlayInfo() const noexcept
{
    return samplePlayInfo;
}

int ChannelPlayerResults::getPlayedIndexInInstrument() const noexcept
{
    return playedIndexInInstrument;
}

bool ChannelPlayerResults::isPsgDataEqual(const ChannelPlayerResults& other) const noexcept
{
    return ((newPlayedNote == other.newPlayedNote)
            && (effectDeclared == other.effectDeclared)
            && (instrumentId == other.instrumentId)
            && (*channelOutputRegisters == *other.channelOutputRegisters)
    );
}

}   // namespace arkostracker
