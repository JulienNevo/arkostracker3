#pragma once

#include <juce_core/juce_core.h>

enum class Follow: uint8_t
{
    followIfRecordOff,
    followOn,
    followOff,
};