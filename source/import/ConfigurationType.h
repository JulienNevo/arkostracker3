#pragma once

namespace arkostracker 
{

/** The types of a import configuration. */
enum class ConfigurationType
{
    none,
    mod,
    midi,
};


}   // namespace arkostracker

