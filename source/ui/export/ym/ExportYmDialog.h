#pragma once

#include "../../components/dialogs/ModalDialog.h"
#include "../../utils/backgroundOperation/BackgroundOperationWithDialog.h"
#include "../../utils/backgroundTask/BackgroundTaskWithProgress.h"
#include "../common/SubsongChooser.h"

namespace arkostracker 
{

class MainController;
class SongController;

/** Shows a Dialog to export to YM. */
class ExportYmDialog final : public ModalDialog,
                             public BackgroundTaskListener<std::unique_ptr<juce::MemoryOutputStream>>         // The export itself.
{
public:
    /**
     * Constructor.
     * @param mainController the Main Controller.
     * @param listener the listener to close this Dialog.
     */
    ExportYmDialog(MainController& mainController, std::function<void()> listener) noexcept;

    // BackgroundTaskListener method implementations.
    // ======================================================
    void onBackgroundTaskFinished(TaskOutputState taskOutputState, std::unique_ptr<juce::MemoryOutputStream> result) noexcept override;

private:
    /**
     * Called when the operation finishes.
     * @param success true if success.
     */
    void onBackgroundOperationFinished(bool success) noexcept;

    /** Called when the Export button is clicked. */
    void onExportButtonClicked() noexcept;
    /** Called when the Cancel button is clicked. */
    void onCancelButtonClicked() const noexcept;

    /** Closes the possible shown Modal Dialog. */
    void closeShownDialog() noexcept;
    /** Exits this dialog by calling the listener. */
    void exit() const noexcept;

    SongController& songController;

    std::function<void()> listener;

    SubsongChooser subsongChooser;

    juce::GroupComponent optionsGroup;                // Group for the interleaved options.
    juce::ToggleButton interleavedToggleButton;       // The interleaved Toggle Button.
    juce::Label interleavedToggleLabel;               // Text for the interleaved Toggle.
    juce::ToggleButton nonInterleavedToggleButton;    // The non-interleaved Toggle Button.
    juce::Label nonInterleavedToggleLabel;            // Text for the non-interleaved Toggle.

    std::unique_ptr<BackgroundTaskWithProgress<std::unique_ptr<juce::MemoryOutputStream>>> progressTask;
    std::unique_ptr<BackgroundOperationWithDialog<bool>> saveOperation;

    std::unique_ptr<ModalDialog> dialog;

    juce::File fileToSaveTo;
};

}   // namespace arkostracker
