#include "ExportYmDialog.h"

#include "../../../controllers/MainController.h"
#include "../../../export/ym/YmExporter.h"
#include "../../../utils/FileExtensions.h"
#include "../../components/FileChooserCustom.h"
#include "../../components/dialogs/SuccessOrErrorDialog.h"
#include "../common/task/SaveStreamToFile.h"

namespace arkostracker 
{

ExportYmDialog::ExportYmDialog(MainController& pMainController, std::function<void()> pListener) noexcept :
        ModalDialog(juce::translate("Export to AKY"), 510, 340,
                    [&] { onExportButtonClicked(); },
                    [&] { onCancelButtonClicked(); },
                    true, true),
        songController(pMainController.getSongController()),
        listener(std::move(pListener)),
        subsongChooser(pMainController, nullptr),

        optionsGroup(juce::String(), juce::translate("Encoding options")),
        interleavedToggleButton(juce::translate("Encode register by register (recommended)")),
        interleavedToggleLabel(juce::String(),
                               juce::translate("Each register stream is encoded one after the other. The compression ratio is usually higher. Called \"interleaved\" in YM format terminology.")),
        nonInterleavedToggleButton(juce::translate("Encode frame by frame")),
        nonInterleavedToggleLabel(juce::String(),
                                  juce::translate("Each frame is encoded one after the other. Default for older YM format, like YM3. Some converters may require this format. Called \"non-interleaved\" in YM format terminology.")),

        progressTask(),
        saveOperation(),
        dialog(),
        fileToSaveTo()
{
    setOkButtonText(juce::translate("Export"));
    setOkButtonWidth(70);
    setCancelButtonText(juce::translate("Close"));

    const auto bounds = getUsableModalDialogBounds();
    const auto left = bounds.getX();
    const auto top = bounds.getY();
    const auto width = bounds.getWidth();
    const auto margins = LookAndFeelConstants::margins;
    const auto groupMarginX = LookAndFeelConstants::groupMarginsX;
    const auto groupMarginY = LookAndFeelConstants::groupMarginsY;
    const auto labelsHeight = LookAndFeelConstants::labelsHeight;

    subsongChooser.setBounds(left, top, width, SubsongChooser::desiredHeight);

    // The options inside a group.
    constexpr auto radioButtonId = 7789;     // Why not?
    optionsGroup.setBounds(left, subsongChooser.getBottom() + (margins * 2), width, 186);
    interleavedToggleButton.setBounds(optionsGroup.getX() + groupMarginX, optionsGroup.getY() + groupMarginY, optionsGroup.getWidth() - 2 * groupMarginX,
                                      labelsHeight);
    interleavedToggleLabel.setBounds(interleavedToggleButton.getX() + margins, interleavedToggleButton.getBottom(), interleavedToggleButton.getWidth() - margins,
                                     40);
    nonInterleavedToggleButton.setBounds(interleavedToggleButton.getX(), interleavedToggleLabel.getBottom() + margins, interleavedToggleLabel.getWidth(),
                                         interleavedToggleButton.getHeight());
    nonInterleavedToggleLabel.setBounds(interleavedToggleLabel.getX(), nonInterleavedToggleButton.getBottom(), interleavedToggleLabel.getWidth(), 60);
    interleavedToggleButton.setRadioGroupId(radioButtonId, juce::NotificationType::dontSendNotification);
    nonInterleavedToggleButton.setRadioGroupId(radioButtonId, juce::NotificationType::dontSendNotification);

    interleavedToggleButton.setToggleState(true, juce::NotificationType::dontSendNotification);

    addComponentToModalDialog(subsongChooser);
    addComponentToModalDialog(interleavedToggleButton);
    addComponentToModalDialog(interleavedToggleLabel);
    addComponentToModalDialog(nonInterleavedToggleButton);
    addComponentToModalDialog(nonInterleavedToggleLabel);
    addComponentToModalDialog(optionsGroup);
}

void ExportYmDialog::onExportButtonClicked() noexcept
{
    // Opens the file picker.
    fileToSaveTo = FileChooserCustom::save(FileChooserCustom::Target::ym, FileExtensions::ymExtensionWithoutDot);
    if (fileToSaveTo.getFullPathName().isEmpty()) {
        return;     // Cancel.
    }

    const auto subsongId = subsongChooser.getSelectedSubsongId();
    const auto song = songController.getSong();
    constexpr auto psgIndex = 0;            // Automatically exports the first PSG.
    const auto interleaved = interleavedToggleButton.getToggleState();

    // Creates the exporter, and the Task to perform it asynchronously.
    auto ymExporterTask = std::make_unique<YmExporter>(song, subsongId, psgIndex, interleaved);

    progressTask = std::make_unique<BackgroundTaskWithProgress<std::unique_ptr<juce::MemoryOutputStream>>>(juce::translate("Exporting..."), *this,
                                                                                                             std::move(ymExporterTask));
    progressTask->performTask();
}

void ExportYmDialog::onCancelButtonClicked() const noexcept
{
    exit();
}

void ExportYmDialog::exit() const noexcept
{
    listener();
}


// BackgroundTaskListener method implementations.
// ======================================================

void ExportYmDialog::onBackgroundTaskFinished(const TaskOutputState taskOutputState, const std::unique_ptr<juce::MemoryOutputStream> result) noexcept
{
    progressTask->clear();

    if (taskOutputState == TaskOutputState::canceled) {
        return;
    }
    if ((taskOutputState == TaskOutputState::error) || (result == nullptr)) {
        dialog = SuccessOrErrorDialog::buildForError(juce::translate("An error occurred while exporting to YM!"), [&] { closeShownDialog(); });
        return;
    }

    // Saves and zips if wanted.
    const auto localFileToSaveTo = fileToSaveTo;
    const auto dataToSave = result->getMemoryBlock();

    saveOperation = std::make_unique<BackgroundOperationWithDialog<bool>>(juce::translate("Export"), juce::translate("Saving..."),
            [&](const bool success) { onBackgroundOperationFinished(success); },
            [&, localFileToSaveTo, dataToSave]() -> bool {
                auto inputStream = std::make_unique<juce::MemoryInputStream>(dataToSave, false);        // No need to copy, dataToSave is already a copy.
                SaveStreamToFile saveStreamToFile(localFileToSaveTo, std::move(inputStream), false);    // YM are never zipped.
                return saveStreamToFile.perform();
            });
    saveOperation->performOperation();
}

void ExportYmDialog::onBackgroundOperationFinished(const bool success) noexcept
{
    // The saving has ended.
    saveOperation.reset();
    jassert(success);

    if (!success) {
        dialog = SuccessOrErrorDialog::buildForError(juce::translate("An error occurred while exporting to YM!"), [&] { closeShownDialog(); });
    } else {
        dialog = SuccessOrErrorDialog::buildForSuccess(juce::translate("Export to YM finished!"), [&] { exit(); });
    }
}


// ======================================================

void ExportYmDialog::closeShownDialog() noexcept
{
    dialog.reset();
}

}   // namespace arkostracker
