#pragma once

#include <memory>

#include "../components/dialogs/ModalDialog.h"

namespace arkostracker 
{

class MainController;

/**
 * Manages the export pages.
 * We consider only one window can be opened at the same time.
 */
class Export
{
public:
    /**
     * Constructor.
     * @param mainController the Main Controller.
     */
    explicit Export(MainController& mainController) noexcept;

    /** Opens the Export to AKG page. */
    void openExportToAkg() noexcept;

    /** Opens the Export to AKY page. */
    void openExportToAky() noexcept;

    /** Opens the Export to AKM page. */
    void openExportToAkm() noexcept;

    /** Opens the Export to YM page. */
    void openExportToYm() noexcept;
    /** Opens the Export to VGM page. */
    void openExportToVgm() noexcept;
    /** Opens the Export to WAV page. */
    void openExportToWav() noexcept;
    /** Opens the Export sfxs page. */
    void openExportSfxs() noexcept;
    /** Opens the Export events page. */
    void openExportEvents() noexcept;
    /** Opens the Export to RAW page. */
    void openExportToRaw() noexcept;

private:
    /** Closes the dialog. */
    void closeDialog() noexcept;

    MainController& mainController;
    std::unique_ptr<ModalDialog> exportDialog;
};

}   // namespace arkostracker
