#pragma once

#include "../../components/dialogs/ModalDialog.h"
#include "../../../export/SongExportResult.h"
#include "../../utils/backgroundTask/BackgroundTaskWithProgress.h"
#include "../common/ExportAs.h"
#include "../common/task/SaveSourceOrBinaryDialog.h"

namespace arkostracker 
{

class PreferencesManager;
class MainController;
class SongController;

/** Dialog to export to AKM. */
class ExportAkmDialog : public ModalDialog,
                        public BackgroundTaskListener<std::unique_ptr<SongExportResult>>
{
public:
    /**
       * Constructor.
       * @param mainController the Main Controller.
       * @param listener the listener to close this Dialog.
       */
    ExportAkmDialog(MainController& mainController, std::function<void()> listener) noexcept;


    // BackgroundTaskListener method implementations.
    // ======================================================
    void onBackgroundTaskFinished(TaskOutputState taskOutputState, std::unique_ptr<SongExportResult> result) noexcept override;

private:
    /** Called when the Export button is clicked. */
    void onExportButtonClicked() noexcept;
    /** Called when the Cancel button is clicked. */
    void onCancelButtonClicked() noexcept;

    /**
     * Called when OK is clicked on final Save Dialog.
     * @param success true if the export was successful.
     */
    void onSaveSourceDialogOkClicked(bool success) noexcept;

    /** The user exited the Failure Dialog. */
    void onFailureDialogExit() noexcept;

    SongController& songController;

    std::function<void()> listener;

    ExportAs exportAsDialog;

    std::unique_ptr<BackgroundTaskWithProgress<std::unique_ptr<SongExportResult>>> backgroundTask;

    juce::File fileToSaveTo;
    std::unique_ptr<ExportAs::Result> exportAsResult;
    std::unique_ptr<ModalDialog> failureDialog;

    std::unique_ptr<SaveSourceOrBinaryDialog> saveSourceOrBinary;
};


}   // namespace arkostracker

