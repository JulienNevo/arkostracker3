#include "Export.h"

#include "akg/ExportAkgDialog.h"
#include "akm/ExportAkmDialog.h"
#include "aky/ExportAkyDialog.h"
#include "events/ExportEventsDialog.h"
#include "raw/ExportRawDialog.h"
#include "sfx/ExportSfxsDialog.h"
#include "vgm/ExportVgmDialog.h"
#include "wav/ExportWavDialog.h"
#include "ym/ExportYmDialog.h"

namespace arkostracker 
{

Export::Export(MainController& pMainController) noexcept :
        mainController(pMainController)
{
}

void Export::openExportToAkg() noexcept
{
    jassert(exportDialog == nullptr);               // Already present?
    exportDialog = std::make_unique<ExportAkgDialog>(mainController, [&] { closeDialog(); });
}

void Export::openExportToAky() noexcept
{
    jassert(exportDialog == nullptr);               // Already present?
    exportDialog = std::make_unique<ExportAkyDialog>(mainController, [&] { closeDialog(); });
}

void Export::openExportToAkm() noexcept
{
    jassert(exportDialog == nullptr);               // Already present?
    exportDialog = std::make_unique<ExportAkmDialog>(mainController, [&] { closeDialog(); });
}

void Export::openExportToYm() noexcept
{
    jassert(exportDialog == nullptr);               // Already present?
    exportDialog = std::make_unique<ExportYmDialog>(mainController, [&] { closeDialog(); });
}

void Export::openExportToVgm() noexcept
{
    jassert(exportDialog == nullptr);               // Already present?
    exportDialog = std::make_unique<ExportVgmDialog>(mainController, [&] { closeDialog(); });
}

void Export::openExportToWav() noexcept
{
    jassert(exportDialog == nullptr);               // Already present?
    exportDialog = std::make_unique<ExportWavDialog>(mainController, [&] { closeDialog(); });
}

void Export::openExportSfxs() noexcept
{
    jassert(exportDialog == nullptr);               // Already present?
    exportDialog = std::make_unique<ExportSfxsDialog>(mainController, [&] { closeDialog(); });
}

void Export::openExportEvents() noexcept
{
    jassert(exportDialog == nullptr);               // Already present?
    exportDialog = std::make_unique<ExportEventsDialog>(mainController, [&] { closeDialog(); });
}

void Export::openExportToRaw() noexcept
{
    jassert(exportDialog == nullptr);               // Already present?
    exportDialog = std::make_unique<ExportRawDialog>(mainController, [&] { closeDialog(); });
}

void Export::closeDialog() noexcept
{
    exportDialog.reset();
}

}   // namespace arkostracker
