#pragma once

#include <memory>

#include "../containerArranger/Panel.h"

#include "controller/MetersController.h"

namespace arkostracker 
{

class MainController;

/** A panel to show the VU-Meters. */
class MetersPanel final : public Panel
{
public:
    /**
     * Constructor.
     * @param mainController the Main Controller.
     * @param listener to get the panel events.
     */
    explicit MetersPanel(MainController& mainController, Listener& listener) noexcept;

    // Panel method implementations.
    // ================================
    PanelType getType() const noexcept override;
    void getKeyboardFocus() noexcept override;

    // Component method implementations.
    // ================================
    void resized() override;

private:
    std::unique_ptr<MetersController> metersController;
};

}   // namespace arkostracker
