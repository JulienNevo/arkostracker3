#pragma once

#include <unordered_set>

namespace arkostracker 
{

/** The MetersController holds the meter views, and interacts with the other controllers. */
class MetersController
{
public:
    /** Destructor. */
    virtual ~MetersController() = default;

    /**
     * Calculates and applies the size and location of all the already created views.
     * @param startX the X where to start drawing.
     * @param startY the Y where to start drawing.
     * @param width the width upon which to draw.
     * @param height the height upon which to draw.
     */
    virtual void updateViewLocations(int startX, int startY, int width, int height) = 0;

    /**
     * The user wants to change the mute state.
     * @param channelIndex the channel which state to change.
     * @param newMuteState true if the channel must be muted.
     */
    virtual void onWantToChangeMuteState(int channelIndex, bool newMuteState) = 0;

    /**
     * The user wants to must all except one channel. But if others are all muted, unmute all.
     * @param channelIndex the target channel index.
     */
    virtual void onWantToAllMuteExcept(int channelIndex) = 0;

    /**
     * Sets the channel count. It will create or delete views.
     * @param channelCount how many channels there are (>0).
     */
    //virtual void setChannelCount(int channelCount) = 0;

    /**
     * Sets the channels to mute. If empty, none will be.
     * @param channelIndexesToMute the channels to mute. May be empty.
     */
    //virtual void setMutedChannels(std::unordered_set<int> channelIndexesToMute) = 0;

    /**
     * Sets a channel to mute or not.
     * @param channelIndex the index of the channel.
     * @param mute true to mute, false to unmute.
     */
    //virtual void setChannelMuteState(int channelIndex, bool mute) = 0;

    /**
     * Solos a channel.
     * @param channelIndexToSolo the index of the channel.
     */
    //virtual void soloChannel(int channelIndexToSolo) = 0;
};

}   // namespace arkostracker

