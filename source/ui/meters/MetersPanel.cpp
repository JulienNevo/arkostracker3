#include "MetersPanel.h"

#include "controller/MetersControllerImpl.h"

namespace arkostracker 
{

MetersPanel::MetersPanel(MainController& pMainController, Listener& pListener) noexcept :
        Panel(pListener),
        metersController(std::make_unique<MetersControllerImpl>(*this, pMainController))
{
}

PanelType MetersPanel::getType() const noexcept
{
    return PanelType::meters;
}

void MetersPanel::resized()
{
    metersController->updateViewLocations(getXInsideComponent(), getYInsideComponent(), getAvailableWidthInComponent(), getAvailableHeightInComponent());
}

void MetersPanel::getKeyboardFocus() noexcept
{
    // Nothing to do, we don't want focus.
}

}   // namespace arkostracker
