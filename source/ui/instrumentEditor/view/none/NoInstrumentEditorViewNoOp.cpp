#include "NoInstrumentEditorViewNoOp.h"

namespace arkostracker 
{

void NoInstrumentEditorViewNoOp::getKeyboardFocus() noexcept
{
    // Nothing to do.
}


}   // namespace arkostracker

