#include "SampleInstrumentEditorViewNoOp.h"

namespace arkostracker
{

void SampleInstrumentEditorViewNoOp::refreshAllUi(SamplePart /*samplePart*/, WaveViewerDisplayedData /*waveViewerDisplayedData*/) noexcept
{
}

void SampleInstrumentEditorViewNoOp::refreshUi(WaveViewerDisplayedData /*waveViewerDisplayedData*/) noexcept
{
}

}   // namespace arkostracker
