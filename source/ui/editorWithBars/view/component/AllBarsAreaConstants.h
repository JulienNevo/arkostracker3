#pragma once

namespace arkostracker 
{

/** Holds some constants about the AllBarsArea. */
class AllBarsAreaConstants
{
public:
    static constexpr auto captionHeight = 40;

    static constexpr auto minimumHeight = 70;
    static constexpr auto normalHeight = 120;
    static constexpr auto largeHeight = 200;
    static constexpr auto largerHeight = 300;
    static constexpr auto veryLargeHeight = 400;
};

}   // namespace arkostracker
