#include "TrackViewMetadata.h"

namespace arkostracker
{

TrackViewMetadata::TrackViewMetadata(int pHighlightStep, int pSecondaryHighlight,
                                     std::unordered_map<Effect, juce::juce_wchar> pEffectToChar,
                                     std::unordered_map<Effect, juce::Colour> pEffectToColor,
                                     std::unordered_map<int, juce::uint32> pInstrumentToColorArgb,
                                     int pArpeggioExpressionCount, int pPitchExpressionCount
) :
        highlightStep(pHighlightStep),
        secondaryHighlight(pSecondaryHighlight),
        effectToChar(std::move(pEffectToChar)),
        effectToColor(std::move(pEffectToColor)),
        instrumentToColorArgb(std::move(pInstrumentToColorArgb)),
        arpeggioExpressionCount(pArpeggioExpressionCount),
        pitchExpressionCount(pPitchExpressionCount)
{
    jassert(highlightStep > 0);
    jassert(pSecondaryHighlight > 0);
}

}   // namespace arkostracker
