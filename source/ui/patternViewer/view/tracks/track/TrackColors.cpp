#include "TrackColors.h"

#include <utility>

namespace arkostracker 
{

TrackColors::TrackColors(CellColors pCellColors, const juce::Colour& pErrorColor, const juce::Colour& pNoNoteColor, const juce::Colour& pLegatoNoteColor) :
        cellColors(std::move(pCellColors)),
        errorColor(pErrorColor),
        noNoteColor(pNoNoteColor),
        legatoNoteColor(pLegatoNoteColor)
{
}


}   // namespace arkostracker

