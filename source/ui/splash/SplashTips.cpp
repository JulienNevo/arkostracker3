#include "SplashTips.h"

#include <BinaryData.h>

#include "../../app/preferences/PreferencesManager.h"
#include "../../controllers/MainController.h"
#include "../lookAndFeel/CustomLookAndFeel.h"
#include "../utils/TextUtil.h"

namespace arkostracker 
{

SplashTips::SplashTips(const MainController& pMainController, std::function<void()> pInputCallback) noexcept :
        CustomResizableWindow("Splash"),
        callback(std::move(pInputCallback)),

        tipsController(pMainController.getCommandManager()),

        contentComponent(),
        logoImage(),
        text(),
        tipImage(),
        showSplashToggleButton(juce::translate("Show this on start-up")),
        versionLabel(),
        okButton(juce::translate("Go!")),
        nextTipButton(juce::translate("Next tip"))
{
}


// SplashDialog method implementations.
// ==========================================================

void SplashTips::show() noexcept
{
    constexpr int desiredWidth = 560;
    constexpr int desiredHeight = 555;

    setOpaque(true);

    logoImage.setImage(juce::ImageFileFormat::loadFrom(BinaryData::LogoWithTextBelow_png, BinaryData::LogoWithTextBelow_pngSize), juce::RectanglePlacement::centred);
    text.setJustificationType(juce::Justification::centred);
    text.setMinimumHorizontalScale(0.7F);       // JUCE original values (inaccessible). Else, TextLayout doesn't calculate the width/height correctly.

    setUpVersionLabel(versionLabel);
    showSplashToggleButton.setToggleState(true, juce::NotificationType::dontSendNotification);

    okButton.onClick = [&] { exitSplash(); };
    nextTipButton.onClick = [&] { showNextTip(); };

    centreWithSize(desiredWidth, desiredHeight);
    contentComponent.setSize(desiredWidth, desiredHeight);
    setContentNonOwned(&contentComponent, false);

    contentComponent.addAndMakeVisible(&logoImage);
    contentComponent.addAndMakeVisible(&text);
    contentComponent.addAndMakeVisible(&showSplashToggleButton);
    contentComponent.addAndMakeVisible(&versionLabel);
    contentComponent.addAndMakeVisible(&okButton);
    contentComponent.addAndMakeVisible(&nextTipButton);

    enterModalState(true, nullptr, false);
    setVisible(true);

    // Shows a tip.
    const auto tip = tipsController.getRandomTip();
    displayTip(tip);
}


// Component method implementations.
// ==========================================================

void SplashTips::resized()
{
    const auto width = getWidth();
    const auto height = getHeight();

    constexpr auto logoDimension = 200;
    constexpr auto labelHeights = 25;
    constexpr auto labelMargin = margins;
    constexpr auto verticalMargin = margins;
    constexpr auto okButtonWidth = 90;
    constexpr auto nextTipButtonWidth = 110;

    // First, the logo at the top.
    logoImage.setBounds((width - logoDimension) / 2, 20, logoDimension, logoDimension);

    // Then the checkbox/version/OK button at the bottom.
    showSplashToggleButton.setBounds(labelMargin, height - labelHeights - verticalMargin, 200, labelHeights);

    versionLabel.setBounds(width - versionLabelWidth - labelMargin, verticalMargin, versionLabelWidth, labelHeights);

    okButton.setBounds((width - okButtonWidth) / 2, height - labelHeights - verticalMargin, okButtonWidth, labelHeights);
    nextTipButton.setBounds(width - nextTipButtonWidth - margins, okButton.getY(), nextTipButtonWidth, labelHeights);

    ResizableWindow::resized();         // Required by the parent.
}

bool SplashTips::keyPressed(const juce::KeyPress& key)
{
    if (key == juce::KeyPress::escapeKey) {
        exitSplash();
        return true;
    }

    return Component::keyPressed(key);
}


// ==========================================================

void SplashTips::exitSplash() noexcept
{
    // Updates the "show on start up" preference.
    const auto showOnStartUp = showSplashToggleButton.getToggleState();
    PreferencesManager::getInstance().setShowSplash(showOnStartUp);

    callback();
}

void SplashTips::showNextTip() noexcept
{
    const auto tip = tipsController.getNextTip();
    displayTip(tip);
}

void SplashTips::displayTip(const Tip& tip) noexcept
{
    const auto availableWidth = getWidth() - 2 * margins;       // Will take as much as possible.
    constexpr auto left = margins;

    const auto& tipText = tip.getText();

    // Locates the text.
    const auto [textWidth, textHeight] = TextUtil::calculateTextSize(tipText, text.getFont(), availableWidth);
    text.setText(tipText, juce::NotificationType::dontSendNotification);
    const auto textX = (availableWidth - textWidth) / 2 + left;

    const auto* imageData = tip.getImageData();
    const auto imageSize = tip.getImageSize();
    if ((imageData == nullptr) || (imageSize == 0)) {
        // No more image for this tip.
        tipImage.reset();

        // The text is centered vertically.
        //const auto textY = logoImage.getBottom();
        const auto textY = (okButton.getY() - logoImage.getBottom() - textHeight) / 2 + logoImage.getBottom();
        text.setBounds(textX, textY, textWidth, textHeight);
        return;
    }

    // The tip image is here.
    const auto textY = logoImage.getBottom() + margins;
    text.setBounds(textX, textY, textWidth, textHeight);

    // Creates the image.
    tipImage = std::make_unique<juce::ImageComponent>();
    const auto imageWidth = getWidth() - 2 * margins;       // Will take as much as possible.
    const auto imageHeight = okButton.getY() - text.getBottom() - margins * 2;
    constexpr auto imageX = left;
    const auto imageY = text.getBottom() + margins;
    tipImage->setBounds(imageX, imageY, imageWidth, imageHeight);
    tipImage->setImagePlacement(juce::RectanglePlacement::Flags::onlyReduceInSize);
    contentComponent.addAndMakeVisible(*tipImage);

    // Displays the tip image.
    const auto imageToDisplay = juce::ImageFileFormat::loadFrom(imageData, static_cast<size_t>(imageSize));
    tipImage->setImage(imageToDisplay);
}

}   // namespace arkostracker
