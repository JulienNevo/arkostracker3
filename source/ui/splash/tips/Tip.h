#pragma once

#include <juce_core/juce_core.h>

namespace arkostracker 
{

/** A tip (consisting of a text and a possible image data). */
class Tip
{
public:
    /**
     * Constructor.
     * @param text the text.
     * @param imageData the image data, if present.
     * @param imageSize the image size, if present.
     */
    explicit Tip(const juce::String& text, const char* imageData = nullptr, int imageSize = 0) noexcept;

    /** @return the text of the tip. */
    const juce::String& getText() const noexcept;
    /** @return the image data, if any */
    const char* getImageData() const noexcept;
    /** @return the image size, if any */
    int getImageSize() const noexcept;

private:
    juce::String text;
    const char* imageData;
    int imageSize;
};

}   // namespace arkostracker
