#pragma once

#include <juce_gui_basics/juce_gui_basics.h>

#include "Tip.h"
#include "../../keyboard/CommandIds.h"

namespace arkostracker 
{

/** Manages the tips. */
class TipsController
{
public:

    /**
     * Constructor.
     * @param applicationCommandManager useful to get the key related to actions.
     */
    explicit TipsController(juce::ApplicationCommandManager& applicationCommandManager) noexcept;

    /** @return a random tip. */
    Tip getRandomTip() const noexcept;

    /** @return the next tip. */
    Tip getNextTip() const noexcept;

private:
    /** @return the tip according to the current internal tip index. If out of bounds, the index is corrected. */
    Tip getTip() const noexcept;

    /**
     * @return a Tip that includes a text and a CommandId.
     * @param text the text, with XXX as a placeholder for the command shortcut.
     * @param imageData the data of the image.
     * @param imageSize the size of the image data.
     * @param commandId the ID of the command.
     */
    Tip buildTip(const juce::String& text, CommandIds commandId, const char* imageData = nullptr, int imageSize = 0) const noexcept;

    /**
     * @return a Tip that includes a text and an image.
     * @param text the text.
     * @param imageData the data of the image.
     * @param imageSize the size of the image data.
     */
    static Tip buildTip(const juce::String& text, const char* imageData = nullptr, int imageSize = 0) noexcept;

    juce::ApplicationCommandManager& applicationCommandManager;
    mutable int lastTipIndex;           // May be out of bounds!
};

}   // namespace arkostracker
