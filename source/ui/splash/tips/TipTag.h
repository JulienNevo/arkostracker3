#pragma once

namespace arkostracker
{

enum TipTag
{
    shortCutsCanBeRedefined,
    moreThan3Channels,

    playSongFromCursor,

    restoreLayout,

    count
};

}   // namespace arkostracker
