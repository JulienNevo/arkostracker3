#include "Tip.h"

namespace arkostracker 
{

Tip::Tip(const juce::String& pText, const char* pImageData, const int pImageSize) noexcept :
        text(juce::translate("Tip of the day:\n") + pText),
        imageData(pImageData),
        imageSize(pImageSize)
{
}

const juce::String& Tip::getText() const noexcept
{
    return text;
}

const char* Tip::getImageData() const noexcept
{
    return imageData;
}

int Tip::getImageSize() const noexcept
{
    return imageSize;
}

}   // namespace arkostracker

