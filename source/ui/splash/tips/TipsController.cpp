#include "TipsController.h"

#include <BinaryData.h>

#include "TipTag.h"

namespace arkostracker 
{

TipsController::TipsController(juce::ApplicationCommandManager& pApplicationCommandManager) noexcept :
        applicationCommandManager(pApplicationCommandManager),
        lastTipIndex(0)
{
}

Tip TipsController::getRandomTip() const noexcept
{
    auto random = juce::Random();
    lastTipIndex = random.nextInt();

    return getTip();
}

Tip TipsController::getNextTip() const noexcept
{
    ++lastTipIndex;
    return getTip();
}

Tip TipsController::getTip() const noexcept
{
    const auto index = static_cast<size_t>(lastTipIndex) % (TipTag::count);
    switch (static_cast<TipTag>(index)) {
        case count: [[fallthrough]];
        default:
            jassertfalse;           // Never supposed to happen.
        case shortCutsCanBeRedefined:
            return buildTip(juce::translate("Almost all the keyboard shortcuts can be redefined! Go to File > Setup > Keyboard mapping."),
                BinaryData::keyboardMapping_png, BinaryData::keyboardMapping_pngSize);
        case moreThan3Channels:
            return Tip(juce::translate("Want to create a song with more than 3 channels?\nGo to Edit > Subsong properties, and add a new PSG at the bottom."),
                BinaryData::addPsg_png, BinaryData::addPsg_pngSize);
        case playSongFromCursor:
            return buildTip(juce::translate("In the pattern viewer, you can play the song from your cursor thanks to XXX."),
                CommandIds::playPatternFromCursorOrBlock);
        case restoreLayout:
            return buildTip(juce::translate("Layouts represent how panels (linker, pattern viewer, etc.) are placed in one page. "
                                            "Click on one or use a keystroke (such as XXX)."),
                CommandIds::restoreLayout1, BinaryData::restoreLayout_png, BinaryData::restoreLayout_pngSize);
    }
}

Tip TipsController::buildTip(const juce::String& text, const char* imageData, const int imageSize) noexcept
{
    return Tip(text, imageData, imageSize);
}

Tip TipsController::buildTip(const juce::String& text, const CommandIds commandId, const char* imageData, int imageSize) const noexcept
{
    const auto* keyMappings = applicationCommandManager.getKeyMappings();
    if (keyMappings == nullptr) {
        jassertfalse;
        return Tip({ });
    }

    juce::String keyText;
    const auto keysPressed = keyMappings->getKeyPressesAssignedToCommand(commandId);
    if (keysPressed.isEmpty()) {
        // There is no key.
        keyText = juce::translate("[Unassigned]");
    } else {
        // Generates a readable description of the keys.
        auto isFirst = true;
        for (const auto& keyPressed : keysPressed) {
            // Adds a separator.
            if (!isFirst) {
                keyText += " or ";
            }
            isFirst = false;
            keyText += "[" + keyPressed.getTextDescription() + "]";
        }
    }

    // Replaces the placeholder.
    const auto newText = text.replace("XXX", keyText);

    return Tip(newText, imageData, imageSize);
}

}   // namespace arkostracker
