#pragma once

namespace arkostracker 
{

/** Horizontal or vertical. */
enum class Orientation {
    /** For items, means they located horizontally (one besides the others). */
    horizontal,
    /** For items, means they located vertically (one on top of the other). */
    vertical
};

}   // namespace arkostracker

