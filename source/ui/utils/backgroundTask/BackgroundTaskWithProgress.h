#pragma once

#include "../../../utils/message/MessageWithOneParameter.h"
#include "../../../utils/task/Task.h"
#include "../../components/dialogs/ProgressDialog.h"
#include "BackgroundTask.h"

namespace arkostracker 
{

/**
 * Manages a background task, returning a result. It shows a Progress Dialog.
 * There is a difference with a "background operation" in that a Task can be canceled and shows progress.
 * Note that the dialog is deleted only when this object is deleted too.
 *
 * WARNING: do NOT reset (via unique_ptr) this directly when the task is done, else a Progress could still
 * be sent, and the parent won't exist anymore. Use the clear method, then reset after a bit.
 */
template<typename RESULT>
class BackgroundTaskWithProgress final : public TaskProgressListener
{
public:
    /**
     * Constructor.
     * @param pTitle the title of the Dialog.
     * @param pListener the listener, called on the UI thread.
     * @param pTask the task to perform on the background thread.
     */
    BackgroundTaskWithProgress(juce::String pTitle, BackgroundTaskListener<RESULT>& pListener, std::unique_ptr<Task<RESULT>> pTask) noexcept :
            messageListenerToProgressReceived(this),
            title(std::move(pTitle)),
            progressDialog(),
            backgroundTask(pListener, std::move(pTask))
    {
        backgroundTask.setProgressListener(this);
    }

    /** Starts the task. Shows a pop-up. */
    void performTask() noexcept
    {
        // This is called on the UI thread.

        // Opens the Progress Dialog.
        jassert(progressDialog == nullptr);
        progressDialog = std::make_unique<ProgressDialog>(title, true, 100, [&] {
            // Cancel clicked.
            backgroundTask.askForCancel();
        });

        // Let the inner object do to job.
        backgroundTask.performTask();
    }

    /** Call this when the task is done, to make sure no more events will be sent. Use this instead of a reset (do it afterward). */
    void clear() noexcept
    {
        backgroundTask.askForCancel();
        progressDialog.reset();
    }


    // TaskProgressListener method implementations.
    // ===============================================
    void onTaskProgressed(int progress, int progressMaximumValue) noexcept override
    {
        // Worker Thread.

        auto* message = new MessageWithOneParameter<std::pair<int, int>>({ progress, progressMaximumValue });     // Managed by JUCE.
        messageListenerToProgressReceived.postMessage(message);
    }

private:
    /**
     * Updates the progress. This MUST be called from the UI thread.
     * @param value the value.
     * @param maximumValue the maximum value (100 for example).
     */
    void showProgressPercent(const int value, const int maximumValue) const noexcept
    {
        // UI thread.
        if (progressDialog != nullptr) {
            progressDialog->setProgressMaximumValue(maximumValue);
            progressDialog->setProgress(value);
        }
    }


    /** Implementation of the MessageListener to the result of the threaded task. */
    class MessageListenerToProgressReceived final : public juce::MessageListener,
                                                    public WithParent<BackgroundTaskWithProgress>
    {
    public:
        explicit MessageListenerToProgressReceived(BackgroundTaskWithProgress* parent) :
                WithParent<BackgroundTaskWithProgress>(*parent)
        {
        }

        void handleMessage(const juce::Message& message) override
        {
            const auto* rightMessage = dynamic_cast<const MessageWithOneParameter<std::pair<int, int>>*>(&message);
            if (rightMessage != nullptr) {
                const auto& data = rightMessage->getData();
                this->parentObject.showProgressPercent(data.first, data.second);
            } else {
                jassertfalse;           // Shouldn't happen!
            }
        }
    };
    MessageListenerToProgressReceived messageListenerToProgressReceived;      // Listener to event from the worker thread. Can be safely used on UI thread.

    juce::String title;

    std::unique_ptr<ProgressDialog> progressDialog;
    BackgroundTask<RESULT> backgroundTask;
};

}   // namespace arkostracker
