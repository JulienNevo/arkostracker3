#include "TextEditorUtil.h"

namespace arkostracker 
{

const juce::String TextEditorUtil::restrictionInt("0123456789");                // NOLINT(cert-err58-cpp, *-statically-constructed-objects)
const juce::String TextEditorUtil::restrictionFloat("0123456789.");             // NOLINT(cert-err58-cpp, *-statically-constructed-objects)
const juce::String TextEditorUtil::restrictionSource("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_0123456789");             // NOLINT(cert-err58-cpp, *-statically-constructed-objects)
const juce::String TextEditorUtil::restrictionHexadecimal("0123456789abcdefABCDEF");             // NOLINT(cert-err58-cpp, *-statically-constructed-objects)
const juce::String TextEditorUtil::restrictionQuickEditPositive("ABCDEFabcdef0123456789 ,");             // NOLINT(cert-err58-cpp, *-statically-constructed-objects)
const juce::String TextEditorUtil::restrictionQuickEditRelative("-ABCDEFabcdef0123456789 ,");             // NOLINT(cert-err58-cpp, *-statically-constructed-objects)

juce::TextEditor::LengthAndCharacterRestriction TextEditorUtil::buildRestrictionForDecimal(int digitCount) noexcept
{
    return { digitCount, restrictionInt };
}

juce::TextEditor::LengthAndCharacterRestriction TextEditorUtil::buildRestrictionForFloatDecimal(int digitCount) noexcept
{
    return { digitCount, restrictionFloat };
}

juce::TextEditor::LengthAndCharacterRestriction TextEditorUtil::buildRestrictionForSource(int digitCount) noexcept
{
    return { digitCount, restrictionSource };
}

juce::TextEditor::LengthAndCharacterRestriction TextEditorUtil::buildRestrictionForHexadecimal(int digitCount) noexcept
{
    return { digitCount, restrictionHexadecimal };
}

std::unique_ptr<juce::TextEditor::LengthAndCharacterRestriction> TextEditorUtil::buildRestrictionForQuickEditPositive(int digitCount) noexcept
{
    return std::make_unique<juce::TextEditor::LengthAndCharacterRestriction>(digitCount, restrictionQuickEditPositive);
}

std::unique_ptr<juce::TextEditor::LengthAndCharacterRestriction> TextEditorUtil::buildRestrictionForQuickEditRelative(int digitCount) noexcept
{
    return std::make_unique<juce::TextEditor::LengthAndCharacterRestriction>(digitCount, restrictionQuickEditRelative);
}

}   // namespace arkostracker
