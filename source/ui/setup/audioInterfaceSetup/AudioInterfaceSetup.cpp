#include "AudioInterfaceSetup.h"

#include "../../../app/preferences/PreferencesManager.h"
#include "../../../controllers/AudioController.h"
#include "../../../controllers/MainController.h"
#include "../../../controllers/MidiController.h"

namespace arkostracker 
{

AudioInterfaceSetup::AudioInterfaceSetup(MainController& pMainController, std::function<void()> pReturnCallback) noexcept :
        ModalDialog(juce::translate("Audio/MIDI interfaces"), 600, 400,
                    [&] { onOkOrCancelButtonClicked(); }, [&] { onOkOrCancelButtonClicked(); }, true),
        mainController(pMainController),
        audioDeviceViewport(),
        audioDeviceSelectorComponent(pMainController.getAudioController().getAudioDeviceManager(),
                                     0, 0, 2, 2,
                                     true, false, true, false),
        returnCallback(std::move(pReturnCallback))
{
    const auto bounds = getUsableModalDialogBounds();
    audioDeviceViewport.setBounds(bounds);
    audioDeviceSelectorComponent.setBounds(bounds);    // We don't actually care, it will resize itself.

    audioDeviceViewport.setViewedComponent(&audioDeviceSelectorComponent, false);
    addComponentToModalDialog(audioDeviceViewport);
}

void AudioInterfaceSetup::onOkOrCancelButtonClicked() const noexcept
{
    // Stores the audio preferences.
    PreferencesManager::getInstance().saveAudioProperties(audioDeviceSelectorComponent);
    // Contrary to the audio, Midi has no callback when a change is performed. So updates it in all cases.
    mainController.getAudioController().getMidiController().scanMidiDevicesAndRegister();

    returnCallback();
}

}   // namespace arkostracker
