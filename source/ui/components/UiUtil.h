#pragma once

#include <memory>

#include <juce_gui_extra/juce_gui_extra.h>

namespace arkostracker
{

/** Some utility class for UI. */
class UiUtil
{
public:
    /**
     * Creates and shows a Bubble MessageComponent near the given Component.
     * @param targetComponent the target Component.
     * @param text the text to show.
     * @param durationMs how to long to show the bubble, in ms.
     * @return the shown bubble.
     */
    static std::unique_ptr<juce::BubbleMessageComponent> createBubble(juce::Component& targetComponent, const juce::String& text, int durationMs = 5000) noexcept;
};

}   // namespace arkostracker
