#pragma once

#include <memory>

#include "ModalDialog.h"
#include "../ProgressBarView.h"

namespace arkostracker 
{

/** A modal dialog which shows a Progress Bar, and optionally a Cancel button. */
class ProgressDialog final : public ModalDialog
{
public:
    /**
     * Constructor.
     * @param title the title.
     * @param showProgressBar true to show the Progress Bar. If false, the callbacks of the progress will still be called.
     * @param progressMaximumValue the progress maximum value. Ignored if there is no progress bar.
     * @param onCancelClicked called when Cancel is clicked, if shown, or nullptr to hide the Cancel button.
    */
    ProgressDialog(const juce::String& title, bool showProgressBar, int progressMaximumValue, std::function<void()> onCancelClicked) noexcept;

    /** Sets the progress. This performs a repaint. */
    void setProgress(int progress) const noexcept;

    /** Sets the progress maximum value. This does NOT perform a repaint. */
    void setProgressMaximumValue(int progressMaximumValue) const noexcept;

private:
    static constexpr auto progressBarHeight = 40;   // Height of the ProgressBar.

    std::unique_ptr<ProgressBarView> progressBar;   // The progress bar.
    std::unique_ptr<juce::Label> progressLabel;     // A progress label, shown if there is no progress bar.
};

}   // namespace arkostracker

