#include "ProgressDialog.h"

#include "../../lookAndFeel/LookAndFeelConstants.h"

namespace arkostracker 
{

ProgressDialog::ProgressDialog(const juce::String& title, const bool showProgressBar, const int progressMaximumValue, std::function<void()> onCancelClicked) noexcept :
    ModalDialog(title, 500, progressBarHeight + 2 * LookAndFeelConstants::margins + ((onCancelClicked == nullptr) ? 70 : 0),
        nullptr, std::move(onCancelClicked), false, true, false),
    progressBar(std::make_unique<ProgressBarView>()),
    progressLabel(std::make_unique<juce::Label>(juce::String(), juce::translate("Working...")))
{
    setCancelButtonWidth(100);

    const auto width = getUsableModalDialogBounds().getWidth();
    const auto margins = LookAndFeelConstants::margins;

    // Shows either ProgressBar or the Label. As a convenience, they both have the same size.
    Component* component;
    if (showProgressBar) {
        component = progressBar.get();
        progressBar->setMaximumProgressValue(progressMaximumValue);
    } else {
        // Else, shows the Label.
        component = progressLabel.get();
        progressLabel->setJustificationType(juce::Justification::centred);
    }
    component->setBounds(margins, margins, width, progressBarHeight);
    addComponentToModalDialog(*component);
}

void ProgressDialog::setProgress(const int progress) const noexcept
{
    progressBar->setProgressValue(progress);
}

void ProgressDialog::setProgressMaximumValue(const int progressMaximumValue) const noexcept
{
    progressBar->setMaximumProgressValue(progressMaximumValue);
}

}   // namespace arkostracker

