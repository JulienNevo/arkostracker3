#include "UiUtil.h"
#include "../lookAndFeel/LookAndFeelConstants.h"

namespace arkostracker
{

std::unique_ptr<juce::BubbleMessageComponent> UiUtil::createBubble(juce::Component& targetComponent, const juce::String& text, const int durationMs) noexcept
{
    const auto textColor = juce::LookAndFeel::getDefaultLookAndFeel().findColour(juce::Label::ColourIds::textColourId);

    auto bubble = std::make_unique<juce::BubbleMessageComponent>(1000);

    juce::AttributedString attributedString;
    attributedString.setText(text);
    attributedString.setJustification(juce::Justification::centred);
    attributedString.setColour(textColor);
    targetComponent.getTopLevelComponent()->addChildComponent(*bubble);
    bubble->showAt(&targetComponent, attributedString, durationMs);

    return bubble;
}

}   // namespace arkostracker
