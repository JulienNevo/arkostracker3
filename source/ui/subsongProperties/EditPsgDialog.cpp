#include "EditPsgDialog.h"

#include "../../app/preferences/PreferencesManager.h"
#include "../../business/period/GeneratePeriods.h"
#include "../../utils/NumberUtil.h"
#include "../components/UiUtil.h"
#include "../lookAndFeel/LookAndFeelConstants.h"
#include "../utils/TextEditorUtil.h"

namespace arkostracker 
{

const int EditPsgDialog::offsetMixingOutputId = 1;

EditPsgDialog::EditPsgDialog(EditPsgDialog::Listener& pListener, const Psg& pPsg, const int pPsgIndex) noexcept :
        ModalDialog(juce::translate("Edit PSG"), 480, 340,
                    [&] { onOkButtonClicked(); },
                    [&] { onCancelButtonClicked(); },
                          true, true),
        listener(pListener),
        psgIndex(pPsgIndex),
        psgTypeGroup(juce::String(), juce::translate("Type")),
        ayToggle(juce::translate("AY (CPC, MSX, Spectrum...)")),
        ymToggle(juce::translate("YM (Atari ST)")),
        psgFrequencyGroup(juce::String(), juce::translate("Frequency")),
        psgFrequencyCpcToggle(juce::translate("1000000 Hz (Amstrad CPC)")),
        psgFrequencyPentagonToggle(juce::translate("1750000 Hz (Pentagon 128k)")),
        psgFrequencyMsxToggle(juce::translate("1773400 Hz (MSX / ZX Spectrum)")),
        psgFrequencyAtariToggle(juce::translate("2000000 Hz (Atari ST)")),
        psgFrequencyCustomToggle(juce::translate("Custom (in Hz):")),
        psgFrequencyCustomTextEditor(),

        referenceFrequencyGroup(juce::String(), juce::translate("Reference frequency")),
        referenceFrequency440Toggle(juce::translate("440 Hz")),
        referenceFrequencyCustomToggle(juce::translate("Custom (in Hz):")),
        referenceFrequencyCustomTextEditor(),

        outputMixGroup(juce::String(), juce::translate("Output mix")),
        outputMixComboBox(),

        restrictionInt(TextEditorUtil::buildRestrictionForDecimal(7)),
        restrictionFloat(TextEditorUtil::buildRestrictionForFloatDecimal(7)),

        copyFrequenciesButton(juce::translate("Copy freqs."), juce::translate("Get the frequencies in the clipboard.")),
        messageBubble()
{
    const auto margins = LookAndFeelConstants::margins;
    const auto smallMargins = margins / 2;
    const auto labelsHeight = LookAndFeelConstants::labelsHeight;
    const auto groupMarginsX = LookAndFeelConstants::groupMarginsX;
    const auto groupMarginsY = LookAndFeelConstants::groupMarginsY;

    const auto innerBounds = getUsableModalDialogBounds();
    const auto left = innerBounds.getX();
    const auto top = innerBounds.getY();
    const auto width = innerBounds.getWidth();
    const auto firstColumnWidth = width / 2 - margins / 2 + 60;
    const auto firstColumnContentWidth = firstColumnWidth - groupMarginsX * 2;
    const auto secondColumnX = left + firstColumnWidth + margins / 2;
    const auto secondColumnWidth = width - secondColumnX;
    const auto secondColumnContentWidth = secondColumnWidth - groupMarginsX * 2;
    constexpr auto offsetXCustomEditor = 30;

    // Sets up the PSG type group.
    constexpr auto typeRadioId = 451;
    psgTypeGroup.setBounds(left, top, width, 51);
    ayToggle.setBounds(psgTypeGroup.getX() + groupMarginsX, psgTypeGroup.getY() + groupMarginsY, 240, labelsHeight);
    ymToggle.setBounds(ayToggle.getRight(), ayToggle.getY(), 150, labelsHeight);
    ayToggle.setRadioGroupId(typeRadioId);
    ymToggle.setRadioGroupId(typeRadioId);

    // Sets up the PSG Frequency group.
    constexpr auto psgFrequencyRadioId = typeRadioId + 1;
    constexpr auto interline = 5;
    psgFrequencyGroup.setBounds(left, psgTypeGroup.getBottom() + margins, firstColumnWidth, 195);
    psgFrequencyCpcToggle.setBounds(psgFrequencyGroup.getX() + groupMarginsX, psgFrequencyGroup.getY() + groupMarginsY, firstColumnContentWidth, labelsHeight);
    psgFrequencyPentagonToggle.setBounds(psgFrequencyCpcToggle.getX(), psgFrequencyCpcToggle.getBottom() + interline, firstColumnContentWidth, labelsHeight);
    psgFrequencyMsxToggle.setBounds(psgFrequencyPentagonToggle.getX(), psgFrequencyPentagonToggle.getBottom() + interline, firstColumnContentWidth, labelsHeight);
    psgFrequencyAtariToggle.setBounds(psgFrequencyMsxToggle.getX(), psgFrequencyMsxToggle.getBottom() + interline, firstColumnContentWidth, labelsHeight);
    psgFrequencyCustomToggle.setBounds(psgFrequencyAtariToggle.getX(), psgFrequencyAtariToggle.getBottom() + interline, firstColumnContentWidth, labelsHeight);
    psgFrequencyCustomTextEditor.setBounds(psgFrequencyCustomToggle.getX() + offsetXCustomEditor, psgFrequencyCustomToggle.getBottom(), 100, labelsHeight);
    for (auto* toggle : { &psgFrequencyCpcToggle, &psgFrequencyPentagonToggle, &psgFrequencyMsxToggle, &psgFrequencyAtariToggle, &psgFrequencyCustomToggle}) {
        toggle->setRadioGroupId(psgFrequencyRadioId);
    }

    // Sets up the Reference Frequency group.
    constexpr auto referenceFrequencyRadioId = psgFrequencyRadioId + 1;
    referenceFrequencyGroup.setBounds(secondColumnX, psgFrequencyGroup.getY(), secondColumnWidth, 100);
    referenceFrequency440Toggle.setBounds(referenceFrequencyGroup.getX() + groupMarginsX, referenceFrequencyGroup.getY() + groupMarginsY, secondColumnContentWidth,
                                          labelsHeight);
    referenceFrequencyCustomToggle.setBounds(referenceFrequency440Toggle.getX(), referenceFrequency440Toggle.getBottom(), secondColumnContentWidth, labelsHeight);
    referenceFrequencyCustomTextEditor.setBounds(referenceFrequencyCustomToggle.getX() + offsetXCustomEditor, referenceFrequencyCustomToggle.getBottom(),
                                                 100, labelsHeight);
    referenceFrequency440Toggle.setRadioGroupId(referenceFrequencyRadioId);
    referenceFrequencyCustomToggle.setRadioGroupId(referenceFrequencyRadioId);

    // Sets up the Mixing Output group.
    outputMixGroup.setBounds(secondColumnX, referenceFrequencyGroup.getBottom() + smallMargins, secondColumnWidth, 54);
    outputMixComboBox.setBounds(outputMixGroup.getX() + groupMarginsX, outputMixGroup.getY() + groupMarginsY + 4,
                                100, labelsHeight);

    copyFrequenciesButton.setBounds(left, getButtonsY(), 110, getButtonsHeight());

    // Fills the Views.
    fillViews(pPsg);

    // Listeners.
    psgFrequencyCustomToggle.onClick = [&] { psgFrequencyCustomTextEditor.grabKeyboardFocus(); };
    psgFrequencyCustomTextEditor.onTextChange = [&] { selectPsgFrequencyCustomToggle(); };
    psgFrequencyCustomTextEditor.onFocusLost = [&] { checkAndModifyPsgFrequencyValueAndSelectIfNecessary(); };
    psgFrequencyCustomTextEditor.onReturnKey = [&] { checkAndModifyPsgFrequencyValueAndSelectIfNecessary(); };

    referenceFrequencyCustomToggle.onClick = [&] { referenceFrequencyCustomTextEditor.grabKeyboardFocus(); };
    referenceFrequencyCustomTextEditor.onTextChange = [&] { selectReferenceFrequencyCustomToggle(); };
    referenceFrequencyCustomTextEditor.onFocusLost = [&] { checkAndModifyReferencePsgFrequencyValueAndSelectIfNecessary(); };
    referenceFrequencyCustomTextEditor.onReturnKey = [&] { checkAndModifyReferencePsgFrequencyValueAndSelectIfNecessary(); };

    copyFrequenciesButton.onClick = [&] { onCopyFrequenciesButtonClicked(); };

    // Restricts the Text Editors.
    psgFrequencyCustomTextEditor.setInputFilter(&restrictionInt, false);
    referenceFrequencyCustomTextEditor.setInputFilter(&restrictionFloat, false);

    addComponentToModalDialog(psgTypeGroup);
    addComponentToModalDialog(ayToggle);
    addComponentToModalDialog(ymToggle);
    addComponentToModalDialog(psgFrequencyGroup);
    addComponentToModalDialog(psgFrequencyCpcToggle);
    addComponentToModalDialog(psgFrequencyPentagonToggle);
    addComponentToModalDialog(psgFrequencyMsxToggle);
    addComponentToModalDialog(psgFrequencyAtariToggle);
    addComponentToModalDialog(psgFrequencyCustomToggle);
    addComponentToModalDialog(psgFrequencyCustomTextEditor);
    addComponentToModalDialog(referenceFrequencyGroup);
    addComponentToModalDialog(referenceFrequency440Toggle);
    addComponentToModalDialog(referenceFrequencyCustomToggle);
    addComponentToModalDialog(referenceFrequencyCustomTextEditor);
    addComponentToModalDialog(outputMixGroup);
    addComponentToModalDialog(outputMixComboBox);
    addComponentToModalDialog(copyFrequenciesButton);
}

void EditPsgDialog::onOkButtonClicked() noexcept
{
    // Creates the PSG object from the UI.
    const auto psg = buildPsgFromUi();
    listener.onPsgValidated(psgIndex, psg);
}

Psg EditPsgDialog::buildPsgFromUi() const noexcept
{
    const auto psgType = ayToggle.getToggleState() ? PsgType::ay : PsgType::ym;
    const auto referenceFrequency = referenceFrequency440Toggle.getToggleState() ? 440.0F : referenceFrequencyCustomTextEditor.getText().getFloatValue();
    const auto psgMixingOutput = static_cast<PsgMixingOutput>(outputMixComboBox.getSelectedId() - offsetMixingOutputId);

    auto psgFrequency = psgFrequencyCustomTextEditor.getText().getIntValue();         // Default.
    if (psgFrequencyCpcToggle.getToggleState()) {
        psgFrequency = PsgFrequency::psgFrequencyCPC;
    } else if (psgFrequencyPentagonToggle.getToggleState()) {
        psgFrequency = PsgFrequency::psgFrequencyPentagon128K;
    } else if (psgFrequencyMsxToggle.getToggleState()) {
        psgFrequency = PsgFrequency::psgFrequencyMsx;
    } else if (psgFrequencyAtariToggle.getToggleState()) {
        psgFrequency = PsgFrequency::psgFrequencyAtariST;
    } else {
        jassert(psgFrequencyCustomToggle.getToggleState());         // Problem!!!
    }

    return Psg(psgType, psgFrequency, referenceFrequency, PsgFrequency::defaultSamplePlayerFrequencyHz, psgMixingOutput);
}

void EditPsgDialog::onCancelButtonClicked() noexcept
{
    listener.onPsgNotModified();
}

void EditPsgDialog::fillViews(const Psg& psg) noexcept
{
    // PSG type.
    const auto psgType = psg.getType();
    jassert((psgType == PsgType::ay) || (psgType == PsgType::ym));
    if (psgType == PsgType::ay) {
        ayToggle.setToggleState(true, juce::NotificationType::dontSendNotification);
    } else {
        ymToggle.setToggleState(true, juce::NotificationType::dontSendNotification);
    }

    // PSG frequency.
    const auto psgFrequency = psg.getPsgFrequency();
    if (psgFrequency == PsgFrequency::psgFrequencyCPC) {
        psgFrequencyCpcToggle.setToggleState(true, juce::NotificationType::dontSendNotification);
    } else if (psgFrequency == PsgFrequency::psgFrequencyPentagon128K) {
        psgFrequencyPentagonToggle.setToggleState(true, juce::NotificationType::dontSendNotification);
    } else if (psgFrequency == PsgFrequency::psgFrequencyMsx) {
        psgFrequencyMsxToggle.setToggleState(true, juce::NotificationType::dontSendNotification);
    } else if (psgFrequency == PsgFrequency::psgFrequencyAtariST) {
        psgFrequencyAtariToggle.setToggleState(true, juce::NotificationType::dontSendNotification);
    } else {
        psgFrequencyCustomToggle.setToggleState(true, juce::NotificationType::dontSendNotification);
        psgFrequencyCustomTextEditor.setText(juce::String(psgFrequency));
    }

    // Reference frequency.
    if (const auto referenceFrequency = psg.getReferenceFrequency(); juce::exactlyEqual(referenceFrequency, 440.0F)) {
        referenceFrequency440Toggle.setToggleState(true, juce::NotificationType::dontSendNotification);
    } else {
        referenceFrequencyCustomToggle.setToggleState(true, juce::NotificationType::dontSendNotification);
        referenceFrequencyCustomTextEditor.setText(juce::String(referenceFrequency));
    }

    // Mixing output.
    for (const auto mixingOutput : { PsgMixingOutput::ABC, PsgMixingOutput::ACB, PsgMixingOutput::BAC, PsgMixingOutput::BCA,
                                     PsgMixingOutput::CAB, PsgMixingOutput::CBA,
                                     PsgMixingOutput::threeChannelsToLeft, PsgMixingOutput::threeChannelsToMiddle, PsgMixingOutput::threeChannelsToRight}) {
        const auto id = static_cast<int>(mixingOutput) + offsetMixingOutputId;
        outputMixComboBox.addItem(PsgMixingOutputUtil::psgMixingOutputToDisplayedText(mixingOutput), id);
    }
    const auto selectedMixingOutput = psg.getPsgMixingOutput();
    outputMixComboBox.setSelectedId(static_cast<int>(selectedMixingOutput) + offsetMixingOutputId, juce::NotificationType::dontSendNotification);
}

void EditPsgDialog::checkAndModifyPsgFrequencyValueAndSelectIfNecessary() noexcept
{
    auto psgFrequency = psgFrequencyCustomTextEditor.getText().getIntValue();
    psgFrequency = NumberUtil::correctNumber(psgFrequency, PsgFrequency::minimumPsgFrequency, PsgFrequency::maximumPsgFrequency);

    psgFrequencyCustomTextEditor.setText(juce::String(psgFrequency), false);
}

void EditPsgDialog::selectPsgFrequencyCustomToggle() noexcept
{
    psgFrequencyCustomToggle.setToggleState(true, juce::NotificationType::dontSendNotification);
}

void EditPsgDialog::checkAndModifyReferencePsgFrequencyValueAndSelectIfNecessary() noexcept
{
    auto referenceFrequency = referenceFrequencyCustomTextEditor.getText().getFloatValue();
    referenceFrequency = NumberUtil::correctNumber(referenceFrequency, PsgFrequency::minimumReferenceFrequency, PsgFrequency::maximumReferenceFrequency);

    referenceFrequencyCustomTextEditor.setText(juce::String(referenceFrequency), false);
}

void EditPsgDialog::selectReferenceFrequencyCustomToggle() noexcept
{
    referenceFrequencyCustomToggle.setToggleState(true, juce::NotificationType::dontSendNotification);
}

void EditPsgDialog::onCopyFrequenciesButtonClicked() noexcept
{
    const auto sourceProfile = PreferencesManager::getInstance().getCurrentSourceProfile();
    const auto psg = buildPsgFromUi();

    // Generates the source.
    const auto outputStream = GeneratePeriods::generate(psg.getPsgFrequency(), psg.getReferenceFrequency(), sourceProfile);

    // Saves to clipboard.
    juce::SystemClipboard::copyTextToClipboard(outputStream->getMemoryBlock().toString());

    messageBubble = UiUtil::createBubble(copyFrequenciesButton, juce::translate("The frequencies have been copied to the clipboard, as source."));
}

}   // namespace arkostracker
