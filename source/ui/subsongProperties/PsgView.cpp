#include "PsgView.h"

#include <BinaryData.h>

#include "../lookAndFeel/LookAndFeelConstants.h"

namespace arkostracker 
{

PsgView::PsgView(Listener& pListener) noexcept :
        listener(pListener),
        group(juce::String(), true),
        typeLabel(),
        psgFrequencyLabel(),
        referenceFrequencyLabel(),
        editButton(BinaryData::IconToolboxOn_png, BinaryData::IconToolboxOn_pngSize, juce::translate("Edit the PSG data"),
                   [&] (int, bool, bool) { onEditClicked(); }),
        deleteButton(BinaryData::IconDeleteOn_png, BinaryData::IconDeleteOn_pngSize,
                     juce::translate("Delete the PSG"),
                     [&] (int, bool, bool) { onDeleteClicked(); }),
        addButton(BinaryData::IconNew_png, BinaryData::IconNew_pngSize, juce::translate("Add a new PSG after"),
                  [&](int, bool, bool) { onAddButtonClicked(); }),
        psgIndex(0)
{
    addAndMakeVisible(group);

    group.addComponentToGroup(typeLabel);
    group.addComponentToGroup(psgFrequencyLabel);
    group.addComponentToGroup(referenceFrequencyLabel);
    group.addComponentToGroup(editButton);
    group.addComponentToGroup(deleteButton);
    group.addComponentToGroup(addButton);
}

void PsgView::setDisplayedData(const int currentPsgIndex, const int psgCount, const Psg& psg, bool canDelete) noexcept
{
    psgIndex = currentPsgIndex;
    group.setGroupTitle(juce::translate("Psg ") + juce::String(currentPsgIndex + 1) + "/" + juce::String(psgCount));

    const auto psgTypeText = juce::translate(PsgTypeUtil::psgTypeToDisplayedText(psg.getType())) + " (" +
            PsgMixingOutputUtil::psgMixingOutputToDisplayedText(psg.getPsgMixingOutput()) + ")";
    typeLabel.setText(psgTypeText, juce::NotificationType::dontSendNotification);

    psgFrequencyLabel.setText(juce::translate("Frequency: ") + juce::String(psg.getPsgFrequency()) + "hz", juce::NotificationType::dontSendNotification);
    referenceFrequencyLabel.setText(juce::translate("Reference: ") + juce::String(psg.getReferenceFrequency()) + "hz",
                                    juce::NotificationType::dontSendNotification);

    deleteButton.setVisible(canDelete);
}

void PsgView::onEditClicked() noexcept
{
    listener.onEditPsgButtonClicked(psgIndex);
}

void PsgView::onDeleteClicked() noexcept
{
    listener.onDeletePsgButtonClicked(psgIndex);
}

void PsgView::onAddButtonClicked() noexcept
{
    listener.onAddPsgButtonClicked(psgIndex);
}

// juce::Component method implementations.
// ==========================================

void PsgView::resized()
{
    group.setBounds(0, 0, getWidth(), getHeight());

    const auto innerArea = group.getGroupInnerArea();
    const auto width = innerArea.getWidth();

    const auto labelsHeight = LookAndFeelConstants::labelsHeight;
    const auto left = 0;
    const auto y = 0;
    const auto y2 = y + labelsHeight;
    const auto buttonsWidth = LookAndFeelConstants::buttonImagesWidth;

    typeLabel.setBounds(left, y, 90, labelsHeight);
    psgFrequencyLabel.setBounds(typeLabel.getRight(), y, 180, labelsHeight);
    referenceFrequencyLabel.setBounds(psgFrequencyLabel.getX(), y2, 150, labelsHeight);

    editButton.setBounds(width - buttonsWidth - 20, y, buttonsWidth, labelsHeight);
    deleteButton.setBounds(editButton.getX() - buttonsWidth - 2, y, buttonsWidth, labelsHeight);
    addButton.setBounds(editButton.getX(), y2, buttonsWidth, labelsHeight);

    group.setViewportHeight(referenceFrequencyLabel.getBottom());
}


}   // namespace arkostracker

