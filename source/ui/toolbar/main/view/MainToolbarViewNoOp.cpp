#include "MainToolbarViewNoOp.h"

namespace arkostracker 
{

void MainToolbarViewNoOp::setTimerToDisplay(juce::String /*timer*/) noexcept
{
}

void MainToolbarViewNoOp::setCurrentArrangementNumber(int /*number*/) noexcept
{
}

void MainToolbarViewNoOp::setOctave(int /*octave*/) noexcept
{
}

void MainToolbarViewNoOp::setSpeed(int /*speed*/, int /*bpm*/) noexcept
{
}

void MainToolbarViewNoOp::setSerialIconState(bool /*connected*/, bool /*enabled*/) noexcept
{
}

void MainToolbarViewNoOp::showSerialBubble(const juce::String& /*message*/) noexcept
{
}

void MainToolbarViewNoOp::setHighlightedPlayIcon(PlayButton /*playButtonToHighlight*/) noexcept
{
}

}   // namespace arkostracker

