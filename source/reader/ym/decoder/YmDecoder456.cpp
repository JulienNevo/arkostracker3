#include "YmDecoder456.h"

namespace arkostracker 
{

YmDecoder456::YmDecoder456(juce::InputStream& pInputStream) noexcept :
        YmDecoder(pInputStream),
        interleaved(false),
        iterationCount(-1),
        firstFrameDataIndex(-1)
{
}

bool YmDecoder456::acceptFormatHeader() noexcept
{
    return (checkTag("YM4!LeOnArD!") || checkTag("YM5!LeOnArD!") || checkTag("YM6!LeOnArD!"));
}

YmMetadata YmDecoder456::parseHeader(bool& success) noexcept
{
    // If not YM4, then YM5/6 (the validity of the file has been checked before, so it is safe).
    const bool ym5Or6 = !checkTag("YM4!LeOnArD!");

    YmMetadata ymMetadata;

    const auto totalLength = inputStream.getTotalLength();
    success = (totalLength > 100);         // Empirical... Just to make sure there is data.

    // Reads the frame count.
    success &= inputStream.setPosition(tagSize);
    iterationCount = readBigEndian4Bytes();
    if (!success || (iterationCount <= 0)) {
        return ymMetadata;
    }
    ymMetadata.iterationCount = iterationCount;

    // Reads the song attributes.
    const auto songAttributes = readBigEndian4Bytes();
    interleaved = ((songAttributes & 1) != 0);

    // Reads the digidrum count.
    const auto digidrumCount = inputStream.readShortBigEndian();
    ymMetadata.digidrumCount = digidrumCount;

    // On YM5/6, some more data.
    if (ym5Or6) {
        ymMetadata.psgMasterClockHz = readBigEndian4Bytes();
        ymMetadata.playerReplayHz = inputStream.readShortBigEndian();
        ymMetadata.loopIterationIndex = readBigEndian4Bytes();
        inputStream.readShortBigEndian();           // Skips additional data.
    }

    // Skips the digidrums, if any.
    for (auto digidrumIndex = 0; digidrumIndex < digidrumCount; ++digidrumIndex) {
        const auto digidrumSize = inputStream.readIntBigEndian();
        inputStream.skipNextBytes(digidrumSize);
    }

    // Title, author, comments.
    // Warning! The doc is not clear. It is said the following data is only present in YM5+, but they
    // are still described in the YM4 format, so... I put them anyway. The YM Player sources simply don't support YM4 anymore, so...
    ymMetadata.title = readNTString();
    ymMetadata.author = readNTString();
    ymMetadata.comments = readNTString();

    // The first frame is just after.
    firstFrameDataIndex = (int)inputStream.getPosition();

    success &= (firstFrameDataIndex < totalLength);       // Small security.

    return ymMetadata;
}

bool YmDecoder456::isInterleaved() const noexcept
{
    return interleaved;
}

int YmDecoder456::getEncodedRegisterCountInFrame() const noexcept
{
    return 16;
}

int YmDecoder456::getPostMusicFramesDataSize() const noexcept
{
    return 4;           // Marker "End!" at the end.
}

int YmDecoder456::getIterationCount() const noexcept
{
    return iterationCount;
}

int YmDecoder456::getFirstFrameDataIndex() const noexcept
{
    return firstFrameDataIndex;
}


}   // namespace arkostracker

