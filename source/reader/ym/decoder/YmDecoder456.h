#pragma once

#include "YmDecoder.h"

namespace arkostracker 
{

/** Decoder of the header for YM 4, 5 and 6, because there are only a few differences. */
class YmDecoder456 : public YmDecoder
{
public:

    /**
     * Constructor.
     * @param inputStream the Stream of the YM.
     */
    explicit YmDecoder456(juce::InputStream& inputStream) noexcept;

    // YmDecoder method implementations.
    // ===================================================
    bool acceptFormatHeader() noexcept override;
    YmMetadata parseHeader(bool& success) noexcept override;
    int getEncodedRegisterCountInFrame() const noexcept override;

protected:
    static const int tagSize = 12;                  // The size of the header tag.

    bool isInterleaved() const noexcept override;
    int getPostMusicFramesDataSize() const noexcept override;
    int getIterationCount() const noexcept override;
    int getFirstFrameDataIndex() const noexcept override;

private:
    bool interleaved;                               // True if the encoding of the registers is interleaved or not.
    int iterationCount;                             // The iteration count.
    int firstFrameDataIndex;
};


}   // namespace arkostracker

