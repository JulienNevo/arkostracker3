#include "Tool.h"

int main(const int argc, char* argv[])      // NOLINT(*-avoid-c-arrays,clion-misra-cpp2008-3-1-3)
{
    return arkostracker::Tool::execute(argc, argv);
}
