#include "Tool.h"

#include <juce_core/juce_core.h>

#include "../../export/ym/YmExporter.h"
#include "../../utils/FileUtil.h"
#include "../utils/CommandLineArgumentDescriptor.h"
#include "../utils/CommandLineToolHelper.h"

namespace arkostracker
{

int Tool::execute(int argc, char* argv[])      // NOLINT(*-avoid-c-arrays,clion-misra-cpp2008-3-1-3)
{
    const auto guiInit = CommandLineToolHelper::initJuce();

    // Creates the command line.
    const juce::String description = "Converts to YM any song that can be loaded into Arkos Tracker 3.\n"
                                     "Usage: SongToYm [-s <subsong number>] [-p <psg number>] [-n] <path to input song> <path to output YM>";
    std::vector<CommandLineArgumentDescriptor*> descriptors;

    CommandLineToolHelper commandLineToolHelper;

    const Option optionNonInterleaved = Option::buildOption("n", "non-interleaved");
    CommandLineArgumentDescriptor descriptorOptionNonInterleaved = CommandLineArgumentDescriptor::buildArgumentWithOption(
            juce::translate("Non-interleaved. Default is interleaved."), optionNonInterleaved, false);

    // Uses the helper to declare common parameters.
    commandLineToolHelper.declareSubsongParameter(descriptors);
    commandLineToolHelper.declarePsgParameter(descriptors);
    commandLineToolHelper.declareInputSongParameter(descriptors);

    CommandLineArgumentDescriptor descriptorParameterOutput = CommandLineArgumentDescriptor::buildArgumentWithDirectValue(
            juce::translate("<path to output YM>"), juce::translate("Path and filename to the YM file to create."), true);

    descriptors.push_back(&descriptorOptionNonInterleaved);
    descriptors.push_back(&descriptorParameterOutput);

    // Starts parsing.
    const auto parseResult = commandLineToolHelper.parseAndGetSong(argc, argv, descriptors, description);
    if (parseResult.second == nullptr) {
        return parseResult.first ? 0 : -1;
    }
    const auto song = parseResult.second;

    // Reads the possible Subsong and PSG index.
    const auto subsongId = commandLineToolHelper.getSubsongIdOrWriteError(*song);
    if (subsongId.isAbsent()) {
        return -1;
    }
    const auto psgIndex = commandLineToolHelper.getPsgIndexOrWriteError(*song, subsongId.getValueRef());
    if (psgIndex.isAbsent()) {
        return -1;
    }

    // Not interleaved?
    const auto interleaved = !descriptorOptionNonInterleaved.isPresent();

    // Makes the export.
    YmExporter exporter(song, subsongId.getValue(), psgIndex.getValue(), interleaved);
    const auto result = exporter.performTask();
    if (!result.first) {
        CommandLineToolHelper::cerr(juce::translate("Export to YM failed!"));
        return -1;
    }

    const auto outputFileString = descriptorParameterOutput.getDirectValue();
    const auto success = FileUtil::saveMemoryBlockToFile(outputFileString, result.second->getMemoryBlock());
    if (!success) {
        CommandLineToolHelper::cout(juce::translate("Saving failed!"));
        return -1;
    }

    CommandLineToolHelper::cout(juce::translate("Export to YM successful."));
    return 0;
}

}   // namespace arkostracker
