#include "CommandLineConstants.h"

namespace arkostracker
{

const juce::String CommandLineConstants::stringShortOptionTag("-");         // NOLINT(cert-err58-cpp, *-statically-constructed-objects)
const juce::String CommandLineConstants::stringLongOptionTag("--");         // NOLINT(cert-err58-cpp, *-statically-constructed-objects)

}   // namespace arkostracker
