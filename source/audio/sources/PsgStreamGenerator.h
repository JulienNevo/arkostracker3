#pragma once

#include <array>
#include <mutex>
#include <unordered_set>

#include <juce_audio_basics/juce_audio_basics.h>

#include "../../player/SamplePlayInfo.h"
#include "../../song/psg/PsgMixingOutput.h"
#include "../../song/psg/PsgType.h"
#include "../../utils/CircularBuffer.h"
#include "../Volumes.h"
#include "HardwareTables.h"

namespace arkostracker
{

class PsgRegistersProvider;
class SampleData;

/**
 * AudioSource that generates the signal for one PSG (AY or YM), filling the buffer given to it.
 * It also plays samples.
 */
class PsgStreamGenerator final : public juce::AudioSource
{
public:
    static constexpr auto signalReadBufferSize = 512U;            // How large are each read "signal" buffer, for UI to display it for example.

    /**
     * Constructor.
     * @param psgRegistersProvider provides the registers.
     * @param psgType the type of the PSG (AY, YM).
     * @param psgIndex the index of the PSG (>=0) handled by this generator.
     * @param replayFrequency the replay frequency of the song in Hz (12.5, 25, 50 for example).
     * @param psgFrequency the PSG frequency in Hz (1 000 000 on CPC).
     * @param psgMixingOutput how the mix is performed with the PSG channels.
     * @param pChannelAMixVolume the channel A volume, from 0 to 1.
     * @param pChannelBMixVolume the channel B volume, from 0 to 1.
     * @param pChannelCMixVolume the channel C volume, from 0 to 1.
     */
    PsgStreamGenerator(PsgRegistersProvider& psgRegistersProvider, PsgType psgType, int psgIndex, float replayFrequency, int psgFrequency,
                       PsgMixingOutput psgMixingOutput, double pChannelAMixVolume, double pChannelBMixVolume, double pChannelCMixVolume) noexcept;

    /**
     * Fills the given buffer with the data from the generated signal.
     * Call this on the UI thread, because this is useful for displaying the signal!
     * @param channelIndex the channel index (0-2).
     * @param blockToFill the block to fill. It must be well sized (see signalReadBufferSize).
     */
    void fillSignal(int channelIndex, const juce::HeapBlock<uint16_t>& blockToFill) const noexcept;

    /**
     * Sets the indexes of the muted channels. Called on the UI thread.
     * @param mutedChannelIndexes the indexes of the muted channels. If empty, all the channels are heard.
     */
    void setMutedChannelIndexes(const std::unordered_set<int>& mutedChannelIndexes) noexcept;

    /**
     * @return true if the PSG metadata are the same as the ones that were given when creating this generator.
     * @param psgType the type of the PSG (AY, YM).
     * @param psgFrequency the PSG frequency in Hz (1 000 000 on CPC).
     * @param replayFrequency the replay frequency of the song in Hz (12.5, 25, 50 for example).
     * @param channelAVolume the channel A volume, from 0 to 1.
     * @param channelBVolume the channel B volume, from 0 to 1.
     * @param channelCVolume the channel C volume, from 0 to 1.
     * @param psgMixingOutput how the channels are positioned in the mix.
     */
    bool isPsgMetadataEqual(PsgType psgType, int psgFrequency, float replayFrequency,
                            double channelAVolume, double channelBVolume, double channelCVolume,
                            PsgMixingOutput psgMixingOutput) const noexcept;

    /** @return  the replay frequency, in Hz. */
    float getReplayFrequencyHz() const noexcept;

    // AudioSource method implementations.
    // =============================================
    void prepareToPlay(int samplesPerBlockExpected, double sampleRate) override;
    void releaseResources() override;
    void getNextAudioBlock(const juce::AudioSourceChannelInfo& bufferToFill) override;

private:
    /** How large are each "signal" circular buffer, for UI to display it for example. Should be larger than what is displayed to prevent overlap. */
    static constexpr auto signalBufferSize = signalReadBufferSize * 8U;

    static constexpr auto channelCount = 2U;                      // ONLY stereo is managed!

    /**
     * From the datasheet:
     * "The frequency of each square wave generated by the three Tone Generators (one each for Channels A,B and C)
     * is obtained in the PSG by first counting down the input clock by 16, then by further counting down
     * the result by the programmed 12-bit Tone Period value."
     * We use a divided-by-2 number because we work with half-periods to generate the high/low shelf.
     */
    static constexpr auto periodMultiplier = 8;

    /**
     * Returns a volume from 0 to 16 from a volume from 0 to 32. 16 becomes 32,
     * and a normal volume is (volumeIn * 2 + 1), that is from 1 to 31 (to reach the full volume).
     * However, a volumeIn of 0 remains at 0 to have an electronic 0 value, even though it seems the half volume is actually used.
     * @param volume0To16 the volume from 0-16 (16 is hardware envelope).
     * @return the volume from 0 to 32.
     */
    static int scaleVolumeFrom0To16To0To32(int volume0To16) noexcept;

    /** Generates the non-resampled AY signal for all the channels, filling the generation buffers. */
    void generateSignal() noexcept;

    /** Reads the next PSG registers from the provider. This updates the internal values (periods, volumes, etc.). Called every 50hz, for example. */
    void readNextPsgRegisters() noexcept;

    /** @return true if a hardware envelope is used on channel A. */
    bool isHardwareEnvelopeUsedOnA() const noexcept {
        return volumeAFrom0To31Or32 == 32;
    }
    /** @return true if a hardware envelope is used on channel B. */
    bool isHardwareEnvelopeUsedOnB() const noexcept {
        return volumeBFrom0To31Or32 == 32;
    }
    /** @return true if a hardware envelope is used on channel A. */
    bool isHardwareEnvelopeUsedOnC() const noexcept {
        return volumeCFrom0To31Or32 == 32;
    }

    /**
     * Sets-up the data of the sample, from the given data info. Works for every channel.
     * @param channelIndex the index of the channel (>=0).
     * @param newSampleData the input data, if any.
     * @param sampleRate the input sample rate.
     * @param currentSampleDataInfo output data. the sample data info.
     * @param currentSampleIndex output value. The sample index to fill.
     * @param currentPlaySample output value. True to play the sample.
     * @param currentSampleStep output value. The step.
     */
    static void prepareSample(int channelIndex, const SampleData* newSampleData, double sampleRate, SamplePlayInfo& currentSampleDataInfo, double& currentSampleIndex,
                              bool& currentPlaySample, double& currentSampleStep) noexcept;

    /**
     * Plays one sample.
     * @param sampleDataInfo the input sample data info.
     * @param playSample output data. True to play the sample.
     * @param sampleIndex output data. The offset of the byte to read.
     * @param sampleStep the speed at which the sample is read.
     * @param volume16bitsOut output value. The volume to ultimately play. It must be applied the given volume table.
     * @param sampleRate the replay rate (44100 for example).
     * @param volumeTable the volume table, 32 steps, from 0 to 65535.
     */
    static void playSample(const SamplePlayInfo& sampleDataInfo, bool& playSample, double& sampleIndex, const double& sampleStep, uint16_t& volume16bitsOut,
                           double sampleRate, const std::array<uint16_t, Volumes::volumeCount>& volumeTable);

    /**
     * Mixes the given three channels into left and right, according to the mixing output and channel volume.
     * @param volumeA16Bits the channel A value, 16 bits.
     * @param volumeB16Bits the channel B value, 16 bits.
     * @param volumeC16Bits the channel C value, 16 bits.
     * @param leftVolume16BitsOut the value to fill, for left channel.
     * @param rightVolume16BitsOut the value to fill, for right channel.
     */
    void mixChannels(uint16_t volumeA16Bits, uint16_t volumeB16Bits, uint16_t volumeC16Bits, double& leftVolume16BitsOut, double& rightVolume16BitsOut) const noexcept;

    HardwareTables hardwareTables;                                                  // Holds look-up tables for hardware volumes.

    // -----------------------------------------------
    std::atomic_bool newDataFromUi;                                                 // True when there is new data from the UI pending.
    mutable std::mutex dataFromUiThreadMutex;                                       // Protects data received from the UI thread.
    std::unique_ptr<std::unordered_set<int>> mutedChannelIndexesFromUiThread;       // Present if new muted channels are received from the UI.

    // -----------------------------------------------

    PsgRegistersProvider& psgRegistersProvider;                                     // Can provide with the PSG registers to play.

    const PsgType psgType;
    const int psgIndex;
    const float replayFrequency;
    const int psgFrequency;
    const PsgMixingOutput psgMixingOutput;
    const double channelAMixVolume;                                                 // From 0 to 1, to simulate the 100/91/100 of a CPC headphone for example.
    const double channelBMixVolume;
    const double channelCMixVolume;

    // The sample rate of the audio player, in Hz (44100 for example). WARNING! Only certain when the "prepareToPlay" method has been called!
    double sampleRate;

    double resamplingRatio;
    int generationBufferSize;                                                       // How much to generate each time.
    std::array<juce::HeapBlock<double>, channelCount> generationBuffers;            // Where our non-resampled data is generated, one for each channel.
    std::array<juce::HeapBlock<double>, channelCount> resampledBuffers;             // Where the resampled data is generated, one for each channel.

    std::array<double, channelCount> sumToReportPerChannel;
    std::array<double, channelCount> ratioToReportPerChannel;

    int playerPeriod;                                               // Calculated to know exactly when to send the registers.
    int playerPeriodCounter;                                        // Counter increasing till the playerPeriod is reached.
    int periodCounterA;                                             // Counters increasing till the Period has been reached.
    int periodCounterB;
    int periodCounterC;
    int periodA;                                                    // The Period for channel A.
    int periodB;
    int periodC;

    bool isHighShelfA;                                              // High shelf (true) or Low state (false), according only to the square signal being produced, not the mixer.
    bool isHighShelfB;
    bool isHighShelfC;

    int hardwarePeriodCounter;
    int hardwarePeriod;                                             // Period of the hardware envelope.
    int hardwareEnvelope;                                           // Hardware Envelope used by the Hardware volume. Value from 0 to 0xf. Never contains 0xff.
    int hardwareCurveCounter;                                       // Counter inside the hardware Curve (not called Envelope for disambiguation). From 0 to 63. Loops at 32.

    int noise;                                                      // The noise, from 0 (no noise) to 31.
    int noisePeriod;
    int noisePeriodCounter;

    bool isNoiseHighShelf;
    juce::uint32 noiseSeed;                                         // Used for noise generation.

    bool isMixerSoundAOpen;                                         // True if Mixer bit to 0! Which means "open".
    bool isMixerSoundBOpen;
    bool isMixerSoundCOpen;
    bool isMixerNoiseAOpen;
    bool isMixerNoiseBOpen;
    bool isMixerNoiseCOpen;
    bool isMixerNoiseOnAnyChannel;

    int volumeAFrom0To31Or32;                                       // The volume for channel A, from 0 to 31, or 32 for hardware envelope.
    int volumeBFrom0To31Or32;                                       // The volume for channel B, from 0 to 31, or 32 for hardware envelope.
    int volumeCFrom0To31Or32;                                       // The volume for channel C, from 0 to 31, or 32 for hardware envelope.

    CircularBuffer<uint16_t, signalBufferSize> signalChannelA; // Filled then given to the Listener.
    CircularBuffer<uint16_t, signalBufferSize> signalChannelB;
    CircularBuffer<uint16_t, signalBufferSize> signalChannelC;

    std::unordered_set<int> mutedChannelIndexes;                    // The index of the muted channels.

    SamplePlayInfo channelASampleDataInfo;                          // Sample data for channel A.
    double sampleAIndex;                                            // Where to play on the sample of channel A. Increases.
    double sampleAStep;                                             // How quick to move to one sample byte to another. This is related to the pitch!
    bool playSampleA;                                               // True to play the channel A sample. False when the sample has reached its end.

    SamplePlayInfo channelBSampleDataInfo;
    double sampleBIndex;
    double sampleBStep;
    bool playSampleB;

    SamplePlayInfo channelCSampleDataInfo;
    double sampleCIndex;
    double sampleCStep;
    bool playSampleC;
};

}   // namespace arkostracker
