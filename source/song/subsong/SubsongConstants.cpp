#include "SubsongConstants.h"

namespace arkostracker 
{

const int SubsongConstants::minimumSpeed = 1;
const int SubsongConstants::maximumSpeed = 255;
const int SubsongConstants::defaultSpeed = 6;

const int SubsongConstants::defaultDigiChannel = 1;
const int SubsongConstants::defaultPrimaryHighlight = 4;
const int SubsongConstants::defaultSecondaryHighlight = 4;

}   // namespace arkostracker

