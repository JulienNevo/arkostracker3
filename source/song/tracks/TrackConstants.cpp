#include "TrackConstants.h"

namespace arkostracker 
{

const int TrackConstants::defaultPositionHeight = 64;

const int TrackConstants::maximumCapacity = 128;                     // Maximum Cells a Track can hold.
const int TrackConstants::lastPossibleIndex = maximumCapacity - 1;

}   // namespace arkostracker

