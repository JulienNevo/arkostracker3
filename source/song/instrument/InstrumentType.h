#pragma once

namespace arkostracker 
{

/** Enumeration for a type of an instrument. */
enum class InstrumentType
{
    psgInstrument,
    sampleInstrument
};

}   // namespace arkostracker

