#include "SpreadPsgInstrumentCell.h"
#include "../../cells/CellConstants.h"
#include "../../../utils/NumberUtil.h"
#include "../../../utils/PsgValues.h"

namespace arkostracker 
{

const SpreadPsgInstrumentCell SpreadPsgInstrumentCell::emptyCell(PsgInstrumentCell::getEmptyPsgInstrumentCell(), {});

SpreadPsgInstrumentCell::SpreadPsgInstrumentCell(const PsgInstrumentCell& pCellWithSpread, std::set<PsgSection> pGeneratedSections) noexcept :
        cellWithSpread(pCellWithSpread),
        generatedSections(std::move(pGeneratedSections))
{
}

const SpreadPsgInstrumentCell& SpreadPsgInstrumentCell::getEmptyCell() noexcept
{
    return emptyCell;
}

const std::set<PsgSection>& SpreadPsgInstrumentCell::getGeneratedSectionsRef() const noexcept
{
    return generatedSections;
}

const PsgInstrumentCell& SpreadPsgInstrumentCell::getCellWithAutoSpreadAppliedRef() const noexcept
{
    return cellWithSpread;
}

LowLevelPsgInstrumentCell SpreadPsgInstrumentCell::buildLowLevelCell() const noexcept           // TODO TU this.
{
    // Extracts the data from the reference Cell.
    const auto link = cellWithSpread.getLink();
    const auto volume = cellWithSpread.getVolume();
    const auto noise = cellWithSpread.getNoise();
    const auto ratio = cellWithSpread.getRatio();
    const auto primaryPeriod = cellWithSpread.getPrimaryPeriod();
    const auto primaryArpeggioNoteInOctave = cellWithSpread.getPrimaryArpeggioNoteInOctave();
    const auto primaryArpeggioOctave = cellWithSpread.getPrimaryArpeggioOctave();
    const auto primaryArpeggio = primaryArpeggioNoteInOctave + primaryArpeggioOctave * CellConstants::noteCountInOctave;
    const auto primaryPitch = cellWithSpread.getPrimaryPitch();
    const auto secondaryPeriod = cellWithSpread.getSecondaryPeriod();
    const auto secondaryArpeggioNoteInOctave = cellWithSpread.getSecondaryArpeggioNoteInOctave();
    const auto secondaryArpeggioOctave = cellWithSpread.getSecondaryArpeggioOctave();
    const auto secondaryArpeggio = secondaryArpeggioNoteInOctave + secondaryArpeggioOctave * CellConstants::noteCountInOctave;
    const auto secondaryPitch = cellWithSpread.getSecondaryPitch();
    const auto hardwareEnvelope = cellWithSpread.getHardwareEnvelope();
    const auto retrig = cellWithSpread.isRetrig();

    int softwarePeriod;
    int softwareArpeggio;
    int softwarePitch;
    int hardwarePeriod;
    int hardwareArpeggio;
    int hardwarePitch;

    // Software or hardware ? If Soft is basis, we consider primary = soft.
    if (cellWithSpread.isSoftware() || (link == PsgInstrumentCellLink::softToHard) || (link == PsgInstrumentCellLink::softAndHard)) {
        softwarePeriod = NumberUtil::correctNumber(primaryPeriod, PsgValues::minimumPeriod, PsgValues::maximumSoftwarePeriod);
        softwareArpeggio = primaryArpeggio;
        softwarePitch = primaryPitch;
        hardwarePeriod = NumberUtil::correctNumber(secondaryPeriod, PsgValues::minimumPeriod, PsgValues::maximumHardwarePeriod);
        hardwareArpeggio = secondaryArpeggio;
        hardwarePitch = secondaryPitch;
    } else {
        // Hardware-basis (Hard to Soft, Hard only).
        jassert((link == PsgInstrumentCellLink::hardOnly) || (link == PsgInstrumentCellLink::hardToSoft));

        hardwarePeriod = NumberUtil::correctNumber(primaryPeriod, PsgValues::minimumPeriod, PsgValues::maximumHardwarePeriod);
        hardwareArpeggio = primaryArpeggio;
        hardwarePitch = primaryPitch;
        softwarePeriod = NumberUtil::correctNumber(secondaryPeriod, PsgValues::minimumPeriod, PsgValues::maximumSoftwarePeriod);
        softwareArpeggio = secondaryArpeggio;
        softwarePitch = secondaryPitch;
    }

    return { link, volume, noise,
             softwarePeriod, softwareArpeggio, softwarePitch,
             ratio, hardwareEnvelope,
             hardwarePeriod, hardwareArpeggio, hardwarePitch, retrig };
}

}   // namespace arkostracker
