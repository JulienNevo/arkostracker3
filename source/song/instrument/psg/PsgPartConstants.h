#pragma once

namespace arkostracker
{

class PsgPartConstants
{
public:
    static constexpr auto minimumSpeed = 0;
    static constexpr auto maximumSpeed = 255;

    static constexpr auto slowestSpeed = maximumSpeed;
};

}   // namespace arkostracker
