#include "SongTxtExporter.h"

#include "../../song/Song.h"
#include "../../utils/StreamUtil.h"
#include "../../utils/XmlHelper.h"
#include "../at3/SongExporter.h"

namespace arkostracker
{

const juce::String SongTxtExporter::firstLineHeader = "Arkos Tracker text format V1.0";             // NOLINT(cert-err58-cpp, *-statically-constructed-objects)
const juce::String SongTxtExporter::sectionBase = "SECTION ";                                       // NOLINT(cert-err58-cpp, *-statically-constructed-objects)
const juce::String SongTxtExporter::sectionIndentation = "-";                                       // NOLINT(cert-err58-cpp, *-statically-constructed-objects)
const juce::String SongTxtExporter::keyValueSeparator = " ";                                        // NOLINT(cert-err58-cpp, *-statically-constructed-objects)

std::unique_ptr<juce::MemoryOutputStream> SongTxtExporter::exportSong(const Song& song) noexcept
{
    auto outputStream = std::make_unique<juce::MemoryOutputStream>(16384);

    // Instead of re-doing the XML job, simply uses it and produces TXT instead.
    const auto xmlRootNode = SongExporter::exportSong(song);
    if (xmlRootNode == nullptr) {
        jassertfalse;
        return nullptr;
    }

    // Format header.
    write(*outputStream, firstLineHeader);
    writeEmptyLine(*outputStream);

    encodeNode(*outputStream, *xmlRootNode, 0);

    return outputStream;
}

void SongTxtExporter::writeKeyValues(juce::MemoryOutputStream& outputStream, const juce::XmlElement& node) noexcept
{
    for (const auto& [key, value] : XmlHelper::readTextOnlyNodes(node)) {
        write(outputStream, key, value);
    }
}

void SongTxtExporter::write(juce::MemoryOutputStream& outputStream, const juce::String& line) noexcept
{
    StreamUtil::write(outputStream, line);
}

void SongTxtExporter::write(juce::MemoryOutputStream& outputStream, const juce::String& key, const juce::String& value) noexcept
{
    StreamUtil::write(outputStream, key + keyValueSeparator + value);
}

void SongTxtExporter::write(juce::MemoryOutputStream& outputStream, const juce::String& key, juce::int64 value) noexcept
{
    StreamUtil::write(outputStream, key + keyValueSeparator + juce::String(value));
}

void SongTxtExporter::writeSection(juce::MemoryOutputStream& outputStream, const juce::String& sectionName, int indentationLevel) noexcept
{
    for (auto indentationIndex = 0; indentationIndex < indentationLevel; ++indentationIndex) {
        StreamUtil::write(outputStream, sectionIndentation, false);
    }
    StreamUtil::write(outputStream, sectionBase + sectionName);
}

void SongTxtExporter::writeEmptyLine(juce::MemoryOutputStream& outputStream) noexcept
{
    StreamUtil::write(outputStream, juce::String());
}

void SongTxtExporter::encodeNode(juce::MemoryOutputStream& outputStream, const juce::XmlElement& rootNode, int indentationLevel) noexcept     // NOLINT(*-no-recursion)
{
    // Recursive method.
    writeSection(outputStream, rootNode.getTagName(), indentationLevel);
    writeKeyValues(outputStream, rootNode);
    writeEmptyLine(outputStream);

    for (auto childIndex = 0, childCount = rootNode.getNumChildElements(); childIndex < childCount; ++childIndex) {
        const auto* childNode = rootNode.getChildElement(childIndex);
        if (!XmlHelper::hasOnlyText(*childNode)) {
            encodeNode(outputStream, *childNode, indentationLevel + 1);
        }
    }
}

}   // namespace arkostracker
