#pragma once

#include <juce_core/juce_core.h>

namespace arkostracker
{

class Song;

/**
 * Exports the Song as a human-readable text. The nodes are the same as in XML.
 * THIS IS ACTUALLY NOT USED... I'm not sure it is useful.
 */
class SongTxtExporter
{
public:
    /** Prevents instantiation. */
    SongTxtExporter() = delete;

    /**
     * Exports a Song to a human-readable text.
     * @param song the Song.
     * @return the text, or nullptr if an error occurred (shouldn't happen).
     */
    static std::unique_ptr<juce::MemoryOutputStream> exportSong(const Song& song) noexcept;

private:
    static const juce::String firstLineHeader;
    static const juce::String sectionBase;
    static const juce::String sectionIndentation;

    static const juce::String keyValueSeparator;

    /** Writes a line. */
    static void write(juce::MemoryOutputStream& outputStream, const juce::String& line) noexcept;
    /** Writes a key/value. */
    static void write(juce::MemoryOutputStream& outputStream, const juce::String& key, const juce::String& value) noexcept;
    /** Writes a key/value (int). */
    static void write(juce::MemoryOutputStream& outputStream, const juce::String& key, juce::int64 value) noexcept;

    /**
     * Writes a section (only the header).
     * @param outputStream where to write.
     * @param sectionName the name of the section. Prefix will be added.
     * @param indentationLevel the indentation level on the section (>=0).
     */
    static void writeSection(juce::MemoryOutputStream& outputStream, const juce::String& sectionName, int indentationLevel) noexcept;
    /** Writes an empty line. */
    static void writeEmptyLine(juce::MemoryOutputStream& outputStream) noexcept;

    /** Writes key/values read in the given node. Only simple texts are retrieved. */
    static void writeKeyValues(juce::MemoryOutputStream& outputStream, const juce::XmlElement& node) noexcept;

    /**
     * Encodes the given node, recursively.
     * @param outputStream where to write.
     * @param rootNode the node to explore. It generates a section.
     * @param indentationLevel the indentation level on the section (>=0).
     */
    static void encodeNode(juce::MemoryOutputStream& outputStream, const juce::XmlElement& rootNode, int indentationLevel) noexcept;
};

}   // namespace arkostracker
