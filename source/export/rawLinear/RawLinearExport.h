#pragma once

#include <memory>

#include "../ExportConfiguration.h"
#include "../SongExportResult.h"
#include "../../song/instrument/InstrumentType.h"
#include "../../utils/task/Task.h"

namespace arkostracker
{

class Song;
class SourceGenerator;

/**
 * Export to Raw Linear format, a non-optimized format containing a simple fully linear format with
 * wait/noise-instrument-channel/end. Useful to export sample notes.
 */
class RawLinearExporter final : public Task<std::unique_ptr<SongExportResult>>
{
public:
    /**
     * Constructor.
     * @param song the Song. No need to optimize it.
     * @param exportConfiguration data about how to export.
     * @param instrumentTypes the instrument types to export.
     */
    RawLinearExporter(const std::shared_ptr<Song>& song, ExportConfiguration exportConfiguration,
        std::set<InstrumentType> instrumentTypes) noexcept;

    // Task method implementations.
    // ===============================
    std::pair<bool, std::unique_ptr<SongExportResult>> performTask() noexcept override;

private:
    /** @return the base label to be used before every label. */
    juce::String getBaseLabel() const noexcept;

    /** Generates all the data of the song. The header is already encoded. */
    void generateData(const Id& subsongId, SourceGenerator& sourceGenerator) const noexcept;

    /** Encodes the wait, if present. If encoding, the wait counter is reset. */
    static void encodeWait(SourceGenerator& sourceGenerator, int& waitCounter) noexcept;

    std::shared_ptr<Song> song;
    ExportConfiguration exportConfiguration;
    std::set<InstrumentType> instrumentTypes;
};

}   // namespace arkostracker
