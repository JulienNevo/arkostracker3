#pragma once

namespace arkostracker 
{

/** Constants for the AKY. */
class AkyConstants
{
public:
    static const int registerBlockMaximumSize = 255;                            // The maximum size of a RegisterBlock.

    static const int loopByte = 0b00001000;         // The loop byte (b3 = loop?). Must be followed by a DW to where to loop (a non-initial state only!).
};

}   // namespace arkostracker

