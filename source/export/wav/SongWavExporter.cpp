#include "SongWavExporter.h"

#include "../../audio/sources/PsgStreamGenerator.h"
#include "../../audio/sources/PsgsProcessor.h"
#include "../../business/song/tool/frameCounter/FrameCounter.h"
#include "../../player/SongPlayer.h"
#include "DcOffsetCalculator.h"

namespace arkostracker
{

const unsigned int SongWavExporter::channelCount = 2U;
const int SongWavExporter::bitsPerSample = 16;
const int SongWavExporter::bufferSize = 1024;

SongWavExporter::SongWavExporter(std::shared_ptr<Song> pSong, Id pSubsongId, const double pExportFrequencyHz, const int pAdditionalLoopCount,
                                 const OutputMix& pOutputMix, std::unique_ptr<juce::OutputStream> pOutputStream) noexcept :
        song(std::move(pSong)),
        subsongId(std::move(pSubsongId)),
        exportFrequencyHz(pExportFrequencyHz),
        additionalLoopCount(pAdditionalLoopCount),
        outputMix(pOutputMix),
        originalOutputStream(std::move(pOutputStream))
{
}


// Task method implementations.
// ===============================

std::pair<bool, std::unique_ptr<bool>> SongWavExporter::performTask() noexcept
{
    // Creates the WAV writer.
    juce::WavAudioFormat wavAudioFormat;
    const juce::StringPairArray metadataValues;     // Nothing to put inside.
    // Gives the ownership to the AudioFormatWriter. Still need a local reference to it, as it will NOT be deleted in case of failure.
    auto* localOutputStream = originalOutputStream.release();
    const std::unique_ptr<juce::AudioFormatWriter> audioFormatWriter(wavAudioFormat.createWriterFor(localOutputStream,
                                                                                                    exportFrequencyHz, channelCount,
                                                                                                    bitsPerSample, metadataValues, 0));    // No optimization.
    // Stops if the Writer couldn't be created.
    if (audioFormatWriter == nullptr) {
        delete localOutputStream;       // The output stream is NOT deleted in case of failure (cf. createWriterFor documentation).
        return { false, nullptr };
    }

    // Counts how many iterations are in the Song.
    const auto[baseIterationCounter, upTo, loopToCounter] = FrameCounter::count(*song, subsongId);
    // Adds more iterations if additional loops are present.
    // * 2 because two passes, but only approximation because there can be several iterations per buffer, so lowers it. No need to bother too much for now...
    const auto progressIterationCount = static_cast<int>(static_cast<double>(baseIterationCounter + ((baseIterationCounter - loopToCounter) * additionalLoopCount)) * 1.7);
    auto progressIterationIndex = 0;

    const auto psgs = song->getSubsongPsgs(subsongId);
    const auto replayFrequency = song->getReplayFrequencyHz(subsongId);

    // First pass: DC offset.
    DcOffsetCalculator dcOffsetCalculator(channelCount);
    auto success = playAndWrite(psgs, replayFrequency, progressIterationIndex, progressIterationCount,
        [&dcOffsetCalculator](const juce::AudioSourceChannelInfo& bufferToFill) {
        // Calculates the DC offset from this buffer.
        dcOffsetCalculator.readAudioSource(bufferToFill);
        return true;
    });
    if (!success) {
        return { false, nullptr };
    }

    // Second pass: the song itself.
    success = playAndWrite(psgs, replayFrequency, progressIterationIndex, progressIterationCount,
                                                             [&audioFormatWriter, &dcOffsetCalculator](const juce::AudioSourceChannelInfo& bufferToFill) {
        // Writes the buffer to the Writer, it takes care of everything.
        const auto* audioSampleBuffer = bufferToFill.buffer;

        dcOffsetCalculator.applyDcOffset(bufferToFill);

        return audioFormatWriter->writeFromAudioSampleBuffer(*audioSampleBuffer, bufferToFill.startSample, bufferToFill.numSamples);
    });

    return { success, nullptr };
}

// ===============================

bool SongWavExporter::playAndWrite(const std::vector<Psg>& psgs, const float replayFrequency, int& progressIterationIndex, const int progressIterationCount,
                                   const std::function<bool(juce::AudioSourceChannelInfo& bufferToFill)>& applyOnBuffer) noexcept
{
    const auto psgCount = psgs.size();

    // The chain is as follows: (adapted from AudioControllerImpl)
    // - Multiple PSG generator (one per PSG, obviously).
    // - One MixerAudioSource (psgsProcessor) which mixes all the PSGs into one signal,
    //   and process the buffers to apply effects to the mixed signal (filter, stereo separation, etc.).
    // - Here, no Audio Source Player or Audio Device Manager needed.

    SongPlayer songPlayer(song);
    songPlayer.setOfflineSongEndCountBeforeMuting(additionalLoopCount + 1);
    const auto startLocation = Location(subsongId, 0);
    const auto[loopStartLocation, pastEndLocation] = song->getLoopStartAndPastEndPositions(subsongId);
    songPlayer.play(startLocation, loopStartLocation, pastEndLocation, true, true);

    PsgsProcessor psgsProcessor;
    psgsProcessor.setOutputMix(outputMix);

    // Creates the PSG Generators.
    std::vector<std::unique_ptr<PsgStreamGenerator>> psgStreamGenerators;
    psgStreamGenerators.reserve(psgCount);
    auto psgIndex = 0;
    for (const auto& psg : psgs) {
        auto psgStreamGenerator = std::make_unique<PsgStreamGenerator>(songPlayer, psg.getType(), psgIndex, replayFrequency, psg.getPsgFrequency(),
                                                                       psg.getPsgMixingOutput(),
                                                                       static_cast<double>(outputMix.getChannelAVolume()) / 100.0,
                                                                       static_cast<double>(outputMix.getChannelBVolume()) / 100.0,
                                                                       static_cast<double>(outputMix.getChannelCVolume()) / 100.0);

        psgsProcessor.addInputSource(psgStreamGenerator.get(), false);

        psgStreamGenerators.push_back(std::move(psgStreamGenerator));
        ++psgIndex;
    }

    // Creates a small buffer where the WAV is written.
    juce::AudioSampleBuffer audioSampleBuffer(channelCount, bufferSize);
    audioSampleBuffer.clear();

    juce::AudioSourceChannelInfo bufferToFill(audioSampleBuffer);

    // Prepares to play.
    psgsProcessor.prepareToPlay(bufferSize, exportFrequencyHz);

    auto mustContinue = true;
    auto success = true;
    while (mustContinue && success) {
        bufferToFill.clearActiveBufferRegion();     // This is mandatory, else the data are "stacking up".
        // Calls the player.
        psgsProcessor.getNextAudioBlock(bufferToFill);

        success = applyOnBuffer(bufferToFill);

        mustContinue = !songPlayer.hasOfflineSongEndCountReached();

        // Cancelled?
        if (isCanceled()) {
            return false;
        }

        // Notifies the progress. NOTE: this is actually only an estimation, as the iteration does not match what is produced
        // (maybe several iterations per written buffer).
        onTaskProgressed(progressIterationIndex, progressIterationCount);

        ++progressIterationIndex;
    }

    return success;
}

}   // namespace arkostracker
