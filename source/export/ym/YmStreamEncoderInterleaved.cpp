#include "YmStreamEncoderInterleaved.h"

#include "../../player/SongPlayer.h"
#include "../../utils/task/CancelProvider.h"
#include "../../utils/task/TaskProgressListener.h"

namespace arkostracker 
{

YmStreamEncoderInterleaved::YmStreamEncoderInterleaved(std::shared_ptr<const Song> pSong, Id pSubsongId, const int pPsgIndex,
                                                       CancelProvider& pCancelProvider, TaskProgressListener& pTaskProgressListener) noexcept :
        YmStreamEncoder(std::move(pSong), std::move(pSubsongId), pPsgIndex, pCancelProvider, pTaskProgressListener)
{
}

void YmStreamEncoderInterleaved::generateStream(juce::OutputStream& outputStream, const int iterationCount, const Location& startLocation, const Location& pastEndLocation,
                                                const int digiChannel) noexcept
{
    auto allIterationsIndex = 0;
    const auto iterationCount16Passes = iterationCount * 16;
    auto previousHardwareEnvelope = 16;        // Sentinel value.
    const auto psgCount = song->getPsgCount(startLocation.getSubsongId());

    // Reads all the ticks and encode them. 16 passes are required!
    for (auto psgRegister = 0; psgRegister < PsgRegisters::registerCount; ++psgRegister) {

        // Resets the player at the beginning of each pass.
        SongPlayer songPlayer(song);
        songPlayer.play(startLocation, startLocation, pastEndLocation, true, true);

        for (auto iterationIndex = 0; iterationIndex < iterationCount; ++iterationIndex) {
            // Only one PSG is used, but all must be read.
            for (auto browsedPsgIndex = 0; browsedPsgIndex < psgCount; ++browsedPsgIndex) {
                auto [psgRegistersPtr, _] = songPlayer.getNextRegisters(browsedPsgIndex);
                // Only process the targeted PSG.
                if (browsedPsgIndex != psgIndex) {
                    continue;
                }

                auto& psgRegisters = *psgRegistersPtr;

                // Applies digidrums, if any.
                fillPsgRegistersWithDigidrumData(psgRegisters, digiChannel);
                const auto value = psgRegisters.getValueFromRegister(psgRegister);

                if (psgRegister != 13) {
                    // Encodes the value.
                    encodeByte(outputStream, value);
                } else {
                    // R13 has a special treatment. Encodes the value if retrig, or different from the previous frame. Else, 255 (=the same).
                    const auto encodeValue = (psgRegisters.isRetrig() || (value != previousHardwareEnvelope));
                    encodeByte(outputStream, encodeValue ? value : static_cast<unsigned char>(255));

                    previousHardwareEnvelope = value;
                }
            }

            // Cancelled?
            if (cancelProvider.isCanceled()) {
                return;
            }

            // Notifies the progress.
            taskProgressListener.onTaskProgressed(allIterationsIndex++, iterationCount16Passes);
        }
    }
}

}   // namespace arkostracker
