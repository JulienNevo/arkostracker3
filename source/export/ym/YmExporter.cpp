#include "YmExporter.h"

#include "../../business/song/tool/frameCounter/FrameCounter.h"
#include "YmStreamEncoderInterleaved.h"
#include "YmStreamEncoderNonInterleaved.h"

namespace arkostracker 
{

YmExporter::YmExporter(std::shared_ptr<Song> pSong, Id pSubsongId, int pPsgIndex, bool pInterleaved) noexcept :
        song(std::move(pSong)),
        subsongId(std::move(pSubsongId)),
        psgIndex(pPsgIndex),
        interleaved(pInterleaved),
        digiChannel(song->getDigiChannel(subsongId)),
        streamEncoder()
{
}


// Task method implementations.
// ==========================================

std::pair<bool, std::unique_ptr<juce::MemoryOutputStream>> YmExporter::performTask() noexcept
{
    auto psgFrequency = 0;
    auto replayFrequency = 0.0F;
    juce::String subsongTitle;
    song->performOnConstSubsong(subsongId, [&] (const Subsong& subsong) {
        const auto& psg = subsong.getPsgRefs().at(0U);
        psgFrequency = psg.getPsgFrequency();
        replayFrequency = subsong.getReplayFrequencyHz();
        subsongTitle = subsong.getName();
    });

    // Counts how many iterations are in the Song.
    const auto[counter, upTo, loopCounter] = FrameCounter::count(*song, subsongId);
    const auto iterationCount = counter;
    jassert(iterationCount > 0);

    // Instantiates the YMEncoder according to the mode: interleaved or not.
    if (interleaved) {
        streamEncoder = std::make_unique<YmStreamEncoderInterleaved>(song, subsongId, psgIndex, *this, *this);
    } else {
        streamEncoder = std::make_unique<YmStreamEncoderNonInterleaved>(song, subsongId, psgIndex, *this, *this);
    }

    // The music data is encoded after, but we do it now in a specific buffer, because the digidrum count and samples are determined
    // while generating it.
    // Gets the start/end locations.
    const auto startLocation = Location(subsongId, 0);
    const auto[loopStartLocation, pastEndLocation] = song->getLoopStartAndPastEndPositions(subsongId);
    auto musicDataOutputStream = std::make_unique<juce::MemoryOutputStream>();

    streamEncoder->generateStream(*musicDataOutputStream, iterationCount, startLocation, pastEndLocation, digiChannel);

    auto success = !isCanceled();

    // Now encodes the header in the "real" buffer.
    auto outputStream = std::make_unique<juce::MemoryOutputStream>();
    // b1 = signed samples ? --> no. b2 = 4bits? --> no.
    const auto songAttributes = interleaved ? 1 : 0;
    // Digidrums?
    const auto digidrumCount = streamEncoder->getDigidrumCount();

    outputStream->writeText("YM6!", false, false, nullptr);
    outputStream->writeText("LeOnArD!", false, false, nullptr);
    outputStream->writeIntBigEndian(iterationCount);
    outputStream->writeIntBigEndian(songAttributes);
    outputStream->writeShortBigEndian(static_cast<int16_t>(digidrumCount));
    outputStream->writeIntBigEndian(psgFrequency);
    outputStream->writeShortBigEndian(static_cast<int16_t>(replayFrequency));
    outputStream->writeIntBigEndian(loopCounter);
    outputStream->writeShortBigEndian(0);                        // Padding.

    // Encodes the digidrums.
    streamEncoder->encodeDigidrums(*outputStream);

    // The song name. If the Subsong has a title, it is concatenated.
    auto title = song->getName();
    if (!subsongTitle.trim().isEmpty()) {
        title = title + " - " + subsongTitle;
    }
    writeNtString(*outputStream, title);
    writeNtString(*outputStream, song->getAuthor());
    writeNtString(*outputStream, song->getComments());

    // Appends now the music data.
    musicDataOutputStream->flush();
    success = success && outputStream->write(musicDataOutputStream->getData(), musicDataOutputStream->getDataSize());
    musicDataOutputStream.reset();

    // Encodes the tail.
    outputStream->writeText("End!", false, false, nullptr);
    // Finish!
    outputStream->flush();

    streamEncoder.reset();

    return { success, std::move(outputStream) };
}


// ============================================

void YmExporter::writeNtString(juce::OutputStream& outputStream, const juce::String& text) noexcept
{
    outputStream.writeText(text, false, false, nullptr);
    outputStream.writeByte(0);
}

}   // namespace arkostracker
