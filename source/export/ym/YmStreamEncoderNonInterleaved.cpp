#include "YmStreamEncoderNonInterleaved.h"

#include "../../business/song/tool/frameCounter/FrameCounter.h"
#include "../../player/SongPlayer.h"
#include "../../utils/task/CancelProvider.h"
#include "../../utils/task/TaskProgressListener.h"

namespace arkostracker 
{

YmStreamEncoderNonInterleaved::YmStreamEncoderNonInterleaved(std::shared_ptr<const Song> pSong, Id pSubsongId, const int pPsgIndex,
                                                             CancelProvider& pCancelProvider, TaskProgressListener& pTaskProgressListener) noexcept :
        YmStreamEncoder(std::move(pSong), std::move(pSubsongId), pPsgIndex, pCancelProvider, pTaskProgressListener)
{
}


// YmStreamEncoder method implementations.
// ==========================================

void YmStreamEncoderNonInterleaved::generateStream(juce::OutputStream& outputStream, const int iterationCount, const Location& startLocation, const Location& pastEndLocation,
                                                   const int digiChannel) noexcept
{
    auto previousHardwareEnvelope = 16;        // Sentinel value.

    // Initializes the player.
    SongPlayer songPlayer(song);
    songPlayer.play(startLocation, startLocation, pastEndLocation, true, true);
    const auto psgCount = song->getPsgCount(startLocation.getSubsongId());

    // Reads all the ticks and encode them. One pass is required.
    for (auto iterationIndex = 0; iterationIndex < iterationCount; ++iterationIndex) {
        // Only one PSG is used, but all must be read.
        for (auto browsedPsgIndex = 0; browsedPsgIndex < psgCount; ++browsedPsgIndex) {
            auto [psgRegistersPtr, _] = songPlayer.getNextRegisters(browsedPsgIndex);
            // Only process the targeted PSG.
            if (browsedPsgIndex != psgIndex) {
                continue;
            }

            auto& psgRegisters = *psgRegistersPtr;

            // Modifies the PSG registers before encoding them, to have digidrums.
            fillPsgRegistersWithDigidrumData(psgRegisters, digiChannel);
            encodeRegisters(outputStream, psgRegisters, previousHardwareEnvelope);

            previousHardwareEnvelope = psgRegisters.getHardwareEnvelopeAndRetrig().getEnvelope();
        }

        // Cancelled?
        if (cancelProvider.isCanceled()) {
            return;
        }

        // Notifies the progress.
        taskProgressListener.onTaskProgressed(iterationIndex, iterationCount);
    }
}

}   // namespace arkostracker
