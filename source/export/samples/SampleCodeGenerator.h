#pragma once

namespace arkostracker 
{

class Instrument;
class SampleEncoderFlags;
class Song;
class SourceGenerator;

/** Class to easily generate Sample code from an Instrument. */
class SampleCodeGenerator
{
public:
    /** Prevents construction. */
    SampleCodeGenerator() = delete;

    /**
     * Generates a generic header for a Sample Instrument.
     * @param sourceGenerator the Source Generator where to encode the sources.
     * @param sampleEncoderFlags the flags to indicate how to encode the sample.
     * @param instrument the Instrument. It must be a Sample Instrument!
     * @return true if everything went fine. False if the sample is too big, or not a Sample Instrument.
     */
    static bool generateHeader(SourceGenerator& sourceGenerator, const SampleEncoderFlags& sampleEncoderFlags, const Instrument& instrument) noexcept;

    /**
     * Generates the source for a Sample Instrument.
     * @param sourceGenerator the Source Generator where to encode the sources.
     * @param sampleEncoderFlags the flags to indicate how to encode the sample.
     * @param instrument the Instrument. It must be a Sample Instrument!
     */
    static void generateSampleData(SourceGenerator& sourceGenerator, const SampleEncoderFlags& sampleEncoderFlags, const Instrument& instrument) noexcept;
};

}   // namespace arkostracker
