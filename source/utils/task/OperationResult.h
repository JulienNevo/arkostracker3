#pragma once

namespace arkostracker 
{

/** The result of an operation. */
enum class OperationResult
{
    success,
    failure,
    cancelled
};

}   // namespace arkostracker

