cmake_minimum_required(VERSION 3.17)

set(PROJECT_NAME ArkosTracker)
file(STRINGS "ProjectVersion.txt" PROJECT_VERSION)      # Defines the project version from the file, also used by the CI.

project(${PROJECT_NAME} VERSION ${PROJECT_VERSION})

# Including CPM.cmake, a package manager:
# https://github.com/TheLartians/CPM.cmake
include(cmakeScripts/CPM.cmake)

# Uses C++17.
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

# Set this *locally* to build only on arm64, in case compilation fails on Mac (because only one architecture is available).
if (NOT DEFINED ENV{USE_ONLY_ARM64})
    set(CMAKE_OSX_ARCHITECTURES "arm64 x86_64")     # To build on Mac M1, but also Intel.
endif()

if (DEFINED ENV{USE_INTERPROCEDURAL_OPTIMIZATION})
    message("Interprocedural optimization ON")
    set(CMAKE_INTERPROCEDURAL_OPTIMIZATION TRUE)   # Linking too slow if set! So only for the CI.
endif()

#set(CMAKE_CXX_FLAGS_RELEASE "-O0 -g")           # Useful to debug in Release!

# Gets JUCE from GIT, via CPM.
CPMAddPackage(
        NAME JUCE
        GITHUB_REPOSITORY juce-framework/JUCE
        GIT_TAG 7.0.12)

add_subdirectory(binaryResources)
add_subdirectory(source)
add_subdirectory(thirdParty/lzh)
add_subdirectory(thirdParty/rasm)
add_subdirectory(thirdParty/serial)
# Note: the Z80 emulator is not a project because has no .cpp, so is included directly as a directory in the TU project.
add_subdirectory(testUnits EXCLUDE_FROM_ALL)            # Prevents CPack to build the test units, useless. Yet allows building manually! --> Not from Windows it seems?!
add_subdirectory(testUnitResources EXCLUDE_FROM_ALL)

add_subdirectory(source/commandLineTools/baseExport)
add_subdirectory(source/commandLineTools/songToAkg)
add_subdirectory(source/commandLineTools/songToAkm)
add_subdirectory(source/commandLineTools/songToAky)
add_subdirectory(source/commandLineTools/songToEvents)
add_subdirectory(source/commandLineTools/songToRaw)
add_subdirectory(source/commandLineTools/songToYm)
add_subdirectory(source/commandLineTools/songToVgm)
add_subdirectory(source/commandLineTools/songToWav)
add_subdirectory(source/commandLineTools/songToSfx)




# Defines "full warnings" and "no warnings" flag, used below according to the project.
if ((CMAKE_CXX_COMPILER_ID STREQUAL "MSVC") OR (CMAKE_CXX_COMPILER_FRONTEND_VARIANT STREQUAL "MSVC"))
    set(WARNING_FLAGS /W4)              # /Wall impractical, switched to /W4. /WX = warning as error, removed.
    #set(NO_WARNING_FLAGS /w)
elseif ((CMAKE_CXX_COMPILER_ID STREQUAL "Clang") OR (CMAKE_CXX_COMPILER_ID STREQUAL "AppleClang"))
    # Same as for GCC below, but without -Wnoexcept and -Wstrict-null-sentinel
    # which are unknown.
    set(WARNING_FLAGS
            -Wall -Wextra -pedantic -pedantic-errors -Wunused-variable
            #-Werror                                        # I'd like to, but too many false positive.
            #-Wno-conditional-uninitialized                 # Too many false positive, but this flag seems ignored.
            -Wsign-conversion -Wcast-align
            -Wctor-dtor-privacy -Wdisabled-optimization -Wformat=2
            -Winit-self -Wmissing-include-dirs -Woverloaded-virtual
            -Wredundant-decls -Wshadow -Wstrict-overflow=5
            -Wno-unused -Wno-variadic-macros -Wno-parentheses -fdiagnostics-show-option
            )
    #set(NO_WARNING_FLAGS -w)
else()  # GNU (=GCC)
    # JUCE cannot compile with -Wundef -Wold-style-cast -Wcast-qual -Wlogical-op -Wconversion -Wnoexcept
    set(WARNING_FLAGS
            -Wall -Wextra -pedantic -pedantic-errors -Wunused-variable
            -Werror -Wsign-conversion -Wcast-align
            -Wctor-dtor-privacy -Wdisabled-optimization -Wformat=2
            -Winit-self -Wmissing-include-dirs -Woverloaded-virtual
            -Wredundant-decls -Wshadow -Wstrict-null-sentinel -Wstrict-overflow=5
            -Wno-unused -Wno-variadic-macros -Wno-parentheses -fdiagnostics-show-option
            )
    #set(NO_WARNING_FLAGS -w)
endif()

# For our code (only!), make sure we get all the warnings.
target_compile_options(MainSoftware PRIVATE ${WARNING_FLAGS})
target_compile_options(TestUnits PRIVATE ${WARNING_FLAGS})
target_compile_options(SongToAkg PRIVATE ${WARNING_FLAGS})
target_compile_options(SongToAkm PRIVATE ${WARNING_FLAGS})
target_compile_options(SongToAky PRIVATE ${WARNING_FLAGS})
target_compile_options(SongToEvents PRIVATE ${WARNING_FLAGS})
target_compile_options(SongToRaw PRIVATE ${WARNING_FLAGS})
target_compile_options(SongToWav PRIVATE ${WARNING_FLAGS})
target_compile_options(SongToYm PRIVATE ${WARNING_FLAGS})
target_compile_options(SongToVgm PRIVATE ${WARNING_FLAGS})
target_compile_options(SongToSoundEffects PRIVATE ${WARNING_FLAGS})
target_compile_options(BaseExport PRIVATE ${WARNING_FLAGS})
#target_compile_options(ThirdParty PRIVATE ${NO_WARNING_FLAGS})     # This actually doesn't work. Fallback on pragmas...

# Packaging via CPack.
include(cpack/CPackOptions.cmake)
