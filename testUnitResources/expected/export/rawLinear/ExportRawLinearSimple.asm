    org #1000

SamplePattern
.loop
    db 0
        db 6 * 12 + 0, 0, 0        ;Note
    db 1
        dw 6 * 1                   ;Wait

    db 0
        db 5 * 12 + 0, 1, 0        ;Note
    db 1
        dw 6 * 2                   ;Wait

    db 0
        db 6 * 12 + 1, 0, 1        ;Note
    db 1
        dw 6 * 10                  ;Wait

    db 0
        db 6 * 12 + 1, 0, 2        ;Note
    db 1
        dw 6 * 20                  ;Wait

    db 255    ;End
        dw .loop