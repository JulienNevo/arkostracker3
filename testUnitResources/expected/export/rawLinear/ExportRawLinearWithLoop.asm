    org #1000

SamplePattern
    db 1
        dw 6 * 1                   ;Wait

    db 0
        db 5 * 12 + 0, 1, 0        ;Note
    db 1
        dw 6 * 9                   ;Wait
.loop
    db 1
        dw 6 * 1                   ;Wait

    db 0
        db 5 * 12 + 1, 1, 2        ;Note

    db 1
        dw 6 * 9                  ;Wait

    db 255    ;End
        dw .loop