;Test format for a generic pattern. Here used for samples.

;Required info:
;db type of data:
;   0 = channel info: Note + instrument + channel index
;     Can have several in a row. Use another command when the end lines.
;   1 = wait
;       dw wait (>0)
;   2 = effect only (currently not supported).
;   255 = endsong
;       dw loop address

    org #1000

SamplePattern
.loop
    db 1
        dw 6 * 1                   ;Wait

    db 0
        db 5 * 12 + 0, 1, 0        ;Note

    db 1
        dw 6 * 31                  ;Wait

    db 255    ;End
        dw .loop