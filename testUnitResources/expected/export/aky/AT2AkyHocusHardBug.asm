; Song Hocus Pocus, in AKY version 1.0, generated by Arkos Tracker 2.

Main_Subsong0
Main_Subsong0DisarkGenerateExternalLabel

	org 16384

; Header
Main_Subsong0DisarkByteRegionStart0
	db 128	; Format version: 0 and endianness: little-endian.
	db 3	; How many channels are encoded.
; Frequency of the PSG index 0: 1000000Hz.
	db 64, 66, 15, 0
Main_Subsong0DisarkByteRegionEnd0

Main_Subsong0_Linker
Main_Subsong0DisarkPointerRegionStart1
; Pattern 0
Main_Subsong0_LinkerLoop	; Loops here.
Main_Subsong0DisarkWordForceNonReference2
	dw 84	; Duration in frames.
	dw Main_Subsong0_Track_0
	dw Main_Subsong0_Track_2
	dw Main_Subsong0_Track_2

Main_Subsong0DisarkWordForceNonReference3
	dw 0	; Loops (duration = 0).
	dw Main_Subsong0_LinkerLoop
Main_Subsong0DisarkPointerRegionEnd1

; The tracks.
Main_Subsong0_Track_0
Main_Subsong0DisarkByteRegionStart4
	db 18	; Duration.
Main_Subsong0DisarkByteRegionEnd4
Main_Subsong0DisarkPointerRegionStart5
	dw Main_Subsong0_RegisterBlock_0
Main_Subsong0DisarkPointerRegionEnd5

Main_Subsong0DisarkByteRegionStart6
	db 18	; Duration.
Main_Subsong0DisarkByteRegionEnd6
Main_Subsong0DisarkPointerRegionStart7
	dw Main_Subsong0_RegisterBlock_1
Main_Subsong0DisarkPointerRegionEnd7

Main_Subsong0DisarkByteRegionStart8
	db 6	; Duration.
Main_Subsong0DisarkByteRegionEnd8
Main_Subsong0DisarkPointerRegionStart9
	dw Main_Subsong0_RegisterBlock_2
Main_Subsong0DisarkPointerRegionEnd9

Main_Subsong0DisarkByteRegionStart10
	db 6	; Duration.
Main_Subsong0DisarkByteRegionEnd10
Main_Subsong0DisarkPointerRegionStart11
	dw Main_Subsong0_RegisterBlock_3
Main_Subsong0DisarkPointerRegionEnd11

Main_Subsong0DisarkByteRegionStart12
	db 6	; Duration.
Main_Subsong0DisarkByteRegionEnd12
Main_Subsong0DisarkPointerRegionStart13
	dw Main_Subsong0_RegisterBlock_4
Main_Subsong0DisarkPointerRegionEnd13

Main_Subsong0DisarkByteRegionStart14
	db 6	; Duration.
Main_Subsong0DisarkByteRegionEnd14
Main_Subsong0DisarkPointerRegionStart15
	dw Main_Subsong0_RegisterBlock_5
Main_Subsong0DisarkPointerRegionEnd15

Main_Subsong0DisarkByteRegionStart16
	db 6	; Duration.
Main_Subsong0DisarkByteRegionEnd16
Main_Subsong0DisarkPointerRegionStart17
	dw Main_Subsong0_RegisterBlock_8
Main_Subsong0DisarkPointerRegionEnd17

Main_Subsong0DisarkByteRegionStart18
	db 6	; Duration.
Main_Subsong0DisarkByteRegionEnd18
Main_Subsong0DisarkPointerRegionStart19
	dw Main_Subsong0_RegisterBlock_5
Main_Subsong0DisarkPointerRegionEnd19

Main_Subsong0DisarkByteRegionStart20
	db 12	; Duration.
Main_Subsong0DisarkByteRegionEnd20
Main_Subsong0DisarkPointerRegionStart21
	dw Main_Subsong0_RegisterBlock_8
Main_Subsong0DisarkPointerRegionEnd21

Main_Subsong0_Track_2
Main_Subsong0DisarkByteRegionStart22
	db 84	; Duration.
Main_Subsong0DisarkByteRegionEnd22
Main_Subsong0DisarkPointerRegionStart23
	dw Main_Subsong0_RegisterBlock_10
Main_Subsong0DisarkPointerRegionEnd23


; The RegisterBlocks.
Main_Subsong0DisarkByteRegionStart24
Main_Subsong0_RegisterBlock_10
	db 0	; Initial State: no software, no hardware.

Main_Subsong0_RegisterBlock_10_Loop
	db 4	; Non-initial State, no software no hardware.

	db 8	; Loop to index 1.
Main_Subsong0DisarkPointerRegionStart25
	dw Main_Subsong0_RegisterBlock_10_Loop
Main_Subsong0DisarkPointerRegionEnd25


Main_Subsong0_RegisterBlock_0
	db 125	; Initial State: software only.
	db 1	; Noise.
	db 195, 1	; Software period.

	db 253	; Non-initial State, software only.
	db 134	; New LSB for software period.
	db 3	; New MSB for software period, maybe with noise.

	db 210	; Non-initial State, hardware only.
	db 56	; Hardware period, LSB
	db 0	; Hardware period, MSB

	db 18	; Non-initial State, hardware only.

	db 18	; Non-initial State, hardware only.

	db 18	; Non-initial State, hardware only.

Main_Subsong0_RegisterBlock_0_Loop
	db 189	; Non-initial State, software only.
	db 131	; New MSB for software period, maybe with noise.

	db 18	; Non-initial State, hardware only.

	db 18	; Non-initial State, hardware only.

	db 18	; Non-initial State, hardware only.

	db 18	; Non-initial State, hardware only.

	db 18	; Non-initial State, hardware only.

	db 8	; Loop to index 6.
Main_Subsong0DisarkPointerRegionStart26
	dw Main_Subsong0_RegisterBlock_0_Loop
Main_Subsong0DisarkPointerRegionEnd26


Main_Subsong0_RegisterBlock_1
	db 125	; Initial State: software only.
	db 1	; Noise.
	db 204, 2	; Software period.

	db 253	; Non-initial State, software only.
	db 152	; New LSB for software period.
	db 5	; New MSB for software period, maybe with noise.

	db 210	; Non-initial State, hardware only.
	db 89	; Hardware period, LSB
	db 0	; Hardware period, MSB

	db 18	; Non-initial State, hardware only.

	db 18	; Non-initial State, hardware only.

	db 18	; Non-initial State, hardware only.

Main_Subsong0_RegisterBlock_1_Loop
	db 189	; Non-initial State, software only.
	db 133	; New MSB for software period, maybe with noise.

	db 18	; Non-initial State, hardware only.

	db 18	; Non-initial State, hardware only.

	db 18	; Non-initial State, hardware only.

	db 18	; Non-initial State, hardware only.

	db 18	; Non-initial State, hardware only.

	db 8	; Loop to index 6.
Main_Subsong0DisarkPointerRegionStart27
	dw Main_Subsong0_RegisterBlock_1_Loop
Main_Subsong0DisarkPointerRegionEnd27


Main_Subsong0_RegisterBlock_8
	db 125	; Initial State: software only.
	db 1	; Noise.
	db 90, 2	; Software period.

	db 253	; Non-initial State, software only.
	db 180	; New LSB for software period.
	db 4	; New MSB for software period, maybe with noise.

	db 210	; Non-initial State, hardware only.
	db 75	; Hardware period, LSB
	db 0	; Hardware period, MSB

	db 18	; Non-initial State, hardware only.

	db 18	; Non-initial State, hardware only.

	db 18	; Non-initial State, hardware only.

	db 189	; Non-initial State, software only.
	db 132	; New MSB for software period, maybe with noise.

	db 8
Main_Subsong0DisarkPointerRegionStart28
	dw Main_Subsong0_RegisterBlock_0 + 15	; Optimization: goto common Block at index 7.
Main_Subsong0DisarkPointerRegionEnd28

Main_Subsong0_RegisterBlock_2
	db 125	; Initial State: software only.
	db 1	; Noise.
	db 20, 0	; Software period.

	db 125	; Non-initial State, software only.
	db 40	; New LSB for software period.

Main_Subsong0_RegisterBlock_2_Loop
	db 210	; Non-initial State, hardware only.
	db 2	; Hardware period, LSB
	db 0	; Hardware period, MSB

	db 8	; Loop to index 2.
Main_Subsong0DisarkPointerRegionStart29
	dw Main_Subsong0_RegisterBlock_2_Loop
Main_Subsong0DisarkPointerRegionEnd29


Main_Subsong0_RegisterBlock_3
	db 125	; Initial State: software only.
	db 1	; Noise.
	db 22, 0	; Software period.

	db 125	; Non-initial State, software only.
	db 45	; New LSB for software period.

Main_Subsong0_RegisterBlock_3_Loop
	db 210	; Non-initial State, hardware only.
	db 3	; Hardware period, LSB
	db 0	; Hardware period, MSB

	db 8	; Loop to index 2.
Main_Subsong0DisarkPointerRegionStart30
	dw Main_Subsong0_RegisterBlock_3_Loop
Main_Subsong0DisarkPointerRegionEnd30


Main_Subsong0_RegisterBlock_4
	db 125	; Initial State: software only.
	db 1	; Noise.
	db 40, 0	; Software period.

	db 125	; Non-initial State, software only.
	db 80	; New LSB for software period.

Main_Subsong0_RegisterBlock_4_Loop
	db 210	; Non-initial State, hardware only.
	db 5	; Hardware period, LSB
	db 0	; Hardware period, MSB

	db 8	; Loop to index 2.
Main_Subsong0DisarkPointerRegionStart31
	dw Main_Subsong0_RegisterBlock_4_Loop
Main_Subsong0DisarkPointerRegionEnd31


Main_Subsong0_RegisterBlock_5
	db 125	; Initial State: software only.
	db 1	; Noise.
	db 126, 2	; Software period.

	db 253	; Non-initial State, software only.
	db 252	; New LSB for software period.
	db 4	; New MSB for software period, maybe with noise.

Main_Subsong0_RegisterBlock_5_Loop
	db 210	; Non-initial State, hardware only.
	db 80	; Hardware period, LSB
	db 0	; Hardware period, MSB

	db 8	; Loop to index 2.
Main_Subsong0DisarkPointerRegionStart32
	dw Main_Subsong0_RegisterBlock_5_Loop
Main_Subsong0DisarkPointerRegionEnd32


Main_Subsong0_RegisterBlock_6
	db 125	; Initial State: software only.
	db 1	; Noise.
	db 90, 2	; Software period.

	db 253	; Non-initial State, software only.
	db 180	; New LSB for software period.
	db 4	; New MSB for software period, maybe with noise.

Main_Subsong0_RegisterBlock_6_Loop
	db 210	; Non-initial State, hardware only.
	db 75	; Hardware period, LSB
	db 0	; Hardware period, MSB

	db 8	; Loop to index 2.
Main_Subsong0DisarkPointerRegionStart33
	dw Main_Subsong0_RegisterBlock_6_Loop
Main_Subsong0DisarkPointerRegionEnd33


Main_Subsong0DisarkByteRegionEnd24
