; Generated by Arkos Tracker 2.

TestSong_Start
	db "AT20"
	dw TestSong_Arpeggios	; The address of the Arpeggio table.
	dw TestSong_Pitches	; The address of the Pitch table.
	dw TestSong_Instruments	; The address of the Instrument table.

; The addresses of each Subsong:
	dw TestSong_Subsong0_Start

; Declares all the Arpeggios.
TestSong_Arpeggios
	dw TestSong_Arpeggio1
	dw TestSong_Arpeggio2

TestSong_Arpeggio1
	db 3	; The speed.

	dw 0
	dw $ + 2
	dw 4
	dw $ + 2
	dw 7
	dw $ + 2
	dw 12
	dw $ + 2
	dw 16
	dw $ + 2
	dw 19
	dw TestSong_Arpeggio1 + 4 * 0 + 1

TestSong_Arpeggio2
	db 2	; The speed.

	dw 0
	dw $ + 2
	dw 3
	dw $ + 2
	dw 7
	dw $ + 2
	dw 0
	dw $ + 2
	dw 4
	dw $ + 2
	dw 7
	dw TestSong_Arpeggio2 + 4 * 3 + 1

; Declares all the Pitches.
TestSong_Pitches
	dw TestSong_Pitch1

TestSong_Pitch1
	db 0	; The speed.

	dw 0
	dw $ + 2
	dw -1
	dw $ + 2
	dw -2
	dw $ + 2
	dw -1
	dw $ + 2
	dw 0
	dw $ + 2
	dw 1
	dw $ + 2
	dw 2
	dw $ + 2
	dw 1
	dw TestSong_Pitch1 + 4 * 0 + 1

; Declares all the Instruments.
TestSong_Instruments
	dw TestSong_EmptyInstrument
	dw TestSong_Instrument1
	dw TestSong_Instrument2
	dw 0	; Not a FM Instrument.
	dw TestSong_Instrument4

TestSong_EmptyInstrument
	db 255	; The speed
	db 0	; Retrig?

	dw TestSong_InstrumentCell_3866539354
	dw 0, 2 * 0 + TestSong_EmptyInstrument + 2	; Loops to line index 0.
TestSong_Instrument1
	db 0	; The speed
	db 0	; Retrig?

	dw TestSong_InstrumentCell_1204028311
	dw 0, 2 * 0 + TestSong_Instrument1 + 2	; Loops to line index 0.
TestSong_Instrument2
	db 10	; The speed
	db 0	; Retrig?

	dw TestSong_InstrumentCell_3748761067
	dw TestSong_InstrumentCell_1121662411
	dw TestSong_InstrumentCell_992579692
	dw TestSong_InstrumentCell_605331535
	dw TestSong_InstrumentCell_3738554360
	dw TestSong_InstrumentCell_3609471641
	dw 0, 2 * 0 + TestSong_EmptyInstrument + 2	; Loops to empty instrument.
TestSong_Instrument4
	db 0	; The speed
	db 0	; Retrig?

	dw TestSong_InstrumentCell_1365261734
	dw TestSong_InstrumentCell_1236179015
	dw TestSong_InstrumentCell_1107096296
	dw TestSong_InstrumentCell_978013577
	dw 0, 2 * 3 + TestSong_Instrument4 + 2	; Loops to line index 3.

; All the lines of instruments.
TestSong_InstrumentCell_605331535
	db 53	; Soft only. Volume: 10. Volume only.

TestSong_InstrumentCell_978013577
	db 57	; Soft only. Volume: 12. Volume only.

TestSong_InstrumentCell_992579692
	db 59	; Soft only. Volume: 13. Volume only.

TestSong_InstrumentCell_1107096296
	db 59	; Soft only. Volume: 13. Volume only.

TestSong_InstrumentCell_1121662411
	db 61	; Soft only. Volume: 14. Volume only.

TestSong_InstrumentCell_1204028311
	db 64	; Soft to Hard. Envelope: 8. Retrig ? false. Noise ? false.
	db 132	; Simple case. Ratio: 4

TestSong_InstrumentCell_1236179015
	db 61	; Soft only. Volume: 14. Volume only.

TestSong_InstrumentCell_1365261734
	db 63	; Soft only. Volume: 15. Volume only.

TestSong_InstrumentCell_3609471641
	db 0	; No Soft no Hard. Volume: 0. Noise? false.

TestSong_InstrumentCell_3738554360
	db 35	; Soft only. Volume: 1. Volume only.

TestSong_InstrumentCell_3748761067
	db 62	; Soft only. Volume: 15.
	db 1	; Additional data. Noise: 1. Pitch? false. Arp? false. Period? false.

TestSong_InstrumentCell_3866539354
	db 0	; No Soft no Hard. Volume: 0. Noise? false.

; All the effects shared by all the subsongs.
TestSong_EffectsBlock
TestSong_EffectBlock_P0
	db 0
TestSong_EffectBlock_P6P0
	db 6, 0

; Subsong 0
; ----------------------
TestSong_Subsong0_Start
	db 2	; ReplayFrequency (0=12.5hz, 1=25, 2=50, 3=100, 4=150, 5=300).
	db 0	; Digichannel (0-2).
	db 5	; Initial speed (>=0).
	db 0	; Loop start index (>=0).
	db 0	; End index (>=0).

TestSong_Subsong0_Linker
; Position 0
	dw TestSong_Subsong0_Track0
	dw TestSong_Subsong0_Track1
	dw TestSong_Subsong0_Track2
	db 0 * 2	; Linker block index.

TestSong_Subsong0_LinkerBlocks
TestSong_Subsong0_LinkerBlock0
	db 64	; Height.
	db 0	; Transposition 1.
	db 1	; Transposition 2.
	db -1	; Transposition 3.
	dw TestSong_Subsong0_SpeedTrack0	; SpeedTrack address.
	dw TestSong_Subsong0_EventTrack0	; EventTrack address.

TestSong_Subsong0_Track0
	db 208, 1	; New Instrument (1).
	db 1 + (((TestSong_EffectBlock_P0 - TestSong_EffectsBlock) & #7f00) / 128)	; 7 bits of the MSB of the relative address to the effect block.
	db (TestSong_EffectBlock_P0 - TestSong_EffectsBlock) & #ff	; LSB of the relative address to the effect block.
	db 61, 6	; Waits for 7 lines.
	db 60	; No note, but effects.
	db 1 + (((TestSong_EffectBlock_P6P0 - TestSong_EffectsBlock) & #7f00) / 128)	; 7 bits of the MSB of the relative address to the effect block.
	db (TestSong_EffectBlock_P6P0 - TestSong_EffectsBlock) & #ff	; LSB of the relative address to the effect block.
	db 61, 127	; Waits for 128 lines.

TestSong_Subsong0_Track1
	db 61, 15	; Waits for 16 lines.
	db 152, 4	; New Instrument (4).
	db 61, 127	; Waits for 128 lines.

TestSong_Subsong0_Track2
	db 61, 29	; Waits for 30 lines.
	db 154, 4	; New Instrument (4).
	db 61, 127	; Waits for 128 lines.

; The speed tracks
TestSong_Subsong0_SpeedTrack0
	db 39	; Wait for 20 lines.
	db 12	; Value: 6.
	db 47	; Wait for 24 lines.
	db 16	; Value: 8.
	db 251	; Wait for 126 lines.

; The event tracks
TestSong_Subsong0_EventTrack0
	db 255	; Wait for 128 lines.

