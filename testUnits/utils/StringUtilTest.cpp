#include "../catch.hpp"

#include "../../source/utils/StringUtil.h"

namespace arkostracker 
{

/** Tests the StringUtil class. */

TEST_CASE("extractExtension", "[StringUtil]")
{
    REQUIRE(StringUtil::extractExtension("aks", false) == "aks");
    REQUIRE(StringUtil::extractExtension("aks", true) == ".aks");
    REQUIRE(StringUtil::extractExtension(".aki", false) == "aki");
    REQUIRE(StringUtil::extractExtension(".aki", true) == ".aki");
    REQUIRE(StringUtil::extractExtension("*.abcd", false) == "abcd");
    REQUIRE(StringUtil::extractExtension("*.abcd", true) == ".abcd");
    REQUIRE(StringUtil::extractExtension("bob.zxer", false) == "zxer");
    REQUIRE(StringUtil::extractExtension("bob.zxer", true) == ".zxer");
}

TEST_CASE("stringToBool", "[StringUtil]")
{
    REQUIRE(StringUtil::stringToBool("true"));
    REQUIRE(StringUtil::stringToBool("TRUE"));
    REQUIRE(StringUtil::stringToBool("True"));
    REQUIRE(StringUtil::stringToBool("TrUE"));
    REQUIRE(StringUtil::stringToBool("  True"));
    REQUIRE(StringUtil::stringToBool("  True   "));

    REQUIRE(!StringUtil::stringToBool("tru"));
    REQUIRE(!StringUtil::stringToBool("false"));
    REQUIRE(!StringUtil::stringToBool("False"));
    REQUIRE(!StringUtil::stringToBool("Fals"));
    REQUIRE(!StringUtil::stringToBool("FalsE"));
    REQUIRE(!StringUtil::stringToBool("FALSE"));
}

TEST_CASE("boolToString", "[StringUtil]")
{
    REQUIRE(juce::String("false") == StringUtil::boolToString(false));
    REQUIRE(juce::String("true") == StringUtil::boolToString(true));
}

TEST_CASE("split", "[StringUtil]")
{
    REQUIRE(StringUtil::split("abc,def,ghh,z", ",") == std::vector<juce::String> {"abc", "def", "ghh", "z"});
    REQUIRE(StringUtil::split("a,def", ",") == std::vector<juce::String> {"a", "def"});
    REQUIRE(StringUtil::split("a,def,", ",") == std::vector<juce::String> {"a", "def"});
    REQUIRE(StringUtil::split("zef", ",") == std::vector<juce::String> {"zef"});
    REQUIRE(StringUtil::split("zef,", ",") == std::vector<juce::String> {"zef"});
    REQUIRE(StringUtil::split("", ",").empty());
    REQUIRE(StringUtil::split(",", ",").empty());
    REQUIRE(StringUtil::split("a  bc  d    eee      ffff  ", " ") == std::vector<juce::String> {"a", "bc", "d", "eee", "ffff"});

    REQUIRE(StringUtil::split("abc:::://::::def:::://::::ghh:::://::::z", ":::://::::") == std::vector<juce::String> {"abc", "def", "ghh", "z"});
}

}   // namespace arkostracker

