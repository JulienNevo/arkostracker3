#include <juce_core/juce_core.h>

#include "BinaryData.h"
#include "../../catch.hpp"
#include "../../../source/export/rawLinear/RawLinearExport.h"
#include "../../../source/song/Song.h"
#include "../../helper/RasmHelper.h"
#include "../../helper/TrackTestHelper.h"

namespace arkostracker
{

TEST_CASE("raw linear exporter, nominal with psg instruments", "[RawLinearExporter]")
{
    constexpr auto startAddress = 0x1000;
    auto songUniquePtr = Song::buildDefaultSong(true);
    auto song = std::shared_ptr(std::move(songUniquePtr));
    const auto subsongId = song->getFirstSubsongId();

    song->performOnSubsong(subsongId, [&](Subsong& subsong) {
        subsong.setCell(0, 0, 0, Cell::build(6 * 12, 0));
        subsong.setCell(0, 1, 0, Cell::build(5 * 12, 1));
        subsong.setCell(0, 3, 1, Cell::build(6 * 12 + 1, 0));
        subsong.setCell(0, 13, 2, Cell::build(6 * 12 + 1, 0));
        const auto position = subsong.getPosition(0).withHeight(33);
        subsong.setPosition(0, position);
    });
    song->addInstrument(std::move(Instrument::buildPsgInstrument("i1", PsgPart())));

    const ExportConfiguration exportConfiguration(SourceGeneratorConfiguration::buildZ80(), { subsongId }, "label_", startAddress,
    SampleEncoderFlags(), false);

    RawLinearExporter rawLinearExporter(song, exportConfiguration, { InstrumentType::psgInstrument });

    const auto [success, result] = rawLinearExporter.performTask();

    // Then.
    REQUIRE(success);
    REQUIRE(result != nullptr);

    // Then. Compares the assembled code.
    const auto songData = result->getSongData();
    const auto expectedSourceMemoryBlock = juce::MemoryBlock(BinaryData::ExportRawLinearSimple_asm, BinaryData::ExportRawLinearSimple_asmSize);

    // Useful for testing.
    //FileUtil::saveMemoryBlockToFile("~/Documents/dev/tests/genRawLinear.asm", songData);

    REQUIRE(RasmHelper::assembleAndCompareSources(songData, expectedSourceMemoryBlock));
}

TEST_CASE("raw linear exporter, nominal with sample instruments", "[RawLinearExporter]")
{
    constexpr auto startAddress = 0x1000;
    auto songUniquePtr = Song::buildDefaultSong(false);
    auto song = std::shared_ptr(std::move(songUniquePtr));
    const auto subsongId = song->getFirstSubsongId();

    // Same as above, but filtering only the samples.
    song->performOnSubsong(subsongId, [&](Subsong& subsong) {
        subsong.setCell(0, 0, 0, Cell::build(6 * 12, 0));
        subsong.setCell(0, 1, 0, Cell::build(5 * 12, 1));       // Only this one is used.
        subsong.setCell(0, 3, 1, Cell::build(6 * 12 + 1, 0));
        subsong.setCell(0, 13, 2, Cell::build(6 * 12 + 1, 0));
        const auto position = subsong.getPosition(0).withHeight(32);
        subsong.setPosition(0, position);
    });
    song->addInstrument(std::move(Instrument::buildSampleInstrument("i1", SamplePart())));

    const ExportConfiguration exportConfiguration(SourceGeneratorConfiguration::buildZ80(), { subsongId }, "label_", startAddress,
    SampleEncoderFlags(), false);

    RawLinearExporter rawLinearExporter(song, exportConfiguration, { InstrumentType::sampleInstrument });

    const auto [success, result] = rawLinearExporter.performTask();

    // Then.
    REQUIRE(success);
    REQUIRE(result != nullptr);

    // Then. Compares the assembled code.
    const auto songData = result->getSongData();
    const auto expectedSourceMemoryBlock = juce::MemoryBlock(BinaryData::ExportRawLinearSample_asm, BinaryData::ExportRawLinearSample_asmSize);

    // Useful for testing.
    //FileUtil::saveMemoryBlockToFile("~/Documents/dev/tests/genRawLinear.asm", songData);

    REQUIRE(RasmHelper::assembleAndCompareSources(songData, expectedSourceMemoryBlock));
}

TEST_CASE("raw linear exporter, nominal with sample instruments and loop between space", "[RawLinearExporter]")
{
    constexpr auto startAddress = 0x1000;
    auto songUniquePtr = Song::buildDefaultSong(false);
    auto song = std::shared_ptr(std::move(songUniquePtr));
    const auto subsongId = song->getFirstSubsongId();

    // Same as above, but filtering only the samples.
    song->performOnSubsong(subsongId, [&](Subsong& subsong) {
        const auto position0 = subsong.getPosition(0).withHeight(10);
        subsong.setPosition(0, position0);

        const auto position1 = Position(1, 10);
        subsong.addPosition(position1);

        const auto pattern1 = Pattern({ 3, 4, 5}, 0, 0);
        subsong.addPattern(pattern1);

        subsong.addTrack(Track());
        subsong.addTrack(Track());
        subsong.addTrack(Track());

        subsong.setCell(0, 1, 0, Cell::build(5 * 12, 1));
        subsong.setCell(1, 1, 2, Cell::build(5 * 12 + 1, 1));

        subsong.setLoop(Loop(1, 1, true));
    });
    song->addInstrument(std::move(Instrument::buildSampleInstrument("i1", SamplePart())));

    const ExportConfiguration exportConfiguration(SourceGeneratorConfiguration::buildZ80(), { subsongId }, "label_", startAddress,
    SampleEncoderFlags(), false);

    RawLinearExporter rawLinearExporter(song, exportConfiguration, { InstrumentType::sampleInstrument });

    const auto [success, result] = rawLinearExporter.performTask();

    // Then.
    REQUIRE(success);
    REQUIRE(result != nullptr);

    // Then. Compares the assembled code.
    const auto songData = result->getSongData();
    const auto expectedSourceMemoryBlock = juce::MemoryBlock(BinaryData::ExportRawLinearWithLoop_asm, BinaryData::ExportRawLinearWithLoop_asmSize);

    // Useful for testing.
    //FileUtil::saveMemoryBlockToFile("~/Documents/dev/tests/genRawLinear.asm", songData);

    REQUIRE(RasmHelper::assembleAndCompareSources(songData, expectedSourceMemoryBlock));
}

}   // namespace arkostracker
