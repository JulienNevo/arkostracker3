#include "../../catch.hpp"

#include "../../../source/export/txt/SongTxtExporter.h"
#include "../../../source/import/loader/SongLoader.h"
#include "BinaryData.h"

#include <juce_core/juce_core.h>

namespace arkostracker
{

/** Tests the SongTxtExporter class. */

TEST_CASE("export simple song", "[SongTxtExporter]")
{
    // Given.
    juce::MemoryInputStream inputStream(BinaryData::At3Sarkboteur_xml, static_cast<size_t>(BinaryData::At3Sarkboteur_xmlSize), false);
    SongLoader loader;
    const auto loadResult = loader.loadSong(inputStream, "aks", false, nullptr);
    REQUIRE(loadResult != nullptr);
    REQUIRE(loadResult->errorReport != nullptr);
    REQUIRE(loadResult->errorReport->isOk());
    auto song = std::move(loadResult->song);

    // When.
    auto outputStream = SongTxtExporter::exportSong(*song);
    REQUIRE(outputStream != nullptr);

    // Then.
    // Raw test for now.
    //const juce::File outputFile("~/Documents/dev/test/outputSong.txt");
    //const juce::File outputFile("~/Documents/test/outputSong.txt");
    //outputFile.deleteFile();
    //juce::FileOutputStream fos(outputFile);
    //fos.write(outputStream->getData(), outputStream->getDataSize());
}

}   // namespace arkostracker
