#pragma once

namespace arkostracker
{

/** The player type. */
enum class PlayerType : unsigned char
{
    aky,
    akyStabilized,
    akyMultiPsg,
    akg,
    akm,
    //lightweight,
    //lightweightSharpMz700,
    //standAloneSoundEffects,
};

}   // namespace arkostracker
