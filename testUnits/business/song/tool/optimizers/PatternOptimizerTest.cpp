#include "../../../../catch.hpp"

#include "../../../../../source/business/instrument/GenericInstrumentGenerator.h"
#include "../../../../../source/business/song/tool/optimizers/PatternOptimizer.h"
#include "../../../../helper/SongTestHelper.h"
#include "../../../../helper/TrackTestHelper.h"

namespace arkostracker 
{

TEST_CASE("PatternOptimizer, all the same tracks content if height cut, plus some unused, tracks are equals after normalization", "[PatternOptimizer]")
{
    // Given.
    const auto originalSong = SongTestHelper::createSong();
    originalSong->addInstrument(Instrument::buildEmptyPsgInstrument());
    originalSong->addInstrument(GenericInstrumentGenerator::generateInstrumentForDecreasingVolume());   // 1.
    originalSong->addInstrument(GenericInstrumentGenerator::generateInstrumentForBassDrum());           // 2.
    originalSong->addInstrument(GenericInstrumentGenerator::generateInstrumentForHihat());              // 3.

    SongTestHelper::addSubsong(*originalSong, 1, 6, 0, 1,
                               {
                                       Position(0, 64),
                                       Position(1, 64),
                               },
                               {
                                       Pattern({ 0, 1, 2 }, 0, 0),
                                       Pattern({ 7, 8, 9 }, 1, 1),
                               },
                               {
                                       // Track 0.
                                       TrackTestHelper::buildTrack({
                                                                           { 5,   Cell::build(10, 1) },
                                                                           { 12,  Cell::build(12, 2, CellEffects({{ Effect::arpeggioTable, 2 }})) },
                                                                           { 127, Cell::build(12, 2, CellEffects({{ Effect::arpeggioTable, 2 }})) },
                                                                   }),
                                       // Track 1.
                                       TrackTestHelper::buildTrack({
                                                                           { 5,  Cell::build(10, 1) },
                                                                           { 12, Cell::build(12, 2, CellEffects({{ Effect::arpeggioTable, 2 }})) },
                                                                   }),
                                       // Track 2.
                                       TrackTestHelper::buildTrack({
                                                                           { 5,   Cell::build(10, 1) },
                                                                           { 12,  Cell::build(12, 2, CellEffects({
                                                                                                                         { Effect::arpeggioTable, 2 },
                                                                                                                         { Effect::arpeggioTable, 2 }
                                                                                                                 })) },
                                                                           { 120, Cell::build(12, 2, CellEffects({{ Effect::pitchTable, 5 }})) },
                                                                   }),
                                       // Track 3. Unused.
                                       TrackTestHelper::buildTrack({
                                                                           { 0,  Cell::build(12, 2, CellEffects({{ Effect::pitchTable, 3 }})) },
                                                                           { 5,  Cell::build(10, 2) },
                                                                           { 28, Cell::build(12, 2, CellEffects({{ Effect::arpeggioTable, 1 }})) },
                                                                           { 30, Cell::build(20, 2, CellEffects({{ Effect::pitchTable, 2 }})) },
                                                                   }),
                                       // Track 4. Unused.
                                       TrackTestHelper::buildEmptyTrack(),
                                       // Track 5. Unused.
                                       TrackTestHelper::buildEmptyTrack(),
                                       // Track 6. Unused
                                       TrackTestHelper::buildTrack({
                                                                           { 5,  Cell::build(10, 1) },
                                                                           { 12, Cell::build(12, 2, CellEffects({{ Effect::arpeggioTable, 2 }})) },
                                                                   }),
                                       // Track 7
                                       TrackTestHelper::buildTrack({
                                                                           { 5,  Cell::build(10, 1) },
                                                                           { 12, Cell::build(12, 2, CellEffects({{ Effect::arpeggioTable, 2 }})) },
                                                                           { 64, Cell::build(12, 2, CellEffects({{ Effect::arpeggioTable, 2 }})) },
                                                                   }),
                                       // Track 8.
                                       TrackTestHelper::buildTrack({
                                                                           { 5,  Cell::build(10, 1) },
                                                                           { 12, Cell::build(12, 2, CellEffects({{ Effect::arpeggioTable, 2 }})) },
                                                                           { 65, Cell::build(12, 2, CellEffects({{ Effect::arpeggioTable, 2 }})) },
                                                                   }),
                                       // Track 9.
                                       TrackTestHelper::buildTrack({
                                                                           { 5,  Cell::build(10, 1) },
                                                                           { 12, Cell::build(12, 2, CellEffects({{ Effect::arpeggioTable, 2 }})) },
                                                                           { 66, Cell::build(12, 2, CellEffects({{ Effect::arpeggioTable, 2 }})) },
                                                                   }),
                               },
                               {
                                       // Speed Track 0.
                                       TrackTestHelper::buildSpecialTrack({
                                                                                  { 0,  SpecialCell::buildSpecialCell(1) },
                                                                                  { 10, SpecialCell::buildSpecialCell(2) },
                                                                          }),
                                       // Speed Track 1.
                                       TrackTestHelper::buildSpecialTrack({
                                                                                  { 0,  SpecialCell::buildSpecialCell(1) },
                                                                                  { 10, SpecialCell::buildSpecialCell(2) },
                                                                                  { 64, SpecialCell::buildSpecialCell(10) },
                                                                          }),
                               },
                               {
                                       // Event Track 0.
                                       TrackTestHelper::buildSpecialTrack({
                                                                                  { 3,  SpecialCell::buildSpecialCell(6) },
                                                                                  { 11, SpecialCell::buildSpecialCell(9) },
                                                                          }),
                                       // Event Track 1.
                                       TrackTestHelper::buildSpecialTrack({
                                                                                  { 3,  SpecialCell::buildSpecialCell(6) },
                                                                                  { 11, SpecialCell::buildSpecialCell(9) },
                                                                                  { 64, SpecialCell::buildSpecialCell(1) },
                                                                          }),
                               }
    );


    // When.
    PatternOptimizer patternOptimizer(*originalSong, { originalSong->getFirstSubsongId() });
    const auto newSong = patternOptimizer.optimize(true, true);

    const auto expectedSong = SongTestHelper::createSong();
    expectedSong->addInstrument(Instrument::buildEmptyPsgInstrument());
    expectedSong->addInstrument(GenericInstrumentGenerator::generateInstrumentForDecreasingVolume());   // 1.
    expectedSong->addInstrument(GenericInstrumentGenerator::generateInstrumentForBassDrum());           // 2.
    expectedSong->addInstrument(GenericInstrumentGenerator::generateInstrumentForHihat());              // 3.

    SongTestHelper::addSubsong(*expectedSong, 1, 6, 0, 1,
                               {
                                       Position(0, 64),
                                       Position(0, 64),
                               },
                               {
                                       Pattern({ 0, 0, 0 }, 0, 0),
                               },
                               {
                                       // Track 0.
                                       TrackTestHelper::buildTrack({
                                                                           { 5,  Cell::build(10, 1) },
                                                                           { 12, Cell::build(12, 2, CellEffects({{ Effect::arpeggioTable, 2 }})) },
                                                                   }),
                               },
                               {
                                       // Speed Track 0.
                                       TrackTestHelper::buildSpecialTrack({
                                                                                  { 0,  SpecialCell::buildSpecialCell(1) },
                                                                                  { 10, SpecialCell::buildSpecialCell(2) },
                                                                          }),
                               },
                               {
                                       // Event Track 0.
                                       TrackTestHelper::buildSpecialTrack({
                                                                                  { 3,  SpecialCell::buildSpecialCell(6) },
                                                                                  { 11, SpecialCell::buildSpecialCell(9) },
                                                                          }),
                               }
    );

    REQUIRE(SongTestHelper::areSongMusicallyEqual(*expectedSong, *newSong));
}

}   // namespace arkostracker
