#pragma once

namespace arkostracker
{

class Song;

/** A class that tests the song "Sarkboteur". */
class CheckSarkboteur
{
public:
    static void check(const Song& song, bool isNative);
};

}   // namespace arkostracker
