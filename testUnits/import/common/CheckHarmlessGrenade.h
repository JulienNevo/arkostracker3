#pragma once

namespace arkostracker
{

class Song;

/** A class that tests the song "A Harmless grenade". */
class CheckHarmlessGrenade
{
public:
    static void check(const Song& song);
};

}   // namespace arkostracker
