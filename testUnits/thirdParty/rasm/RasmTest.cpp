#include "../../catch.hpp"

#include <BinaryData.h>
#include <juce_core/juce_core.h>

#include "../../../source/export/aky/AkyExporter.h"
#include "../../../source/utils/MemoryBlockUtil.h"
#include "../../../thirdParty/rasm/rasm.h"
#include "../../helper/RasmHelper.h"

namespace arkostracker 
{

void compare(const unsigned char* data1, const unsigned char* data2, int length)
{
    // Use this one (faster), but the other below is useful to know where the bug happened.
    //REQUIRE(0 == memcmp(data1, data2, length));

    // Compares each byte.
    for (auto i = 0; i < length; ++i) {
        const auto firstValue = data1[i];
        const auto secondValue = data2[i];
        REQUIRE(firstValue == secondValue);
    }
}

void assembleSourceFromMemoryBlock(juce::MemoryBlock& sourceMemoryBlock, bool commentIncludes) {
    juce::MemoryBlock* memoryBlockToCompile = &sourceMemoryBlock;

    // Finds the "includes" and comments them (else, problem with Rasm Include problem, even if
    // they are not supposed to be assembled).
    // Ok, this is done even if not necessary... Who cares?
    juce::MemoryBlock memoryBlockWithoutInclude = RasmHelper::commentIncludes(sourceMemoryBlock);
    if (commentIncludes) {
        memoryBlockToCompile = &memoryBlockWithoutInclude;
    }

    auto generatedSourceResult = RasmHelper::assembleSource(*memoryBlockToCompile);

    REQUIRE(generatedSourceResult != nullptr);
    REQUIRE(generatedSourceResult->getSize() > 0);
}




TEST_CASE("Rasm, short AKY", "[Rasm]")
{
    // Given.
    juce::MemoryBlock sourceMusicMemoryBlock(BinaryData::At2SimpleAky_asm, BinaryData::At2SimpleAky_asmSize);
    juce::MemoryBlock expectedBinaryMemoryBlock(BinaryData::At2SimpleAky_bin, BinaryData::At2SimpleAky_binSize);

    // When.
    const auto generatedSource = RasmHelper::assembleSource(sourceMusicMemoryBlock);

    // Then.
    REQUIRE(generatedSource != nullptr);
    REQUIRE(MemoryBlockUtil::compare(*generatedSource, expectedBinaryMemoryBlock));
}


TEST_CASE("Rasm, simple", "[Rasm]")
{
    juce::String dataString(" ld b,7 : ld a,9 : nop \r\n nop");
    const char* dataIn = dataString.toRawUTF8();
    const int dataInLength = dataString.length();

    // Creates a pointer of pointer. Rasm will make it point to a new buffer.
    unsigned char* dataOutPt = nullptr;

    auto lengthOut = 0;
    auto result = RasmAssemble(dataIn, dataInLength, &dataOutPt, &lengthOut);

    // Checks the result.
    REQUIRE(result == 0);
    REQUIRE(dataOutPt != nullptr);
    REQUIRE(lengthOut == 6);
    unsigned char rightResult[] = { 6, 7, 0x3eU, 9, 0, 0 };
    compare(dataOutPt, static_cast<const unsigned char*>(rightResult), lengthOut);

    free(dataOutPt);
}

TEST_CASE("Rasm, simple out structure", "[Rasm]")
{
    juce::String dataString(" org #1000 \r\nlabel1 ld b,7 : ld a,9 : nop \r\n nop");
    const char* dataIn = dataString.toRawUTF8();
    const int dataInLength = dataString.length();

    // Creates a pointer of pointer. Rasm will make it point to a new buffer.
    unsigned char* dataOutPt = nullptr;
    s_rasm_info* rasmInfoPt = nullptr;

    int lengthOut = 0;
    int result = RasmAssembleInfo(dataIn, dataInLength, &dataOutPt, &lengthOut, &rasmInfoPt);

    // Checks the result.
    REQUIRE(result == 0);
    REQUIRE(dataOutPt != nullptr);
    REQUIRE(lengthOut == 6);
    unsigned char rightResult[] = { 6, 7, 0x3eU, 9, 0, 0 };
    compare(dataOutPt, static_cast<const unsigned char*>(rightResult), lengthOut);

    // Reads the "out".
    REQUIRE(rasmInfoPt != nullptr);
    REQUIRE(rasmInfoPt->nberror == 0);
    REQUIRE(rasmInfoPt->nbsymbol == 1);
    REQUIRE(rasmInfoPt->symbol != nullptr);
    REQUIRE(rasmInfoPt->symbol[0].v == 0x1000);
    REQUIRE(strncmp(rasmInfoPt->symbol[0].name, "LABEL1", 6) == 0);

    free(dataOutPt);
    RasmFreeInfoStruct(rasmInfoPt);
}

TEST_CASE("Rasm, undef", "[Rasm]")
{
    const char* dataIn = BinaryData::RasmUndef_asm;

    // Creates a pointer of pointer. Rasm will make it point to a new buffer.
    unsigned char* dataOutPt = nullptr;

    int lengthOut = 0;
    int result = RasmAssemble(dataIn, BinaryData::RasmUndef_asmSize, &dataOutPt, &lengthOut);

    // Checks the result.
    REQUIRE(result == 0);
    REQUIRE(dataOutPt != nullptr);
    REQUIRE(lengthOut == 4);

    unsigned char rightResult[] = { 0x3eU, 5, 0x3eU, 6 };
    compare(dataOutPt, static_cast<const unsigned char*>(rightResult), lengthOut);

    free(dataOutPt);
}

TEST_CASE("Rasm, limit exceeded", "[Rasm]")
{
    juce::MemoryBlock memoryBlock(BinaryData::RasmAkyDiaEndInput_asm, BinaryData::RasmAkyDiaEndInput_asmSize);

    // Inserts an "org #ffff" at the beginning.
    juce::String dataString(" org #ffff");
    const char* dataIn = dataString.toRawUTF8();
    const auto dataInLength = static_cast<size_t>(dataString.length());

    memoryBlock.insert(dataIn, dataInLength, 0);

    // Creates a pointer of pointer. Rasm will make it point to a new buffer.
    unsigned char* dataOutPt = nullptr;

    int lengthOut = 0;
    int result = RasmAssemble(static_cast<const char*>(memoryBlock.getData()), static_cast<int>(memoryBlock.getSize()), &dataOutPt, &lengthOut);

    // Checks the result. It must have failed (yet the recovery must be OK)!
    REQUIRE(result != 0);
    REQUIRE(dataOutPt == nullptr);
}

TEST_CASE("Rasm, long aky", "[Rasm]")
{
    // Creates a pointer of pointer. Rasm will make it point to a new buffer.
    unsigned char* dataOutPt = nullptr;
    s_rasm_info* rasmInfoPt = nullptr;

    int lengthOut = 0;
    int result = RasmAssembleInfo(BinaryData::RasmAkyDiaEndInput_asm, BinaryData::RasmAkyDiaEndInput_asmSize, &dataOutPt, &lengthOut, &rasmInfoPt);

    REQUIRE(result == 0);
    REQUIRE(dataOutPt != nullptr);
    REQUIRE(lengthOut == BinaryData::RasmAkyDiaEndResult_binSize);

    // Compares each byte.
    compare(dataOutPt, reinterpret_cast<const unsigned char*>(BinaryData::RasmAkyDiaEndResult_bin), lengthOut);

    free(dataOutPt);
    RasmFreeInfoStruct(rasmInfoPt);
}

TEST_CASE("Rasm, binary export", "[Rasm]")
{
    const char* dataIn = BinaryData::BinarySongShortXmlInput_asm;

    // Creates a pointer of pointer. Rasm will make it point to a new buffer.
    unsigned char* dataOutPt = nullptr;

    int lengthOut = 0;
    int result = RasmAssemble(dataIn, BinaryData::BinarySongShortXmlInput_asmSize, &dataOutPt, &lengthOut);

    REQUIRE(result == 0);
    REQUIRE(dataOutPt != nullptr);
    REQUIRE(lengthOut == BinaryData::BinarySongShortXmlResult_binSize);

    // Compares each byte.
    compare(dataOutPt, reinterpret_cast<const unsigned char*>(BinaryData::BinarySongShortXmlResult_bin), lengthOut);

    free(dataOutPt);
}

TEST_CASE("Rasm, AKG player", "[Rasm]")
{
    // Assembles the Player from the source.
    juce::MemoryBlock sourceMemoryBlock(BinaryData::PlayerAkg_asm, BinaryData::PlayerAkg_asmSize);
    assembleSourceFromMemoryBlock(sourceMemoryBlock, true);
}

TEST_CASE("Rasm, AKY player", "[Rasm]")
{
    // Assembles the Player from the source.
    juce::MemoryBlock sourceMemoryBlock(BinaryData::PlayerAky_asm, BinaryData::PlayerAky_asmSize);
    assembleSourceFromMemoryBlock(sourceMemoryBlock, true);
}

}   // namespace arkostracker

