; Untitled, AKG format, v1.0.

; Generated by Arkos Tracker 2.

Untitled_Start
Untitled_StartDisarkGenerateExternalLabel

Untitled_DisarkByteRegionStart0
	db "AT20"
Untitled_DisarkPointerRegionStart1
	dw Untitled_ArpeggioTable	; The address of the Arpeggio table.
	dw Untitled_PitchTable	; The address of the Pitch table.
	dw Untitled_InstrumentTable	; The address of the Instrument table.
	dw Untitled_EffectBlockTable	; The address of the Effect Block table.
Untitled_DisarkPointerRegionEnd1


; The addresses of each Subsong:
Untitled_DisarkPointerRegionStart2
	dw Untitled_Subsong0_Start
Untitled_DisarkPointerRegionEnd2

; Declares all the Arpeggios.
Untitled_ArpeggioTable
Untitled_DisarkPointerRegionStart3
Untitled_DisarkPointerRegionEnd3

; Declares all the Pitches.
Untitled_PitchTable
Untitled_DisarkPointerRegionStart4
Untitled_DisarkPointerRegionEnd4

; Declares all the Instruments.
Untitled_InstrumentTable
Untitled_DisarkPointerRegionStart5
	dw Untitled_EmptyInstrument
	dw Untitled_Instrument1
	dw Untitled_Instrument2
	dw Untitled_Instrument3
	dw Untitled_Instrument4
	dw Untitled_Instrument5
	dw Untitled_Instrument6
	dw Untitled_Instrument7
	dw Untitled_Instrument8
Untitled_DisarkPointerRegionEnd5

Untitled_EmptyInstrument
	db 0	; The speed (>0, 0 for 256).
Untitled_EmptyInstrument_Loop	db 0	; No Soft no Hard. Volume: 0. Noise? false.

	db 6	; Loop to silence.

Untitled_Instrument1
	db 5	; The speed (>0, 0 for 256).
	db 249	; Soft only. Volume: 15. Volume only.

	db 241	; Soft only. Volume: 14. Volume only.

	db 233	; Soft only. Volume: 13. Volume only.

	db 225	; Soft only. Volume: 12. Volume only.

	db 217	; Soft only. Volume: 11. Volume only.

	db 217	; Soft only. Volume: 11. Volume only.

	db 217	; Soft only. Volume: 11. Volume only.

	db 217	; Soft only. Volume: 11. Volume only.

	db 217	; Soft only. Volume: 11. Volume only.

	db 217	; Soft only. Volume: 11. Volume only.

	db 217	; Soft only. Volume: 11. Volume only.

	db 217	; Soft only. Volume: 11. Volume only.

	db 201	; Soft only. Volume: 9. Volume only.

	db 193	; Soft only. Volume: 8. Volume only.

	db 185	; Soft only. Volume: 7. Volume only.

	db 177	; Soft only. Volume: 6. Volume only.

	db 169	; Soft only. Volume: 5. Volume only.

	db 161	; Soft only. Volume: 4. Volume only.

	db 153	; Soft only. Volume: 3. Volume only.

	db 145	; Soft only. Volume: 2. Volume only.

	db 137	; Soft only. Volume: 1. Volume only.

	db 6	; Loop to silence.

Untitled_Instrument2
	db 1	; The speed (>0, 0 for 256).
	db 248	; No Soft no Hard. Volume: 15. Noise? true.
	db 1	; Noise: 1.

	db 113	; Soft only. Volume: 14.
	db 32	; Additional data. Noise: 0. Pitch? true. Arp? false. Period? false.
	dw 150	; Pitch.

	db 105	; Soft only. Volume: 13.
	db 32	; Additional data. Noise: 0. Pitch? true. Arp? false. Period? false.
	dw 300	; Pitch.

	db 97	; Soft only. Volume: 12.
	db 32	; Additional data. Noise: 0. Pitch? true. Arp? false. Period? false.
	dw 400	; Pitch.

	db 89	; Soft only. Volume: 11.
	db 32	; Additional data. Noise: 0. Pitch? true. Arp? false. Period? false.
	dw 500	; Pitch.

	db 81	; Soft only. Volume: 10.
	db 32	; Additional data. Noise: 0. Pitch? true. Arp? false. Period? false.
	dw 600	; Pitch.

	db 6	; Loop to silence.

Untitled_Instrument3
	db 1	; The speed (>0, 0 for 256).
	db 248	; No Soft no Hard. Volume: 15. Noise? true.
	db 1	; Noise: 1.

	db 232	; No Soft no Hard. Volume: 13. Noise? true.
	db 1	; Noise: 1.

	db 216	; No Soft no Hard. Volume: 11. Noise? true.
	db 1	; Noise: 1.

	db 192	; No Soft no Hard. Volume: 8. Noise? true.
	db 1	; Noise: 1.

	db 168	; No Soft no Hard. Volume: 5. Noise? true.
	db 1	; Noise: 1.

	db 6	; Loop to silence.

Untitled_Instrument4
	db 1	; The speed (>0, 0 for 256).
	db 121	; Soft only. Volume: 15.
	db 32	; Additional data. Noise: 0. Pitch? true. Arp? false. Period? false.
	dw 200	; Pitch.

	db 113	; Soft only. Volume: 14.
	db 32	; Additional data. Noise: 0. Pitch? true. Arp? false. Period? false.
	dw 250	; Pitch.

	db 105	; Soft only. Volume: 13.
	db 32	; Additional data. Noise: 0. Pitch? true. Arp? false. Period? false.
	dw 300	; Pitch.

	db 97	; Soft only. Volume: 12.
	db 32	; Additional data. Noise: 0. Pitch? true. Arp? false. Period? false.
	dw 350	; Pitch.

	db 89	; Soft only. Volume: 11.
	db 32	; Additional data. Noise: 0. Pitch? true. Arp? false. Period? false.
	dw 400	; Pitch.

	db 81	; Soft only. Volume: 10.
	db 32	; Additional data. Noise: 0. Pitch? true. Arp? false. Period? false.
	dw 450	; Pitch.

	db 73	; Soft only. Volume: 9.
	db 32	; Additional data. Noise: 0. Pitch? true. Arp? false. Period? false.
	dw 500	; Pitch.

	db 65	; Soft only. Volume: 8.
	db 32	; Additional data. Noise: 0. Pitch? true. Arp? false. Period? false.
	dw 550	; Pitch.

	db 57	; Soft only. Volume: 7.
	db 32	; Additional data. Noise: 0. Pitch? true. Arp? false. Period? false.
	dw 600	; Pitch.

	db 6	; Loop to silence.

Untitled_Instrument5
	db 1	; The speed (>0, 0 for 256).
	db 121	; Soft only. Volume: 15.
	db 32	; Additional data. Noise: 0. Pitch? true. Arp? false. Period? false.
	dw 500	; Pitch.

	db 113	; Soft only. Volume: 14.
	db 32	; Additional data. Noise: 0. Pitch? true. Arp? false. Period? false.
	dw 550	; Pitch.

	db 105	; Soft only. Volume: 13.
	db 32	; Additional data. Noise: 0. Pitch? true. Arp? false. Period? false.
	dw 600	; Pitch.

	db 97	; Soft only. Volume: 12.
	db 32	; Additional data. Noise: 0. Pitch? true. Arp? false. Period? false.
	dw 650	; Pitch.

	db 89	; Soft only. Volume: 11.
	db 32	; Additional data. Noise: 0. Pitch? true. Arp? false. Period? false.
	dw 700	; Pitch.

	db 81	; Soft only. Volume: 10.
	db 32	; Additional data. Noise: 0. Pitch? true. Arp? false. Period? false.
	dw 750	; Pitch.

	db 73	; Soft only. Volume: 9.
	db 32	; Additional data. Noise: 0. Pitch? true. Arp? false. Period? false.
	dw 800	; Pitch.

	db 65	; Soft only. Volume: 8.
	db 32	; Additional data. Noise: 0. Pitch? true. Arp? false. Period? false.
	dw 850	; Pitch.

	db 57	; Soft only. Volume: 7.
	db 32	; Additional data. Noise: 0. Pitch? true. Arp? false. Period? false.
	dw 900	; Pitch.

	db 6	; Loop to silence.

Untitled_Instrument6
	db 1	; The speed (>0, 0 for 256).
	db 248	; No Soft no Hard. Volume: 15. Noise? true.
	db 1	; Noise: 1.

	db 113	; Soft only. Volume: 14.
	db 34	; Additional data. Noise: 2. Pitch? true. Arp? false. Period? false.
	dw -10	; Pitch.

	db 97	; Soft only. Volume: 12.
	db 35	; Additional data. Noise: 3. Pitch? true. Arp? false. Period? false.
	dw -30	; Pitch.

	db 216	; No Soft no Hard. Volume: 11. Noise? true.
	db 1	; Noise: 1.

	db 80	; No Soft no Hard. Volume: 10. Noise? false.

	db 208	; No Soft no Hard. Volume: 10. Noise? true.
	db 1	; Noise: 1.

	db 200	; No Soft no Hard. Volume: 9. Noise? true.
	db 1	; Noise: 1.

	db 192	; No Soft no Hard. Volume: 8. Noise? true.
	db 1	; Noise: 1.

	db 184	; No Soft no Hard. Volume: 7. Noise? true.
	db 1	; Noise: 1.

	db 176	; No Soft no Hard. Volume: 6. Noise? true.
	db 1	; Noise: 1.

	db 168	; No Soft no Hard. Volume: 5. Noise? true.
	db 1	; Noise: 1.

	db 160	; No Soft no Hard. Volume: 4. Noise? true.
	db 1	; Noise: 1.

	db 144	; No Soft no Hard. Volume: 2. Noise? true.
	db 1	; Noise: 1.

	db 136	; No Soft no Hard. Volume: 1. Noise? true.
	db 1	; Noise: 1.

	db 6	; Loop to silence.

Untitled_Instrument7
	db 3	; The speed (>0, 0 for 256).
	db 248	; No Soft no Hard. Volume: 15. Noise? true.
	db 1	; Noise: 1.

	db 240	; No Soft no Hard. Volume: 14. Noise? true.
	db 1	; Noise: 1.

	db 232	; No Soft no Hard. Volume: 13. Noise? true.
	db 1	; Noise: 1.

	db 224	; No Soft no Hard. Volume: 12. Noise? true.
	db 1	; Noise: 1.

	db 216	; No Soft no Hard. Volume: 11. Noise? true.
	db 1	; Noise: 1.

	db 200	; No Soft no Hard. Volume: 9. Noise? true.
	db 1	; Noise: 1.

	db 192	; No Soft no Hard. Volume: 8. Noise? true.
	db 1	; Noise: 1.

	db 184	; No Soft no Hard. Volume: 7. Noise? true.
	db 1	; Noise: 1.

	db 176	; No Soft no Hard. Volume: 6. Noise? true.
	db 1	; Noise: 1.

	db 168	; No Soft no Hard. Volume: 5. Noise? true.
	db 1	; Noise: 1.

	db 160	; No Soft no Hard. Volume: 4. Noise? true.
	db 1	; Noise: 1.

	db 152	; No Soft no Hard. Volume: 3. Noise? true.
	db 1	; Noise: 1.

	db 144	; No Soft no Hard. Volume: 2. Noise? true.
	db 1	; Noise: 1.

	db 136	; No Soft no Hard. Volume: 1. Noise? true.
	db 1	; Noise: 1.

	db 6	; Loop to silence.

Untitled_Instrument8
	db 1	; The speed (>0, 0 for 256).
	db 249	; Soft only. Volume: 15. Volume only.

	db 241	; Soft only. Volume: 14. Volume only.

	db 233	; Soft only. Volume: 13. Volume only.

	db 225	; Soft only. Volume: 12. Volume only.

	db 217	; Soft only. Volume: 11. Volume only.

	db 209	; Soft only. Volume: 10. Volume only.

	db 201	; Soft only. Volume: 9. Volume only.

	db 193	; Soft only. Volume: 8. Volume only.

	db 185	; Soft only. Volume: 7. Volume only.

	db 177	; Soft only. Volume: 6. Volume only.

	db 169	; Soft only. Volume: 5. Volume only.

	db 161	; Soft only. Volume: 4. Volume only.

	db 153	; Soft only. Volume: 3. Volume only.

	db 145	; Soft only. Volume: 2. Volume only.

	db 137	; Soft only. Volume: 1. Volume only.

	db 6	; Loop to silence.


; The indexes of the effect blocks used by this song.
Untitled_EffectBlockTable
Untitled_DisarkPointerRegionStart6
	dw Untitled_EffectBlock_P4P1	; Index 0
	dw Untitled_EffectBlock_P4P2	; Index 1
	dw Untitled_EffectBlock_P4P0	; Index 2
	dw Untitled_EffectBlock_P4P3	; Index 3
	dw Untitled_EffectBlock_P4P4	; Index 4
	dw Untitled_EffectBlock_P4P6	; Index 5
Untitled_DisarkPointerRegionEnd6

Untitled_EffectBlock_P4P0
	db 4, 0
Untitled_EffectBlock_P4P1
	db 4, 1
Untitled_EffectBlock_P4P2
	db 4, 2
Untitled_EffectBlock_P4P3
	db 4, 3
Untitled_EffectBlock_P4P4
	db 4, 4
Untitled_EffectBlock_P4P6
	db 4, 6

Untitled_DisarkByteRegionEnd0

; Subsong 0
; ----------------------
Untitled_Subsong0_DisarkByteRegionStart0
Untitled_Subsong0_Start
	db 2	; ReplayFrequency (0=12.5hz, 1=25, 2=50, 3=100, 4=150, 5=300).
	db 0	; Digichannel (0-2).
	db 1	; PSG count (>0).
	db 0	; Loop start index (>=0).
	db 3	; End index (>=0).
	db 6	; Initial speed (>=0).
	db 25	; Base note index (>=0).

Untitled_Subsong0_Linker
Untitled_Subsong0_DisarkPointerRegionStart1
; Position 0
Untitled_Subsong0_Linker_Loop
	dw Untitled_Subsong0_Track0
	dw Untitled_Subsong0_Track1
	dw Untitled_Subsong0_Track2
	dw Untitled_Subsong0_LinkerBlock0

; Position 1
	dw Untitled_Subsong0_Track0
	dw Untitled_Subsong0_Track1
	dw Untitled_Subsong0_Track2
	dw Untitled_Subsong0_LinkerBlock1

; Position 2
	dw Untitled_Subsong0_Track3
	dw Untitled_Subsong0_Track4
	dw Untitled_Subsong0_Track5
	dw Untitled_Subsong0_LinkerBlock0

; Position 3
	dw Untitled_Subsong0_Track6
	dw Untitled_Subsong0_Track7
	dw Untitled_Subsong0_Track8
	dw Untitled_Subsong0_LinkerBlock0

Untitled_Subsong0_DisarkPointerRegionEnd1
	dw 0	; Loop.
Untitled_Subsong0_DisarkWordForceReference2
	dw Untitled_Subsong0_Linker_Loop

Untitled_Subsong0_LinkerBlock0
	db 64	; Height.
	db 0	; Transposition 0.
	db 0	; Transposition 1.
	db 0	; Transposition 2.
Untitled_Subsong0_DisarkWordForceReference3
	dw Untitled_Subsong0_SpeedTrack0	; SpeedTrack address.
Untitled_Subsong0_DisarkWordForceReference4
	dw Untitled_Subsong0_EventTrack0	; EventTrack address.
Untitled_Subsong0_LinkerBlock1
	db 4	; Height.
	db 0	; Transposition 0.
	db 0	; Transposition 1.
	db 0	; Transposition 2.
Untitled_Subsong0_DisarkWordForceReference5
	dw Untitled_Subsong0_SpeedTrack0	; SpeedTrack address.
Untitled_Subsong0_DisarkWordForceReference6
	dw Untitled_Subsong0_EventTrack0	; EventTrack address.

Untitled_Subsong0_Track0
	db 199
	db 1	; New Instrument (1).
	db 0	; Index to an effect block.
	db 151
	db 3	; New Instrument (3).
	db 23
	db 199
	db 1	; New Instrument (1).
	db 1	; Index to an effect block.
	db 78
	db 0	; Index to an effect block.
	db 151
	db 3	; New Instrument (3).
	db 23
	db 135
	db 1	; New Instrument (1).
	db 151
	db 2	; New Instrument (2).
	db 135
	db 1	; New Instrument (1).
	db 151
	db 3	; New Instrument (3).
	db 199
	db 1	; New Instrument (1).
	db 1	; Index to an effect block.
	db 78
	db 0	; Index to an effect block.
	db 151
	db 3	; New Instrument (3).
	db 135
	db 1	; New Instrument (1).
	db 215
	db 2	; New Instrument (2).
	db 1	; Index to an effect block.
	db 204
	db 1	; New Instrument (1).
	db 0	; Index to an effect block.
	db 151
	db 3	; New Instrument (3).
	db 23
	db 140
	db 1	; New Instrument (1).
	db 19
	db 151
	db 3	; New Instrument (3).
	db 23
	db 145
	db 1	; New Instrument (1).
	db 151
	db 2	; New Instrument (2).
	db 145
	db 1	; New Instrument (1).
	db 151
	db 3	; New Instrument (3).
	db 209
	db 1	; New Instrument (1).
	db 1	; Index to an effect block.
	db 88
	db 0	; Index to an effect block.
	db 151
	db 3	; New Instrument (3).
	db 139
	db 1	; New Instrument (1).
	db 215
	db 2	; New Instrument (2).
	db 1	; Index to an effect block.
	db 202
	db 1	; New Instrument (1).
	db 0	; Index to an effect block.
	db 151
	db 3	; New Instrument (3).
	db 23
	db 144
	db 1	; New Instrument (1).
	db 17
	db 151
	db 3	; New Instrument (3).
	db 23
	db 138
	db 1	; New Instrument (1).
	db 151
	db 2	; New Instrument (2).
	db 138
	db 1	; New Instrument (1).
	db 151
	db 3	; New Instrument (3).
	db 138
	db 1	; New Instrument (1).
	db 81
	db 1	; Index to an effect block.
	db 215
	db 3	; New Instrument (3).
	db 0	; Index to an effect block.
	db 138
	db 1	; New Instrument (1).
	db 74
	db 1	; Index to an effect block.
	db 74
	db 0	; Index to an effect block.
	db 151
	db 3	; New Instrument (3).
	db 23
	db 202
	db 1	; New Instrument (1).
	db 1	; Index to an effect block.
	db 81
	db 0	; Index to an effect block.
	db 151
	db 3	; New Instrument (3).
	db 23
	db 138
	db 1	; New Instrument (1).
	db 151
	db 2	; New Instrument (2).
	db 138
	db 1	; New Instrument (1).
	db 151
	db 3	; New Instrument (3).
	db 202
	db 1	; New Instrument (1).
	db 1	; Index to an effect block.
	db 81
	db 0	; Index to an effect block.
	db 151
	db 3	; New Instrument (3).
	db 138
	db 1	; New Instrument (1).
	db 215
	db 2	; New Instrument (2).
	db 1	; Index to an effect block.
	db 61, 127	; Waits for 128 lines.


Untitled_Subsong0_Track1
	db 62 + 0 * 64	; Optimized wait for 2 lines.

	db 238
	db 8	; New Instrument (8).
	db 0	; Index to an effect block.
	db 60	; Waits for 1 line.

	db 46
	db 60	; Waits for 1 line.

	db 112
	db 1	; Index to an effect block.
	db 60	; Waits for 1 line.

	db 48
	db 60	; Waits for 1 line.

	db 50
	db 50
	db 61, 5	; Waits for 6 lines.

	db 114
	db 0	; Index to an effect block.
	db 62 + 0 * 64	; Optimized wait for 2 lines.

	db 50
	db 62 + 0 * 64	; Optimized wait for 2 lines.

	db 114
	db 1	; Index to an effect block.
	db 62 + 0 * 64	; Optimized wait for 2 lines.

	db 115
	db 0	; Index to an effect block.
	db 62 + 0 * 64	; Optimized wait for 2 lines.

	db 53
	db 61, 17	; Waits for 18 lines.

	db 50
	db 62 + 0 * 64	; Optimized wait for 2 lines.

	db 50
	db 51
	db 62 + 0 * 64	; Optimized wait for 2 lines.

	db 51
	db 60	; Waits for 1 line.

	db 117
	db 1	; Index to an effect block.
	db 53
	db 61, 127	; Waits for 128 lines.


Untitled_Subsong0_Track2
	db 62 + 0 * 64	; Optimized wait for 2 lines.

	db 226
	db 8	; New Instrument (8).
	db 0	; Index to an effect block.
	db 60	; Waits for 1 line.

	db 34
	db 60	; Waits for 1 line.

	db 100
	db 1	; Index to an effect block.
	db 60	; Waits for 1 line.

	db 36
	db 60	; Waits for 1 line.

	db 38
	db 38
	db 61, 5	; Waits for 6 lines.

	db 102
	db 0	; Index to an effect block.
	db 62 + 0 * 64	; Optimized wait for 2 lines.

	db 38
	db 62 + 0 * 64	; Optimized wait for 2 lines.

	db 102
	db 1	; Index to an effect block.
	db 62 + 0 * 64	; Optimized wait for 2 lines.

	db 103
	db 0	; Index to an effect block.
	db 62 + 0 * 64	; Optimized wait for 2 lines.

	db 41
	db 61, 17	; Waits for 18 lines.

	db 38
	db 62 + 0 * 64	; Optimized wait for 2 lines.

	db 38
	db 39
	db 62 + 0 * 64	; Optimized wait for 2 lines.

	db 39
	db 60	; Waits for 1 line.

	db 105
	db 1	; Index to an effect block.
	db 41
	db 61, 127	; Waits for 128 lines.


Untitled_Subsong0_Track3
	db 211
	db 1	; New Instrument (1).
	db 0	; Index to an effect block.
	db 151
	db 3	; New Instrument (3).
	db 23
	db 153
	db 1	; New Instrument (1).
	db 26
	db 151
	db 3	; New Instrument (3).
	db 23
	db 147
	db 1	; New Instrument (1).
	db 151
	db 2	; New Instrument (2).
	db 147
	db 1	; New Instrument (1).
	db 151
	db 3	; New Instrument (3).
	db 147
	db 1	; New Instrument (1).
	db 90
	db 1	; Index to an effect block.
	db 215
	db 3	; New Instrument (3).
	db 0	; Index to an effect block.
	db 147
	db 1	; New Instrument (1).
	db 77
	db 1	; Index to an effect block.
	db 76
	db 0	; Index to an effect block.
	db 151
	db 3	; New Instrument (3).
	db 23
	db 140
	db 1	; New Instrument (1).
	db 19
	db 151
	db 3	; New Instrument (3).
	db 23
	db 145
	db 1	; New Instrument (1).
	db 151
	db 2	; New Instrument (2).
	db 133
	db 1	; New Instrument (1).
	db 151
	db 3	; New Instrument (3).
	db 133
	db 1	; New Instrument (1).
	db 12
	db 151
	db 3	; New Instrument (3).
	db 133
	db 1	; New Instrument (1).
	db 215
	db 3	; New Instrument (3).
	db 1	; Index to an effect block.
	db 202
	db 1	; New Instrument (1).
	db 0	; Index to an effect block.
	db 215
	db 6	; New Instrument (6).
	db 5	; Index to an effect block.
	db 87
	db 4	; Index to an effect block.
	db 208
	db 1	; New Instrument (1).
	db 0	; Index to an effect block.
	db 81
	db 1	; Index to an effect block.
	db 215
	db 6	; New Instrument (6).
	db 4	; Index to an effect block.
	db 215
	db 5	; New Instrument (5).
	db 0	; Index to an effect block.
	db 150
	db 1	; New Instrument (1).
	db 22
	db 22
	db 215
	db 3	; New Instrument (3).
	db 1	; Index to an effect block.
	db 208
	db 1	; New Instrument (1).
	db 0	; Index to an effect block.
	db 17
	db 151
	db 4	; New Instrument (4).
	db 138
	db 1	; New Instrument (1).
	db 215
	db 2	; New Instrument (2).
	db 1	; Index to an effect block.
	db 195
	db 1	; New Instrument (1).
	db 0	; Index to an effect block.
	db 151
	db 3	; New Instrument (3).
	db 23
	db 131
	db 1	; New Instrument (1).
	db 5
	db 151
	db 3	; New Instrument (3).
	db 215
	db 7	; New Instrument (7).
	db 1	; Index to an effect block.
	db 197
	db 1	; New Instrument (1).
	db 0	; Index to an effect block.
	db 10
	db 151
	db 6	; New Instrument (6).
	db 215
	db 3	; New Instrument (3).
	db 1	; Index to an effect block.
	db 209
	db 1	; New Instrument (1).
	db 0	; Index to an effect block.
	db 17
	db 215
	db 3	; New Instrument (3).
	db 3	; Index to an effect block.
	db 202
	db 1	; New Instrument (1).
	db 0	; Index to an effect block.
	db 215
	db 2	; New Instrument (2).
	db 1	; Index to an effect block.
	db 61, 127	; Waits for 128 lines.


Untitled_Subsong0_Track4
	db 238
	db 8	; New Instrument (8).
	db 0	; Index to an effect block.
	db 62 + 0 * 64	; Optimized wait for 2 lines.

	db 48
	db 62 + 0 * 64	; Optimized wait for 2 lines.

	db 50
	db 61, 7	; Waits for 8 lines.

	db 55
	db 61, 5	; Waits for 6 lines.

	db 53
	db 60	; Waits for 1 line.

	db 50
	db 62 + 0 * 64	; Optimized wait for 2 lines.

	db 48
	db 62 + 0 * 64	; Optimized wait for 2 lines.

	db 46
	db 61, 16	; Waits for 17 lines.

	db 51
	db 60	; Waits for 1 line.

	db 50
	db 60	; Waits for 1 line.

	db 46
	db 60	; Waits for 1 line.

	db 46
	db 61, 127	; Waits for 128 lines.


Untitled_Subsong0_Track5
	db 226
	db 8	; New Instrument (8).
	db 0	; Index to an effect block.
	db 62 + 0 * 64	; Optimized wait for 2 lines.

	db 36
	db 62 + 0 * 64	; Optimized wait for 2 lines.

	db 38
	db 61, 7	; Waits for 8 lines.

	db 43
	db 61, 5	; Waits for 6 lines.

	db 41
	db 60	; Waits for 1 line.

	db 38
	db 62 + 0 * 64	; Optimized wait for 2 lines.

	db 36
	db 62 + 0 * 64	; Optimized wait for 2 lines.

	db 34
	db 61, 16	; Waits for 17 lines.

	db 39
	db 60	; Waits for 1 line.

	db 38
	db 60	; Waits for 1 line.

	db 34
	db 60	; Waits for 1 line.

	db 34
	db 61, 127	; Waits for 128 lines.


Untitled_Subsong0_Track6
	db 204
	db 1	; New Instrument (1).
	db 0	; Index to an effect block.
	db 151
	db 3	; New Instrument (3).
	db 23
	db 199
	db 1	; New Instrument (1).
	db 1	; Index to an effect block.
	db 83
	db 0	; Index to an effect block.
	db 151
	db 3	; New Instrument (3).
	db 23
	db 145
	db 1	; New Instrument (1).
	db 151
	db 2	; New Instrument (2).
	db 145
	db 1	; New Instrument (1).
	db 151
	db 3	; New Instrument (3).
	db 209
	db 1	; New Instrument (1).
	db 1	; Index to an effect block.
	db 88
	db 0	; Index to an effect block.
	db 151
	db 3	; New Instrument (3).
	db 139
	db 1	; New Instrument (1).
	db 215
	db 2	; New Instrument (2).
	db 1	; Index to an effect block.
	db 202
	db 1	; New Instrument (1).
	db 0	; Index to an effect block.
	db 215
	db 5	; New Instrument (5).
	db 1	; Index to an effect block.
	db 215
	db 3	; New Instrument (3).
	db 0	; Index to an effect block.
	db 144
	db 1	; New Instrument (1).
	db 81
	db 1	; Index to an effect block.
	db 151
	db 3	; New Instrument (3).
	db 23
	db 214
	db 1	; New Instrument (1).
	db 0	; Index to an effect block.
	db 22
	db 22
	db 215
	db 3	; New Instrument (3).
	db 1	; Index to an effect block.
	db 208
	db 1	; New Instrument (1).
	db 0	; Index to an effect block.
	db 17
	db 151
	db 5	; New Instrument (5).
	db 138
	db 1	; New Instrument (1).
	db 215
	db 2	; New Instrument (2).
	db 1	; Index to an effect block.
	db 202
	db 1	; New Instrument (1).
	db 2	; Index to an effect block.
	db 215
	db 3	; New Instrument (3).
	db 0	; Index to an effect block.
	db 23
	db 145
	db 1	; New Instrument (1).
	db 81
	db 2	; Index to an effect block.
	db 215
	db 3	; New Instrument (3).
	db 0	; Index to an effect block.
	db 215
	db 7	; New Instrument (7).
	db 1	; Index to an effect block.
	db 202
	db 1	; New Instrument (1).
	db 0	; Index to an effect block.
	db 151
	db 2	; New Instrument (2).
	db 151
	db 3	; New Instrument (3).
	db 215
	db 7	; New Instrument (7).
	db 1	; Index to an effect block.
	db 209
	db 1	; New Instrument (1).
	db 2	; Index to an effect block.
	db 215
	db 2	; New Instrument (2).
	db 0	; Index to an effect block.
	db 151
	db 3	; New Instrument (3).
	db 138
	db 1	; New Instrument (1).
	db 215
	db 2	; New Instrument (2).
	db 1	; Index to an effect block.
	db 202
	db 1	; New Instrument (1).
	db 0	; Index to an effect block.
	db 215
	db 3	; New Instrument (3).
	db 3	; Index to an effect block.
	db 202
	db 1	; New Instrument (1).
	db 0	; Index to an effect block.
	db 215
	db 2	; New Instrument (2).
	db 1	; Index to an effect block.
	db 87
	db 0	; Index to an effect block.
	db 201
	db 1	; New Instrument (1).
	db 2	; Index to an effect block.
	db 215
	db 3	; New Instrument (3).
	db 1	; Index to an effect block.
	db 202
	db 1	; New Instrument (1).
	db 2	; Index to an effect block.
	db 61, 127	; Waits for 128 lines.


Untitled_Subsong0_Track7
	db 183
	db 8	; New Instrument (8).
	db 62 + 3 * 64	; Optimized wait for 5 lines.

	db 53
	db 60	; Waits for 1 line.

	db 50
	db 62 + 0 * 64	; Optimized wait for 2 lines.

	db 48
	db 62 + 0 * 64	; Optimized wait for 2 lines.

	db 46
	db 61, 127	; Waits for 128 lines.


Untitled_Subsong0_Track8
	db 171
	db 8	; New Instrument (8).
	db 62 + 3 * 64	; Optimized wait for 5 lines.

	db 41
	db 60	; Waits for 1 line.

	db 38
	db 62 + 0 * 64	; Optimized wait for 2 lines.

	db 36
	db 62 + 0 * 64	; Optimized wait for 2 lines.

	db 34
	db 61, 127	; Waits for 128 lines.


; The speed tracks
Untitled_Subsong0_SpeedTrack0
	db 255	; Wait for 128 lines.

; The event tracks
Untitled_Subsong0_EventTrack0
	db 255	; Wait for 128 lines.

Untitled_Subsong0_DisarkByteRegionEnd0
