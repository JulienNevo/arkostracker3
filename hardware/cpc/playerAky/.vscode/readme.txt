You can add the shortcuts to keybindings.json:

    {
        "key": "ctrl+[Backquote]",
        "command": "workbench.action.tasks.runTask",
        "args": "buildRasm"
    }