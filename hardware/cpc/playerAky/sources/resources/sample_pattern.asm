;Test format for a generic pattern. Here used for samples.

;Required info:
;db type of data:
;   0 = channel info: Note + instrument + channel index
;   1 = wait
;       dw wait (>0)
;   2 = effect only (currently not supported).
;   255 = endsong
;       dw loop address

SamplePattern
.loop
    db 0
        db 6 * 12 + 0, 0, 0        ;Note
    db 1
        dw 6 * 10                   ;Wait

    db 0
        db 6 * 12 + 1, 0, 1        ;Note
    db 1
        dw 6 * 10                   ;Wait

    db 0
        db 6 * 12 + 1, 0, 2        ;Note
    db 1
        dw 6 * 20                   ;Wait

    db 255    ;End
        dw .loop