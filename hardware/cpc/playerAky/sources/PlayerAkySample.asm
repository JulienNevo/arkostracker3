    ;Sample player, included to the PlayerAkyMultiPsg.
    ;This player can play one sampled note at a time, but on any channel, alongside standard PSG music.
    ;Do not include this player directly to your code, it is included by the aforementioned player directly.
    ;See the testers for a fully working example.

    ;TODO Generate new samples.     *****
    ;TODO Export option for the sample pattern (generic format??).
    ;TODO ROM

;How many bytes are played every frame. This should be as high as possible to "fill" the VBL.
;It is also possible to skip the wait for the frame flyback to maximize the quality,
;but then synchronizing anything on the frame will be impossible.
;The samples should have a padding of at least this value, else glitches
;are going to be heard at the end of each sample (AT can generate that).
PLY_AKY_SAMPLE_BYTE_PER_FRAME = 295

;Initializes the samples. It must be called before playing the song.
;HL = address of the sample patterns (exported from AT).
;DE = address of the sample table (exported from AT).
PLY_AKY_InitSamples
    ld (PLY_AKY_SPL_PT_Pattern + 1),hl
    ld (PLY_AKY_SPL_PT_SampleTable + 1),de

    ld hl,1
    ld (PLY_AKY_ManageSample.wait + 1),hl
    xor a
    ld (PLY_AKY_RegisterToSkip + 1),a
    ld (PLY_AKY_SPL_Sample + 1),hl
    ret

;Manages the presence or not of the samples by reading the sample pattern.
;Called from the player every frame BEFORE the PSG values are sent.
;The stack must NOT be. Then goes to PLY_AKY_ManageSample_Return.
;IN:    B = R7.
;OUT:   B = R7, modified or not.
PLY_AKY_ManageSample
.wait   ld hl,0             ;Frames to wait?
    dec hl
    ld a,l
    or h
    jr z,.endWait
    ;Wait not finished.
    ld (PLY_AKY_ManageSample.wait + 1),hl
    jp PLY_AKY_ManageSample_Return
.endWait
    ;Wait finished. Reads the next value.
PLY_AKY_SPL_PT_Pattern ld hl,0
PLY_AKY_SPL_ReadCommand ld a,(hl)       ;What command?
    inc hl
    or a
    jr z,PLY_AKY_SPL_FoundNote
    dec a
    jr z,PLY_AKY_SPL_Wait
    ;End of song. Reads the loop and continues.
    ld a,(hl)
    inc hl
    ld h,(hl)
    ld l,a
    jr PLY_AKY_SPL_ReadCommand

PLY_AKY_SPL_Wait
    ;Wait. Reads how many frames to wait.
    ld e,(hl)
    inc hl
    ld d,(hl)
    inc hl
    ld (PLY_AKY_ManageSample.wait + 1),de
    ld (PLY_AKY_SPL_PT_Pattern + 1),hl
    jp PLY_AKY_ManageSample_Return

PLY_AKY_SPL_FoundNote
    ld a,(hl)       ;Gets the note.
    inc hl
    ;Finds the step of the current note.
    add a,a         ;Only 7 bits, so all right.
    exx
        ld l,a
        ld h,0
        ld de,PLY_AKY_SPL_StepsTable
        add hl,de
        ld a,(hl)
        ld (PLY_AKY_SPL_StepDecimal + 1),a
        inc hl
        ld a,(hl)
        ld (PLY_AKY_SPL_StepInteger + 1),a
    exx

    ld a,(hl)       ;Gets the instrument.
    inc hl
    exx
PLY_AKY_SPL_PT_SampleTable  ld de,0
        ld l,a
        ld h,0
        add hl,hl
        add hl,de
        ld a,(hl)
        inc hl
        ld h,(hl)
        ld l,a
        ld de,PLY_AKY_SPL_SampleEnd + 1
        ldi
        ldi
        ld de,PLY_AKY_SPL_SampleLoop + 1
        ldi
        ldi
        ld (PLY_AKY_SPL_Sample + 1),hl
    exx
    ld a,(hl)       ;Gets the channel index.
    inc hl
    ;set 0,b = 0xcb 0xc0. Encoding the second byte.
    ;set 1,b = 0xcb 0xc8
    ;set 2,b = 0xcb 0xd0
    ld c,a
    add a,a
    add a,a
    add a,a
    add a,#c0
    ld (PLY_AKY_SPL_R7CloseChannelOpcode + 1),a // The second byte is modified (even for ROM).
    ld a,c
    add a,8
    ld (PLY_AKY_SPL_SampleVolumeRegister + 1),a

    ;If a sample is being played, the R7 channel must be closed NOW, before the player is going to
    ;sends this value to the PSG
    ld de,(PLY_AKY_SPL_Sample + 1)
    ld a,e
    or d
    jr z,PLY_AKY_SPL_ReadCommand
PLY_AKY_SPL_R7CloseChannelOpcode set 1,b     ;The second byte is modified (set 0/1/2 on init).

    ;Next command.
    jr PLY_AKY_SPL_ReadCommand
    






;Plays a frame of the sample, or any is programmed.
;Do NOT use the stack. Use JP to call this. PLY_AKY_ManageSample_Return is called on return.
PLY_AKY_PlaySample
PLY_AKY_SPL_Sample ld hl,0   ;0 means no sample frame to play.
    ld a,l
    or h
    jr nz,.samplePresent
    ;No sample. Allows the player to uses its own volume value.
    xor a
    ld (PLY_AKY_RegisterToSkip + 1),a
    jp PLY_AKY_PlaySample_Return
.samplePresent

PLY_AKY_SPL_SampleVolumeRegister ld a,0     ;8/9/10.
    ld (PLY_AKY_RegisterToSkip + 1),a       ;The player code must not play its own volume.
    ;Selects the sample channel.
    ld b,#f4
    out (c),a
    ld bc,#f6c0
    out (c),c
    out (c),0

    ;Main sample loop at 16khz.

    exx
        ld hl,#f4f6
    exx
    ld de,PLY_AKY_SAMPLE_BYTE_PER_FRAME
    ld c,0   ;C = Increasing decimal step.

PLY_AKY_SPL_PlaySampleLoop
    ld a,(hl)           ;Reads from the sample.
    exx
        ld b,h
        out (c),a       ;Sends the byte from HL.
        ld b,l
        out (c),a       ;#f680 (a trick, our sample value has its 7th bit to 1).
        out (c),0       ;#f600
    exx

    ;Moves forward in the sample.
    ld a,c
PLY_AKY_SPL_StepDecimal add a,0
    ld c,a
    ld a,l
PLY_AKY_SPL_StepInteger adc a,0
    ld l,a
    jr nc,.forwardNoCarry
    inc h
.forwardNoCarry

    dec de
    ld a,e
    or d
    jr z,PLY_AKY_SPL_PlaySampleEnd

    ;Wastes time to reach 16 khz somehow. The loop should last 64 nops.
    ld a,6
.wasteTime  dec a
    jr nz,.wasteTime
    nop
    jr PLY_AKY_SPL_PlaySampleLoop

PLY_AKY_SPL_PlaySampleEnd
    ex de,hl        ;DE = advanced pointer on the sample.
    ;Are we beyond the sample range?
PLY_AKY_SPL_SampleEnd ld hl,0
    or a
    sbc hl,de
    jr nc,PLY_AKY_SPL_SetNewSamplePt
    ;End sample ended. Loop? If not 0, loops.
PLY_AKY_SPL_SampleLoop ld de,0

PLY_AKY_SPL_SetNewSamplePt
    ld (PLY_AKY_SPL_Sample + 1),de

    jp PLY_AKY_PlaySample_Return









;Steps for each note, nominal octave. Calculated by ear :). Don't worry, I'm good at that.
;Same table as for the MOD player.
PLY_AKY_SPL_Step_Oct4_0: equ #0100
PLY_AKY_SPL_Step_Oct4_1: equ #0110
PLY_AKY_SPL_Step_Oct4_2: equ #0120
PLY_AKY_SPL_Step_Oct4_3: equ #0130
PLY_AKY_SPL_Step_Oct4_4: equ #0140
PLY_AKY_SPL_Step_Oct4_5: equ #015a
PLY_AKY_SPL_Step_Oct4_6: equ #016a
PLY_AKY_SPL_Step_Oct4_7: equ #0180
PLY_AKY_SPL_Step_Oct4_8: equ #0196
PLY_AKY_SPL_Step_Oct4_9: equ #01ae
PLY_AKY_SPL_Step_Oct4_10: equ #01cb
PLY_AKY_SPL_Step_Oct4_11: equ #01e0

;Steps for every note.
PLY_AKY_SPL_StepsTable:
        dw PLY_AKY_SPL_Step_Oct4_0 / 128                            ;Octave 0
        dw PLY_AKY_SPL_Step_Oct4_1 / 128
        dw PLY_AKY_SPL_Step_Oct4_2 / 128
        dw PLY_AKY_SPL_Step_Oct4_3 / 128
        dw PLY_AKY_SPL_Step_Oct4_4 / 128
        dw PLY_AKY_SPL_Step_Oct4_5 / 128
        dw PLY_AKY_SPL_Step_Oct4_6 / 128
        dw PLY_AKY_SPL_Step_Oct4_7 / 128
        dw PLY_AKY_SPL_Step_Oct4_8 / 128
        dw PLY_AKY_SPL_Step_Oct4_9 / 128
        dw PLY_AKY_SPL_Step_Oct4_10 / 128
        dw PLY_AKY_SPL_Step_Oct4_11 / 128

        dw PLY_AKY_SPL_Step_Oct4_0 / 64                            ;Octave 1
        dw PLY_AKY_SPL_Step_Oct4_1 / 64
        dw PLY_AKY_SPL_Step_Oct4_2 / 64
        dw PLY_AKY_SPL_Step_Oct4_3 / 64
        dw PLY_AKY_SPL_Step_Oct4_4 / 64
        dw PLY_AKY_SPL_Step_Oct4_5 / 64
        dw PLY_AKY_SPL_Step_Oct4_6 / 64
        dw PLY_AKY_SPL_Step_Oct4_7 / 64
        dw PLY_AKY_SPL_Step_Oct4_8 / 64
        dw PLY_AKY_SPL_Step_Oct4_9 / 64
        dw PLY_AKY_SPL_Step_Oct4_10 / 64
        dw PLY_AKY_SPL_Step_Oct4_11 / 64

        dw PLY_AKY_SPL_Step_Oct4_0 / 32                            ;Octave 2
        dw PLY_AKY_SPL_Step_Oct4_1 / 32
        dw PLY_AKY_SPL_Step_Oct4_2 / 32
        dw PLY_AKY_SPL_Step_Oct4_3 / 32
        dw PLY_AKY_SPL_Step_Oct4_4 / 32
        dw PLY_AKY_SPL_Step_Oct4_5 / 32
        dw PLY_AKY_SPL_Step_Oct4_6 / 32
        dw PLY_AKY_SPL_Step_Oct4_7 / 32
        dw PLY_AKY_SPL_Step_Oct4_8 / 32
        dw PLY_AKY_SPL_Step_Oct4_9 / 32
        dw PLY_AKY_SPL_Step_Oct4_10 / 32
        dw PLY_AKY_SPL_Step_Oct4_11 / 32

        dw PLY_AKY_SPL_Step_Oct4_0 / 16                            ;Octave 3
        dw PLY_AKY_SPL_Step_Oct4_1 / 16
        dw PLY_AKY_SPL_Step_Oct4_2 / 16
        dw PLY_AKY_SPL_Step_Oct4_3 / 16
        dw PLY_AKY_SPL_Step_Oct4_4 / 16
        dw PLY_AKY_SPL_Step_Oct4_5 / 16
        dw PLY_AKY_SPL_Step_Oct4_6 / 16
        dw PLY_AKY_SPL_Step_Oct4_7 / 16
        dw PLY_AKY_SPL_Step_Oct4_8 / 16
        dw PLY_AKY_SPL_Step_Oct4_9 / 16
        dw PLY_AKY_SPL_Step_Oct4_10 / 16
        dw PLY_AKY_SPL_Step_Oct4_11 / 16

        dw PLY_AKY_SPL_Step_Oct4_0 / 8                            ;Octave 4
        dw PLY_AKY_SPL_Step_Oct4_1 / 8
        dw PLY_AKY_SPL_Step_Oct4_2 / 8
        dw PLY_AKY_SPL_Step_Oct4_3 / 8
        dw PLY_AKY_SPL_Step_Oct4_4 / 8
        dw PLY_AKY_SPL_Step_Oct4_5 / 8
        dw PLY_AKY_SPL_Step_Oct4_6 / 8
        dw PLY_AKY_SPL_Step_Oct4_7 / 8
        dw PLY_AKY_SPL_Step_Oct4_8 / 8
        dw PLY_AKY_SPL_Step_Oct4_9 / 8
        dw PLY_AKY_SPL_Step_Oct4_10 / 8
        dw PLY_AKY_SPL_Step_Oct4_11 / 8

        dw PLY_AKY_SPL_Step_Oct4_0 / 4                            ;Octave 5
        dw PLY_AKY_SPL_Step_Oct4_1 / 4
        dw PLY_AKY_SPL_Step_Oct4_2 / 4
        dw PLY_AKY_SPL_Step_Oct4_3 / 4
        dw PLY_AKY_SPL_Step_Oct4_4 / 4
        dw PLY_AKY_SPL_Step_Oct4_5 / 4
        dw PLY_AKY_SPL_Step_Oct4_6 / 4
        dw PLY_AKY_SPL_Step_Oct4_7 / 4
        dw PLY_AKY_SPL_Step_Oct4_8 / 4
        dw PLY_AKY_SPL_Step_Oct4_9 / 4
        dw PLY_AKY_SPL_Step_Oct4_10 / 4
        dw PLY_AKY_SPL_Step_Oct4_11 / 4

        dw PLY_AKY_SPL_Step_Oct4_0 / 2                            ;Octave 6
        dw PLY_AKY_SPL_Step_Oct4_1 / 2
        dw PLY_AKY_SPL_Step_Oct4_2 / 2
        dw PLY_AKY_SPL_Step_Oct4_3 / 2
        dw PLY_AKY_SPL_Step_Oct4_4 / 2
        dw PLY_AKY_SPL_Step_Oct4_5 / 2
        dw PLY_AKY_SPL_Step_Oct4_6 / 2
        dw PLY_AKY_SPL_Step_Oct4_7 / 2
        dw PLY_AKY_SPL_Step_Oct4_8 / 2
        dw PLY_AKY_SPL_Step_Oct4_9 / 2
        dw PLY_AKY_SPL_Step_Oct4_10 / 2
        dw PLY_AKY_SPL_Step_Oct4_11 / 2

        dw PLY_AKY_SPL_Step_Oct4_0                                ;Octave 7
        dw PLY_AKY_SPL_Step_Oct4_1
        dw PLY_AKY_SPL_Step_Oct4_2
        dw PLY_AKY_SPL_Step_Oct4_3
        dw PLY_AKY_SPL_Step_Oct4_4
        dw PLY_AKY_SPL_Step_Oct4_5
        dw PLY_AKY_SPL_Step_Oct4_6
        dw PLY_AKY_SPL_Step_Oct4_7
        dw PLY_AKY_SPL_Step_Oct4_8
        dw PLY_AKY_SPL_Step_Oct4_9
        dw PLY_AKY_SPL_Step_Oct4_10
        dw PLY_AKY_SPL_Step_Oct4_11

        dw PLY_AKY_SPL_Step_Oct4_0 * 2                            ;Octave 8
        dw PLY_AKY_SPL_Step_Oct4_1 * 2
        dw PLY_AKY_SPL_Step_Oct4_2 * 2
        dw PLY_AKY_SPL_Step_Oct4_3 * 2
        dw PLY_AKY_SPL_Step_Oct4_4 * 2
        dw PLY_AKY_SPL_Step_Oct4_5 * 2
        dw PLY_AKY_SPL_Step_Oct4_6 * 2
        dw PLY_AKY_SPL_Step_Oct4_7 * 2
        dw PLY_AKY_SPL_Step_Oct4_8 * 2
        dw PLY_AKY_SPL_Step_Oct4_9 * 2
        dw PLY_AKY_SPL_Step_Oct4_10 * 2
        dw PLY_AKY_SPL_Step_Oct4_11 * 2

        dw PLY_AKY_SPL_Step_Oct4_0 * 4                            ;Octave 9
        dw PLY_AKY_SPL_Step_Oct4_1 * 4
        dw PLY_AKY_SPL_Step_Oct4_2 * 4
        dw PLY_AKY_SPL_Step_Oct4_3 * 4
        dw PLY_AKY_SPL_Step_Oct4_4 * 4
        dw PLY_AKY_SPL_Step_Oct4_5 * 4
        dw PLY_AKY_SPL_Step_Oct4_6 * 4
        dw PLY_AKY_SPL_Step_Oct4_7 * 4
        dw PLY_AKY_SPL_Step_Oct4_8 * 4
        dw PLY_AKY_SPL_Step_Oct4_9 * 4
        dw PLY_AKY_SPL_Step_Oct4_10 * 4
        dw PLY_AKY_SPL_Step_Oct4_11 * 4

        dw PLY_AKY_SPL_Step_Oct4_0 * 8                            ;Octave 10
        dw PLY_AKY_SPL_Step_Oct4_1 * 8
        dw PLY_AKY_SPL_Step_Oct4_2 * 8
        dw PLY_AKY_SPL_Step_Oct4_3 * 8
        dw PLY_AKY_SPL_Step_Oct4_4 * 8
        dw PLY_AKY_SPL_Step_Oct4_5 * 8
        dw PLY_AKY_SPL_Step_Oct4_6 * 8
        dw PLY_AKY_SPL_Step_Oct4_7 * 8
        ;dw PLY_AKY_SPL_Step_Oct4_8 * 8
        ;dw PLY_AKY_SPL_Step_Oct4_9 * 8
        ;dw PLY_AKY_SPL_Step_Oct4_10 * 8
        ;dw PLY_AKY_SPL_Step_Oct4_11 * 8

        assert ($ - PLY_AKY_SPL_StepsTable) == 256